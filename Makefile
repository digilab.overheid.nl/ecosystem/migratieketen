-include ./local_*/Makefile

PROJECT_NAME := migratieketen
CLUSTER_EXISTS := $$(k3d cluster list $(PROJECT_NAME) --no-headers | wc -l | xargs)
ROOT_DIR := ${PWD}

# to get the correct KUBECONFIG env var:
# - If no KUBECONFIG has been set, or KUBECONFIG is empty, keep using the default.
# - If KUBECONFIG is a single file, keep using that file.
# - If KUBECONFIG contains multiple files, k3d will be confused.
#   - If the default config (ie. "$HOME/.kube/config") is contained in KUBECONFIG, we use that.
#     Note that this will incorrectly find "$HOME/.kube/config foo" because of limitations in make pattern matching, so don't use spaces ;)
#   - If it is not, we give up, and k3d won't be able to automatically create or delete the kubectl context.
DEFAULT_KUBECONFIG := $(if $(findstring :,${KUBECONFIG}),$(if $(filter ${HOME}/.kube/config,$(subst :, ,${KUBECONFIG})),${HOME}/.kube/config,${KUBECONFIG}),${KUBECONFIG})
KUBECONFIG=$(DEFAULT_KUBECONFIG)

.PHONY: k3d
k3d:
	@if [ $(CLUSTER_EXISTS) -eq 0 ]; then \
		k3d cluster create --config=deploy/k3d/config.yaml; \
		sleep 10; \
		kubectl -n kube-system wait --for=condition=ready pod -l job-name=helm-install-cloudnative-pg; \
		kubectl -n kube-system wait --for=condition=ready=false pod -l job-name=helm-install-cloudnative-pg; \
	else \
		k3d cluster start "$(PROJECT_NAME)"; \
	fi
	kubectl config use-context "k3d-$(PROJECT_NAME)"

.PHONY: dev
dev: k3d
	kubectl apply -k deploy/kustomize/overlays/rrd
	skaffold dev --cleanup=false

.PHONY: stop
stop:
	k3d cluster stop "$(PROJECT_NAME)"

.PHONY: clean
clean:
	k3d cluster delete "$(PROJECT_NAME)"

.PHONY: generate
generate:
	@for f in */go.mod; do \
		( cd "$$(dirname "$$f")"; go generate ); \
	done
	@for f in */*/storage/queries/sqlc.yaml; do \
		docker run --rm -v "$(ROOT_DIR)/$$(dirname "$$(dirname "$$f")"):/src" -w /src/queries sqlc/sqlc:1.25.0 generate; \
	done

.PHONY: seed
seed: reset_fsc_dbs
	go run ./seeder seed --sigma-servers http://np-sigma-backend-127.0.0.1.nip.io:8080/v0

.PHONY: lint
lint:
	@for f in */go.mod; do \
		( echo "linting $$(dirname "$$f")"; cd "$$(dirname "$$f")"; golangci-lint run . --skip-files=.gvm ) || exit 1; \
	done

.PHONY: reset_fsc_dbs
reset_fsc_dbs:
	@$(MAKE) reset_db NS=rrd ORG=rrd DB=nlx_controller DPL=controller-fsc-nlx-controller
	@$(MAKE) reset_db NS=rrd ORG=rrd DB=nlx_manager DPL=manager-fsc-nlx-manager
	@$(MAKE) reset_db NS="$(PROJECT_NAME)" ORG=shared DB=nlx_controller DPL=controller-fsc-nlx-controller
	@$(MAKE) reset_db NS="$(PROJECT_NAME)" ORG=shared DB=nlx_manager DPL=manager-fsc-nlx-manager

.PHONY: reset_db
reset_db:
	# Kill all pod so any active db connections are terminated
	kubectl -n $(NS) scale deployment $(DPL) --replicas=0

	# Copy the fixture to the PostgreSQL data directory in the pod
	kubectl -n $(NS) cp -c postgres deploy/fixtures/$(ORG)_$(DB).sql $(ORG)-common-db-1:/var/lib/postgresql/data/dump.sql

	# Drop the existing database
	kubectl -n $(NS) exec $(ORG)-common-db-1 -c postgres -- sh -c 'psql -c "DROP DATABASE $(DB);"'

	# Create a new database with the common owner
	kubectl -n $(NS) exec $(ORG)-common-db-1 -c postgres -- sh -c 'psql -c "CREATE DATABASE $(DB) WITH OWNER common;"'

	# Restore the database from the fixture
	kubectl -n $(NS) exec $(ORG)-common-db-1 -c postgres -- sh -c 'PGPASSWORD=common psql -U common -h 127.0.0.1 $(DB) < /var/lib/postgresql/data/dump.sql'

	# Start the pods
	kubectl -n $(NS) scale deployment $(DPL) --replicas=1

.PHONY: test
test:
	test/integration
	@for f in */go.mod; do \
		( echo "unit-testing $$(dirname "$$f")"; cd "$$(dirname "$$f")"; go test ./... ) || exit 1; \
	done

.PHONY: tidy
tidy:
	@for f in */go.mod; do \
		( cd "$$(dirname "$$f")"; go mod tidy ) || exit 1; \
	done
