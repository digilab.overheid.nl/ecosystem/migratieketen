# Schematic overview
Schematic overview of multiple components of the Migratieketen in the 'logboek dataverwerking' context.

Whenever a requests enters one of the components: sigma, loket or bvv and the requests is concerning a 'persoonsgegeven' it needs to log the request with all necessary information.

Overview of the migratieketen:
```mermaid
graph TD
	linkStyle default interpolate basis
	subgraph Migratieketen[ ]
		style Migratieketen fill:none,stroke:none;

		subgraph sigma[Sigma]
			app_1["Golang server"]
		end

		subgraph loket[Loket]
			app_3["Golang server"]
		end

		subgraph bvv[BVV]
			app_2["Golang server"]
		end


	end

    official[Official] --> |Browser| loket

	systems[Other Systems] ---> |http| sigma
	app_3 --> |HTTP|sigma

	systems[Other Systems] ---> |http| loket

	app_3 --> |HTTP|bvv
	systems[Other Systems] ---> |http| bvv

```

### OpenTelemetry
To achieve this, the open source library [`OpenTelemetry`](https://opentelemetry.io/) is used.
By adding a few lines of code, tracing can be enabled using the [`SDK`](https://github.com/open-telemetry/opentelemetry-go) provided by OpenTelemetry.
The SDK aims to write trace information to disk as soon as possible to ensure no traces are lost.
For this purpose, a [`collector`](https://opentelemetry.io/docs/collector/) is used.
The collector imports the traces from the SDK, processes the traces to verify or add additional information, and exports the trace into a storage backend.

### Implementation
A collector can be deployed in many ways but in this instance a K8S sidecar is used. For the storage backend 'clickhouse' is used. After running multiple benchmarks with different storage backends, clickhouse came out as one of the fastest backends.


Overview of the migratieketen with datalogboek verwerkingen
```mermaid
graph TD
	linkStyle default interpolate basis
	subgraph Migratieketen[ ]
		style Migratieketen fill:none,stroke:none;
		subgraph sigma[Sigma]
			app_1["Golang server"]

			subgraph sidecar_1[sidecar]
				otel_1["Golang-collector"]
			end

			subgraph storagebackend_1[Storage Backend]
				storage_1["ClickHouse"]
			end

			subgraph inzichtbackend_1[Inzicht Backend]
				app_1_1["Golang server"]
			end

			app_1 ---> |GRPC|otel_1 --> |TCP|storagebackend_1
		end

		subgraph bvv[BVV]
			app_2["Golang server"]

			subgraph sidecar_2[sidecar]
				otel_2["Golang-collector"]
			end

			subgraph storagebackend_2[Storage Backend]
				storage_2["ClickHouse"]
			end

			subgraph inzichtbackend_2[Inzicht Backend]
				app_2_2["Golang server"]
			end

			app_2 ---> |GRPC|otel_2 --> |TCP|storagebackend_2
		end

		subgraph loket[Loket]
			app_3["Golang server"]

			subgraph sidecar_3[sidecar]
				otel_3["Golang-collector"]
			end

			subgraph storagebackend_3[Storage Backend]
				storage_3["ClickHouse"]
			end

			subgraph inzichtbackend_3[Inzicht Backend]
				app_3_3["Golang server"]
			end

			app_3 ---> |GRPC|otel_3 --> |TCP|storagebackend_3

		end

		inzichtbackend_1 --> |TCP|storagebackend_1
		inzichtbackend_2 --> |TCP|storagebackend_2
		inzichtbackend_3 --> |TCP|storagebackend_3

		app_3 --> |HTTP| app_2
		app_3 --> |HTTP| app_1
	end
```


## Current Problem
When a trace containing personal data, `persoonsgegevens`, is created, it is usually linked to an individual.
This data is necessary to provide 'insights' from a citizens perspective, allowing them to query all government requests concerning them. To facilitate this, an identifier linked to the citizen must be stored.

The inzicht-api is developed to query all traces associated with an ecrypted/hashed BSN. In the decentralized architecture, there would be numerous inzicht-apis available, all queried by the inzicht App. To reduce the load and improve privacy, a bloom filter can be implemented at the inzicht-api level to quickly determine if there is definitely no record available.

Requirements:
- An inzicht-API should not have a plain-text citizen identifier like a BSN in its storage;
- An inzicht-API should be made to not be aware if a BSN has insights when using the bloom filter.

### Encryption of a citizen identifier
Encryption of the citizen identifier should be done as soon as possible in the trace pipeline of the open telemetry collector. Preferably in the first processor.
This will make sure that an identifier can not be leaked into other parts of the system.

### Bloom filter
A bloom filter is a probalistic data structure. it can give a definite `no`, but not a certain `yes` about if the set contains the value. More information about how a bloom filter functions can be found [`here`](https://samwho.dev/bloom-filters). The filter can be used in the inzicht API to determine if a given `bsn` has a trace.

The collector can export the citizen ID to the bloom filter.


```mermaid
graph TD
	linkStyle default interpolate basis
	subgraph Collector[Overview of the collector]
		style Collector stroke:none;
		subgraph importer[Importer]
			golang_server[Golang server]
		end

		subgraph processor[Processor]
			encryption[Citizen ID encryptor]
		end

		subgraph exporter[Exporter]
			bloom_filter[Exporter to the bloom filter]
		end

		importer --> processor --> exporter
	end
```

# Privacy
To enable privacy for the citizen when quering the insights the first call should be the lookup query.
This query will use the bloom filter and check if it contains the Identifier of the citizen.
The inzicht app will do this automaticaly by hashing your Identifier and sending only the set bit of the complete bitmap.

Below you can find a sequence diagram of an insights call from a citizens point of view.

```mermaid
sequenceDiagram
actor Citizen
Citizen->>Inzicht App: I want to see my insights

par Inzicht App to Inzicht Backend 1
	Inzicht App->>Inzicht Backend 1: /lookup: Lookup if hashed identifier 1 has insights
	activate Inzicht Backend 1
	Inzicht Backend 1->>Inzicht App: 200: I maybe have something
	deactivate Inzicht Backend 1

	Inzicht App->>Inzicht Backend 1: /logs: Get logs
	activate Inzicht Backend 1
	Inzicht Backend 1->>Inzicht App: 200: Here are the insights
	deactivate Inzicht Backend 1

and Inzicht App to Inzicht Backend 2
	Inzicht App->>Inzicht Backend 2: /lookup: Lookup if hashed identifier 1 has insights
	activate Inzicht Backend 2
	Inzicht Backend 2->>Inzicht App: 404: I definitly dont have something
	deactivate Inzicht Backend 2
end
Inzicht App->>Citizen: Here are you insights
```

The lookup endpoints should not be behind authentication otherwise it will leak the person behind the query into a logging system or something else and that would make the lookup call useless.

```mermaid
graph TD
	subgraph inzicht app[Overview of the inzicht app]
		inzicht_app[Inzicht App]

		inzicht_backend_1[Inzicht Backend 1]
		inzicht_backend_2[Inzicht Backend 2]
		inzicht_backend_3[Inzicht backend N]

		inzicht_app --> |http|inzicht_backend_1
		inzicht_app --> |http|inzicht_backend_2
		inzicht_app --> |http|inzicht_backend_3
	end
```

### The problem
Due to the requirement that the lookup endpoint cannot be behind an authentication system like DigiD, the lookup endpoint is vulnerable to abuse by querying adjacent identifiers.
This could lead to scenarios where someone can check if their neighbour's identifier has had contact with certain government bodies, such as the judicial system or police, which they may wish to keep confidential.
