# ADR: Gebruik van Jaeger als storage backend voor opslag dataverwerkingen

## Status

Aangenomen

## Context

Een dataverwerking betreft het verwerken van een persoonsgeven (zowel lezen als schrijven). Om deze verwerkingen inzichtelijk te maken moeten deze verwekringen worden opgeslagen. Een dataverwerking wordt in de applicatie geexporteerd als een trace. Een trace is een verzameling van events die de verwerking van een dataverwerking beschrijven. Deze events bevatten informatie over de start- en eindtijd van de verwerking, de duur van de verwerking, eventuele input- en outputgegevens en enkele metadata, zoals de grondslag voor de verwerking.

Dit document beschrijft de keuze voor Jaeger om de dataverwerkingen op te slaan.

## Beslissing

We hebben besloten om Jaeger te gebruiken voor het opslaan van traces van onze dataverwerkingen. Jaeger is een open-source tool voor distributed tracing, ontworpen om de monitoring en troubleshooting van microservices-gebaseerde gedistribueerde systemen te vergemakkelijken.

## Overwegingen

De volgende punten hebben bijgedragen aan deze beslissing:

- **Compatibiliteit met OpenTelemetry**: Jaeger is compatibel met OpenTelemetry, een observability-framework dat wordt ondersteund door een breed scala aan instrumentatiebibliotheken. Dit maakt het eenvoudig om Jaeger te integreren in onze bestaande systemen zonder uitgebreide aanpassingen.

- **Schaalbaarheid**: Jaeger is ontworpen om te schalen met onze behoeften, van een enkele service tot een complex systeem met honderden (micro)services. Het kan eenvoudig worden uitgebreid om grote volumes van trace-gegevens te verwerken en op te slaan.

- **Grafana-ondersteuning voor Jaeger**: Grafana - een populaire tool voor monitoring en visualisatie - biedt native ondersteuning voor Jaeger-traces. Dit maakt het eenvoudig om onze trace-gegevens te visualiseren en te analyseren met behulp van de vertrouwde Grafana-interface.

- **Community en Ondersteuning**: Jaeger heeft een actieve open-source community en wordt ondersteund door meerdere grote technologiebedrijven. Dit zorgt voor regelmatige updates, nieuwe features en een betrouwbaar supportnetwerk.

- **Kosten**: Als open-source oplossing zijn er geen licentiekosten verbonden aan het gebruik van Jaeger. 
  Dit maakt het een kosteneffectieve keuze voor onze trace-opslagbehoeften.

- **Lock-in**: Een vendor lock-in wordt vermeden omdat het 1) open source is en 2) compatibel is met OpenTelemetry. 
  OpenTelemetry ondersteunt diverse _exporters_ waardoor het eenvoudig is om te switchen naar een andere storage 
  backend, zonder aanpassingen in de applicatiecode (enkel in de OTLP collector pipeline). Dit geldt overigens niet 
  voor de te ontwikkelen Inzicht API; die communiceert met de Jaeger API en zal bij het wisselen van storage backend aanpassingen nodig hebben.

- **API**: Jaeger biedt een krachtige API voor het opvragen en analyseren van trace-gegevens. Dit maakt het eenvoudig om de dataverwerkingen op te laten vragen vanuit andere systemen, zoals een dashboard voor de burger.

## Acties

Om de implementatie van Jaeger te faciliteren, zullen we de volgende stappen ondernemen:

1. **Deployment van Jaeger**: De fictieve organisaties uit de migratieketen krijgen alleen een eigen Jaeger-instantie. Zo blijft de data bij de bron.
2. **Aansluiten Inzicht API**: Om data te ontrekken uit Jaeger wordt er een Inzicht API ontwikkeld. Deze API communiceert met de Jaeger API en zet de data om naar een formaat dat geschikt is voor de frontend.
3. **OLTP collector pipeline**: Er wordt een OTLP exporter geconfigureerd die de data naar Jaeger stuurt.
4. **Ansluiten Grafana**: Grafana wordt aangesloten op Jaeger om de data te visualiseren.
