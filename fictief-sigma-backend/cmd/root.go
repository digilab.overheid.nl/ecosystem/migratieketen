package cmd

import (
	"log/slog"
	"os"

	"github.com/alecthomas/kong"
)

type Context struct {
	Logger         slog.Logger
	PSQLSchemaName string
}

var cli struct {
	Debug bool `env:"MK_DEBUG" default:"false" optional:"" name:"debug" help:"Enable debug mode."`

	Serve   ServeCmd   `cmd:"serve" help:"Startup server."`
	Migrate MigrateCmd `cmd:"migrate" help:"Run migrations."`
}

func Run() error {
	// Parse the command line.
	ctx := kong.Parse(&cli)

	// Setup the logger.
	var logLevel slog.Level
	if cli.Debug {
		logLevel = slog.LevelDebug
	} else {
		logLevel = slog.LevelInfo
	}
	logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
		Level: logLevel,
	}))

	// Call the Run() method of the selected parsed command.
	err := ctx.Run(&Context{Logger: *logger, PSQLSchemaName: "sigma"})

	return err
}
