package cmd

import (
	"fmt"

	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage"
)

type MigrateCmd struct {
	Up   UpCmd   `cmd:"" help:"Migrate the API."`
	Init InitCmd `cmd:"" help:"Init the API."`
}

type UpCmd struct {
	PostgresDSN string `env:"MK_POSTGRES_DSN" required:"" name:"postgres-dsn" help:"Postgres Connection URL."`
}

type InitCmd struct {
	PostgresDSN string `env:"MK_POSTGRES_DSN" required:"" name:"postgres-dsn" help:"Postgres Connection URL."`
}

func (opt *UpCmd) Run(ctx *Context) error {
	ctx.Logger.Info("migrate up", "postgres-dsn", opt.PostgresDSN)
	if err := storage.PostgresPerformMigrations(opt.PostgresDSN, ctx.PSQLSchemaName); err != nil {
		return fmt.Errorf("failed to perform migrations: %w", err)
	}

	ctx.Logger.Info("Migrate up successful")
	return nil
}

func (opt *InitCmd) Run(ctx *Context) error {
	ctx.Logger.Info("migrate init", "postgres-dsn", opt.PostgresDSN)
	if err := storage.MigrateInit(ctx.PSQLSchemaName, opt.PostgresDSN); err != nil {
		return fmt.Errorf("failed to migrate init: %w", err)
	}
	ctx.Logger.Info("Migrate init successful")
	return nil
}
