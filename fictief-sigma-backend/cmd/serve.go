package cmd

import (
	"context"
	"errors"
	"net/http"

	_ "github.com/alecthomas/kong-yaml"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/service"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/config"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/process"
)

type ServeCmd struct {
	PostgresDSN          string `env:"MK_POSTGRES_DSN" required:"" name:"postgres-dsn" help:"Postgres Connection URL."`
	BackendListenAddress string `env:"MK_BACKEND_LISTEN_ADDRESS" default:":8080" name:"backend-listen-address" help:"Address to listen  on."`

	GlobalConfig       string `env:"MK_GLOBAL_CONFIG_PATH"  default:"../config/global.yaml" name:"global-config" help:"Path to the config file."`
	OrganizationConfig string `env:"MK_CONFIG_PATH" required:"" name:"organization-config" help:"Path to the config file."`

	NotifierEndpoint     string `env:"MK_NOTIFIERDOMAIN_ENDPOINT" required:"" name:"notifier-domain-endpoint" help:"Notifier domain endpoint."`
	NotifierFscGrantHash string `env:"MK_NOTIFIERDOMAIN_FSCGRANTHASH" required:"" name:"notifier-domain-fsc-grant-hash" help:"Notifier domain fsc grant hash."`
}

func (opt *ServeCmd) Run(ctx *Context) error {
	proc := process.New()
	orgConf, err := config.GetOrgConfig(opt.OrganizationConfig)
	if err != nil {
		return err
	}

	globalConf, err := config.GetGlobalConfig(opt.GlobalConfig)
	if err != nil {
		return err
	}

	config := config.Config{
		OrganizationConfig: *orgConf,
		GlobalConfig:       *globalConf,
		NotifierDomain: config.Domain{
			Endpoint:     opt.NotifierEndpoint,
			FscGrantHash: opt.NotifierFscGrantHash,
		},
		BackendListenAddress: opt.BackendListenAddress,
		PostgresDsn:          opt.PostgresDSN,
	}

	logger := ctx.Logger.With("application", "http_server")
	logger.Info("Starting fictief sigma backend", "config", config)

	if err := storage.MigrateInit(ctx.PSQLSchemaName, opt.PostgresDSN); err != nil {
		logger.Error("migrate init failed", "err", err)
		return err
	}
	logger.Info("migrate init done")

	if err := storage.PostgresPerformMigrations(opt.PostgresDSN, ctx.PSQLSchemaName); err != nil {
		logger.Error("failed to migrate db", "err", err)
		return err
	}

	db, err := storage.New(opt.PostgresDSN)
	if err != nil {
		logger.Error("failed to connect to the database", "err", err)
		return err
	}

	svc, err := service.New(logger, &config, db)
	if err != nil {
		logger.Error("faile to setup new service", "err", err)
		return err
	}

	app := application.New(logger, &config, svc)

	app.Router()

	logger.Info("Starting server", "address", opt.BackendListenAddress)

	go func() {
		if err := app.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			logger.Error("listen and serve failed", "err", err)
		}
	}()

	proc.Wait()

	logger.Info("shutting down server")

	// Shutdown application
	shutdownCtx := context.Background()

	app.Shutdown(shutdownCtx)

	svc.Shutdown(shutdownCtx)

	logger.Info("shutdown finished")

	return nil
}
