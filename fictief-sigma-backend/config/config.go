package config

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

type AttributePermissions struct {
	Get []string `yaml:"get"` // List of attribute names. If it contains a '*' element, then all attributes in the rubriek/proces are allowed. Also below
	Set []string `yaml:"set"`
}

type Role struct {
	Name        string `yaml:"name"`
	Label       string `yaml:"label"`
	Permissions struct {
		BVVWriteAccess bool `yaml:"bvvWriteAccess"`

		Rubrieken map[string]AttributePermissions `yaml:"rubrieken"`   // Map keys are the rubriek names
		Processen map[string]AttributePermissions `yaml:"permissions"` // Map keys are the proces names
	}
}

type Attribute struct {
	Name             string      `yaml:"name"`
	Label            string      `yaml:"label"`
	Type             string      `yaml:"type"`
	Multiple         bool        `yaml:"multiple"` // For types 'select' and 'file'
	Default          interface{} `yaml:"default"`
	AlternativeNames []string    `yaml:"alternativeNames"`

	// For number types
	Min  *interface{} `yaml:"min"`
	Max  *interface{} `yaml:"max"`
	Step *interface{} `yaml:"step"`

	// For select, radios, and checkboxes types
	Options []struct {
		Name  string `yaml:"name"`
		Label string `yaml:"label"`
	} `yaml:"options"`
}

type ProcesStatus struct {
	Name  string `yaml:"name"`
	Label string `yaml:"label"`
}

type Rubriek struct {
	Name  string `yaml:"name"`
	Label string `yaml:"label"`

	Attributes []Attribute `yaml:"attributes"`
}

type Proces struct {
	Name     string         `yaml:"name"`
	Label    string         `yaml:"label"`
	Statuses []ProcesStatus `yaml:"statuses"`

	Attributes []Attribute `yaml:"attributes"`
}

type GlobalConfig struct {
	Rubrieken []Rubriek `yaml:"rubrieken"`

	Processen []Proces `yaml:"processen"`
}

type OrganizationConfig struct {
	Organization struct {
		Name           string `yaml:"name"`
		BVVWriteAccess bool   `yaml:"bvvWriteAccess"`
	} `yaml:"organization"`

	Roles []Role

	ListenAddress string `yaml:"listenAddress"`
}

type Config struct {
	GlobalConfig         `mapstructure:",squash"`
	OrganizationConfig   `mapstructure:",squash"`
	Debug                bool
	BackendListenAddress string
	PostgresDsn          string `mapstructure:"POSTGRES_DSN"`
	NotifierDomain       Domain
}

type Domain struct {
	Endpoint     string
	FscGrantHash string
}

func readConfig(path string, cfg any) error {
	file, err := os.ReadFile(path)
	if err != nil {
		return fmt.Errorf("error reading config file: %w", err)
	}

	if err := yaml.Unmarshal(file, cfg); err != nil {
		return fmt.Errorf("unmarshalling config failed: %w", err)
	}

	return nil
}

func GetOrgConfig(path string) (*OrganizationConfig, error) {
	conf := OrganizationConfig{}

	if err := readConfig(path, &conf); err != nil {
		return nil, err
	}

	return &conf, nil
}
func GetGlobalConfig(path string) (*GlobalConfig, error) {
	conf := GlobalConfig{}

	if err := readConfig(path, &conf); err != nil {
		return nil, err
	}

	return &conf, nil
}
