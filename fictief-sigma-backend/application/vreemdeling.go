package application

import (
	"context"

	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
)

// ReadVreemdelingById implements api.StrictServerInterface.
func (app *Application) ReadVreemdelingById(ctx context.Context, request api.ReadVreemdelingByIdRequestObject) (api.ReadVreemdelingByIdResponseObject, error) {
	vreemdeling, err := app.servicer.ReadVreemdelingByID(ctx, request.VreemdelingId, request.Params.Attributes...)
	if err != nil {
		return nil, err
	}

	vwlID := uuid.New()
	if request.Params.FSCVWLVerwerkingsSpan != nil {
		vwlID = *request.Params.FSCVWLVerwerkingsSpan
	}

	return api.ReadVreemdelingById200JSONResponse{
		VreemdelingResponseJSONResponse: api.VreemdelingResponseJSONResponse{
			Body: api.VreemdelingResponse{
				Data: vreemdeling,
			},
			Headers: api.VreemdelingResponseResponseHeaders{
				FSCVWLSysteem:               app.vwlSystem,
				FSCVWLVerantwoordelijke:     app.cfg.Organization.Name,
				FSCVWLVerwerker:             app.cfg.Organization.Name,
				FSCVWLVerwerkingsActiviteit: "https://example.com/activity/v0/vreemdeling-read",
				FSCVWLVerwerkingsSpan:       vwlID,
			},
		},
	}, nil
}
