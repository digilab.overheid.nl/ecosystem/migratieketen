package application

import (
	"context"

	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
)

// CreateObservationByVreemdelingenId implements api.StrictServerInterface.
func (app *Application) CreateObservationByVreemdelingenId(ctx context.Context, request api.CreateObservationByVreemdelingenIdRequestObject) (api.CreateObservationByVreemdelingenIdResponseObject, error) {
	observation, err := app.servicer.CreateObservationByVreemdelingenID(ctx, request.VreemdelingId, request.Body.Data)
	if err != nil {
		return api.CreateObservationByVreemdelingenId400JSONResponse{}, nil
	}

	vwlID := uuid.New()
	if request.Params.FSCVWLVerwerkingsSpan != nil {
		vwlID = *request.Params.FSCVWLVerwerkingsSpan
	}

	return api.CreateObservationByVreemdelingenId201JSONResponse{
		ObservationCreateResponseJSONResponse: api.ObservationCreateResponseJSONResponse{
			Body: api.ObservationCreateResponse{
				Data: observation,
			},
			Headers: api.ObservationCreateResponseResponseHeaders{
				FSCVWLSysteem:               app.vwlSystem,
				FSCVWLVerantwoordelijke:     app.cfg.Organization.Name,
				FSCVWLVerwerker:             app.cfg.Organization.Name,
				FSCVWLVerwerkingsActiviteit: "https://example.com/activity/v0/obseration-create",
				FSCVWLVerwerkingsSpan:       vwlID,
			},
		},
	}, nil
}

// DeleteObservationById implements api.StrictServerInterface.
func (app *Application) DeleteObservationById(ctx context.Context, request api.DeleteObservationByIdRequestObject) (api.DeleteObservationByIdResponseObject, error) {
	if err := app.servicer.DeleteObservationByID(ctx, request.ObservationId); err != nil {
		return api.DeleteObservationById400JSONResponse{}, nil
	}

	return api.DeleteObservationById204Response{}, nil
}
