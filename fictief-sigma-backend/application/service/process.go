package service

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage/queries/generated"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

// CreateProcesByVreemdelingenID implements Servicer.
func (service *Service) CreateProcessByVreemdelingenID(ctx context.Context, vreemdelingID string, params api.ProcesCreate) (api.Proces, error) {
	ctx, span := service.tracerStart(ctx, "createProcessByVreemdelingenID",
		trace.WithAttributes(
			attribute.String("dpl.core.user", fmt.Sprintf("vid:%s", vreemdelingID)),
		),
	)

	defer span.End()

	process, err := service.createProcessByVreemdelingenID(ctx, vreemdelingID, params)
	if err != nil {
		err := fmt.Errorf("create proces by vreemdelingen id: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return api.Proces{}, err
	}

	span.SetStatus(codes.Ok, "")

	return process, nil
}

// UpdateProcessByID implements Servicer.
func (service *Service) UpdateProcessByID(ctx context.Context, processID string, params api.ProcesUpdate) (api.Proces, error) {
	ctx, span := service.tracerStart(ctx, "updateProcesById")

	defer span.End()

	process, err := service.updateProcessByID(ctx, processID, params)
	if err != nil {
		err := fmt.Errorf("update proces by id: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return api.Proces{}, err
	}

	span.SetStatus(codes.Ok, "")

	return process, err
}

// ReadProcesByID implements Servicer.
func (service *Service) ReadProcessByID(ctx context.Context, processID string, attributes *[]string) (api.Proces, error) {
	ctx, span := service.tracerStart(ctx, "readProcesById")

	defer span.End()

	process, err := service.readProcessByID(ctx, processID, attributes)
	if err != nil {
		err := fmt.Errorf("read proces by id: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return api.Proces{}, err
	}

	span.SetStatus(codes.Ok, "")

	return process, nil
}

// ReadProcesses implements Servicer.
func (service *Service) ReadProcesses(ctx context.Context, attributes *[]string) ([]api.Proces, error) {
	ctx, span := service.tracerStart(ctx, "readProcessen")

	defer span.End()

	processes, err := service.readProcesses(ctx, attributes)
	if err != nil {
		err := fmt.Errorf("read processen: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	span.SetStatus(codes.Ok, "")

	return processes, nil
}

// ReadProcessesByVreemdelingID implements Servicer.
func (service *Service) ReadProcessesByVreemdelingID(ctx context.Context, vreemdelingID string, attributes *[]string) ([]api.Proces, error) {
	ctx, span := service.tracerStart(ctx, "readProcessenByVreemdelingenId",
		trace.WithAttributes(
			attribute.String("dpl.core.user", fmt.Sprintf("vid:%s", vreemdelingID)),
		),
	)

	defer span.End()

	processes, err := service.readProcessesByVreemdelingID(ctx, vreemdelingID, attributes)
	if err != nil {
		err := fmt.Errorf("processes get vreemdeling id: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	span.SetStatus(codes.Ok, "")

	return processes, nil
}

func (service *Service) createProcessByVreemdelingenID(ctx context.Context, vreemdelingID string, params api.ProcesCreate) (api.Proces, error) {
	if err := validateProcess(params, service.processes); err != nil {
		return api.Proces{}, fmt.Errorf("validation failed: %w", err)
	}

	count, err := service.storage.Queries.ProcessCount(ctx)
	if err != nil {
		return api.Proces{}, fmt.Errorf("process count failed: %s", err)
	}

	processID := fmt.Sprintf("%010d", count+1)

	id := uuid.New()

	params2 := &queries.ProcessCreateParams{
		ID:            id,
		ProcessID:     processID,
		VreemdelingID: vreemdelingID,
		Type:          params.Type,
		Status:        params.Status,
	}

	if err := service.storage.Queries.ProcessCreate(ctx, params2); err != nil {
		return api.Proces{}, fmt.Errorf("process create failed: %w", err)
	}

	attributes := make([]string, 0, len(params.Attributes))
	for _, attribute := range params.Attributes {
		if _, err := service.CreateAttributeByProcesID(ctx, processID, attribute); err != nil {
			return api.Proces{}, fmt.Errorf("create attribute by proces ID: %w", err)
		}

		attributes = append(attributes, attribute.Attribute)
	}

	data := map[string]any{
		"procesId":      id,
		"vreemdelingId": vreemdelingID,
	}

	if err := service.Notify(params.Type, ntCreate, data); err != nil {
		return api.Proces{}, fmt.Errorf("notify failed: %w", err)
	}

	proces, err := service.readProcessByID(ctx, id.String(), &attributes)
	if err != nil {
		return api.Proces{}, fmt.Errorf("read proces by id failed: %w", err)
	}

	return proces, nil
}

func (service *Service) updateProcessByID(ctx context.Context, processID string, params api.ProcesUpdate) (api.Proces, error) {
	process, err := service.readProcessByID(ctx, processID, nil)
	if err != nil {
		return api.Proces{}, fmt.Errorf("read proces by id: %w", err)
	}

	if err := validateProcesUpdate(params, service.processes[process.Type]); err != nil {
		return api.Proces{}, fmt.Errorf("validate proces update: %w", err)
	}

	if err := service.storage.Queries.ProcessUpdate(ctx, &queries.ProcessUpdateParams{
		Status:    params.Status,
		ProcessID: process.ProcesId,
	}); err != nil {
		return api.Proces{}, fmt.Errorf("process update: %w", err)
	}

	data := map[string]any{
		"procesId":      process.Id,
		"vreemdelingId": process.VreemdelingId,
	}

	if err := service.Notify(process.Type, ntUpdate, data); err != nil {
		return api.Proces{}, fmt.Errorf("notify failed: %w", err)
	}

	process, err = service.ReadProcessByID(ctx, processID, nil)
	if err != nil {
		return api.Proces{}, fmt.Errorf("read proces by id: %w", err)
	}

	return process, nil
}

func (service *Service) readProcessByID(ctx context.Context, processID api.PathProcesId, attributes *[]string) (api.Proces, error) {
	var record *queries.SigmaProcess

	if id, err := uuid.Parse(processID); err == nil {
		if record, err = service.storage.Queries.ProcessGetID(ctx, id); err != nil {
			return api.Proces{}, fmt.Errorf("process get failed: %w", err)
		}
	} else {
		if record, err = service.storage.Queries.ProcessGetPID(ctx, processID); err != nil {
			return api.Proces{}, fmt.Errorf("process get failed: %w", err)
		}
	}

	process := ToProcess(record, service.source)
	if attributes != nil {
		params := &queries.AttributeListParams{
			ProcessID: record.ID,
			Column2:   *attributes,
		}

		attributeRecords, err := service.storage.Queries.AttributeList(ctx, params)
		if err != nil {
			return api.Proces{}, err
		}

		process.Attributes = ToAttributes(attributeRecords, service.source)
	}

	return process, nil
}

func (service *Service) readProcesses(ctx context.Context, attributes *[]string) ([]api.Proces, error) {
	records, err := service.storage.Queries.ProcessesGet(ctx)
	if err != nil {
		return nil, fmt.Errorf("processes get failed: %w", err)
	}

	processes := make([]api.Proces, 0, len(records))
	for idx := range records {
		process := ToProcess(records[idx], service.source)

		if attributes != nil {
			params := &queries.AttributeListParams{
				ProcessID: records[idx].ID,
				Column2:   *attributes,
			}

			attributeRecords, err := service.storage.Queries.AttributeList(ctx, params)
			if err != nil {
				return nil, fmt.Errorf("attribute list: %w", err)
			}

			process.Attributes = ToAttributes(attributeRecords, service.source)
		}

		processes = append(processes, process)
	}

	return processes, nil
}

func (service *Service) readProcessesByVreemdelingID(ctx context.Context, vreemdelingID string, attributes *[]string) ([]api.Proces, error) {
	records, err := service.storage.Queries.ProcessesGetVreemdelingId(ctx, vreemdelingID)
	if err != nil {
		return nil, fmt.Errorf("processes get vreemdelingId: %w", err)
	}

	processes := make([]api.Proces, 0, len(records))
	for idx := range records {
		process := ToProcess(records[idx], service.source)

		if attributes != nil {
			params := &queries.AttributeListParams{
				ProcessID: records[idx].ID,
				Column2:   *attributes,
			}

			attributeRecords, err := service.storage.Queries.AttributeList(ctx, params)
			if err != nil {
				return nil, fmt.Errorf("attribute list: %w", err)
			}

			process.Attributes = ToAttributes(attributeRecords, service.source)
		}

		processes = append(processes, process)
	}

	return processes, nil
}

func ToProcess(record *queries.SigmaProcess, source string) api.Proces {
	var deletedAt *time.Time
	if record.DeletedAt.Valid {
		deletedAt = &record.DeletedAt.Time
	}

	return api.Proces{
		CreatedAt:     record.CreatedAt,
		DeletedAt:     deletedAt,
		Id:            record.ID,
		ProcesId:      record.ProcessID,
		VreemdelingId: record.VreemdelingID,
		Status:        record.Status,
		Type:          record.Type,
		Source:        source,
	}
}
