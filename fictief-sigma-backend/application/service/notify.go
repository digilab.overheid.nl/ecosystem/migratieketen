package service

import (
	"context"
	"fmt"
	"net/http"

	"github.com/google/uuid"

	notification "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api"
)

type NotifyType string

const (
	ntCreate NotifyType = "create"
	ntUpdate NotifyType = "update"
)

func (service *Service) Notify(subject string, ntype NotifyType, data map[string]any) error {
	t := ""
	switch ntype {
	case ntCreate:
		t = "com.migratieketen.proces.toevoegen"
	case ntUpdate:
		t = "com.migratieketen.proces.wijzigen"
	}

	dct := "application/json"

	resp, err := service.notifier.PublishNotification(context.Background(), notification.PublishNotificationJSONRequestBody{
		Data: notification.CloudEvent{
			Data:            &data,
			Datacontenttype: &dct,
			Subject:         &subject,
			Id:              uuid.NewString(),
			Source:          fmt.Sprintf("migratieketen/%s", service.cfg.Organization.Name),
			Specversion:     "1.0",
			Type:            t,
		},
	})
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("publish notification failed: statuscode %d status %s", resp.StatusCode, resp.Status)
	}

	return nil
}
