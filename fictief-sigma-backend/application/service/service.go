package service

import (
	"context"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/config"
	tracer "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/trace"
	notification "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

var _ Servicer = &Service{}

type Proces struct {
	Attributes map[string]config.Attribute
	Statuses   map[string]config.ProcesStatus
	config.Proces
}

type Service struct {
	tracer                trace.Tracer
	logger                *slog.Logger
	storage               *storage.Database
	cfg                   *config.Config
	attributes            map[string]config.Attribute
	processes             map[string]Proces
	notifier              *notification.Client
	supportedAttributes   map[string]config.Attribute
	processingActivityIDs map[string]uuid.UUID
	source                string
	vwlSystem             string
	shutdownFns           []func(context.Context) error
}

func New(logger *slog.Logger, cfg *config.Config, storage *storage.Database) (*Service, error) {
	notifier, err := notification.NewClient(cfg.NotifierDomain.Endpoint, notification.WithRequestEditorFn(func(ctx context.Context, req *http.Request) error {
		tracer.Inject(ctx, req.Header)
		return nil
	}))
	if err != nil {
		return nil, fmt.Errorf("could not create new notification client: %w", err)
	}

	processes := setupProcessen(logger, cfg.Processen)

	attributes := setupAttributes(logger, cfg.Rubrieken)

	supportedAttributes := setupSupportedAttributes(logger, cfg.Rubrieken)

	// Set up OpenTelemetry.
	otelShutdown, err := setupOTelSDK(context.TODO())
	if err != nil {
		return nil, fmt.Errorf("setup otel sdk: %w", err)
	}

	return &Service{
		logger:              logger,
		storage:             storage,
		cfg:                 cfg,
		source:              cfg.Organization.Name,
		attributes:          attributes,
		processes:           processes,
		notifier:            notifier,
		vwlSystem:           "sigma v0",
		tracer:              otel.Tracer("sigma"),
		supportedAttributes: supportedAttributes,
		processingActivityIDs: map[string]uuid.UUID{
			"createObservationByVreemdelingenId": uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"findObservation":                    uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"readVreemdelingById":                uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"findVreemdeling":                    uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"createProcessByVreemdelingenID":     uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"updateProcesById":                   uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"readProcessen":                      uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"readProcesById":                     uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"readProcessenByVreemdelingenId":     uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"createAttributeByProcesId":          uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"deleteAttributeById":                uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
		},
		shutdownFns: []func(context.Context) error{
			otelShutdown,
		},
	}, nil
}

func (service *Service) tracerStart(ctx context.Context, name string, opts ...trace.SpanStartOption) (context.Context, trace.Span) {
	var parent tracer.ProcessingContext
	if p := tracer.ProcessingOperationFromContext(ctx); p.IsValid() {
		parent = *p
	}

	if opts == nil {
		opts = make([]trace.SpanStartOption, 0, 2)
	}

	opts = append(opts,
		trace.WithTimestamp(time.Now()),
		trace.WithAttributes(
			attribute.String("dpl.rva.activity.id", service.processingActivityIDs[name].String()),
		))

	if parent.IsValid() && parent.Foreign() {
		opts = append(opts, trace.WithAttributes(
			attribute.String("dpl.rva.foreign.trace_id", parent.TraceID().String()),
			attribute.String("dpl.rva.foreign.operation_id", parent.OperationID().String()),
		))
	}

	ctx, span := service.tracer.Start(ctx, name, opts...)

	op := tracer.New(tracer.TraceID(span.SpanContext().TraceID()), tracer.SpanID(span.SpanContext().SpanID()))
	ctx = tracer.ContextWithProcessingOperation(ctx, op)

	return ctx, span
}

func setupSupportedAttributes(logger *slog.Logger, rubrieken []config.Rubriek) map[string]config.Attribute {
	attributes := make(map[string]config.Attribute)
	for _, rubriek := range rubrieken {
		for _, attr := range rubriek.Attributes {
			logger.Debug("Added supported attribute", "attr_name", attr.Name)
			attributes[attr.Name] = attr
		}
	}

	return attributes
}

func setupProcessen(logger *slog.Logger, procs []config.Proces) map[string]Proces {
	processes := map[string]Proces{}

	for _, process := range procs {
		attributes := map[string]config.Attribute{}
		logger.Debug("Added supported process", "process_name", process.Name)

		for _, attribute := range process.Attributes {
			logger.Debug("Added supported process attribute", "process_name", process.Name, "attr_name", attribute.Name)
			attributes[attribute.Name] = attribute
		}

		statusen := map[string]config.ProcesStatus{}
		for _, status := range process.Statuses {
			logger.Debug("Added supported process status", "process_name", process.Name, "status_name", status.Name)
			statusen[status.Name] = status
		}

		processes[process.Name] = Proces{
			Proces:     process,
			Attributes: attributes,
			Statuses:   statusen,
		}
	}

	return processes
}

func setupAttributes(logger *slog.Logger, rubrieken []config.Rubriek) map[string]config.Attribute {
	attributes := map[string]config.Attribute{}

	for _, rubriek := range rubrieken {
		for _, attribute := range rubriek.Attributes {
			attributes[attribute.Name] = attribute
		}
	}

	return attributes
}

func (service *Service) Shutdown(ctx context.Context) error {
	for _, fns := range service.shutdownFns {
		if err := fns(ctx); err != nil {
			service.logger.Error("service shutdown", "err", err)
		}
	}

	return nil
}

type Servicer interface {
	ReadVreemdelingByID(ctx context.Context, vreemdelingID string, attributes ...string) (api.Vreemdeling, error)

	CreateObservationByVreemdelingenID(ctx context.Context, vreemdelingID string, params api.ObservationCreate) (api.Observation, error)
	DeleteObservationByID(ctx context.Context, observationID uuid.UUID) error

	CreateProcessByVreemdelingenID(ctx context.Context, vreemdelingID string, params api.ProcesCreate) (api.Proces, error)
	UpdateProcessByID(ctx context.Context, processID string, params api.ProcesUpdate) (api.Proces, error)
	ReadProcessByID(ctx context.Context, procesID string, attributes *api.QueryAttributes) (api.Proces, error)
	ReadProcesses(ctx context.Context, attributes *api.QueryAttributes) ([]api.Proces, error)
	ReadProcessesByVreemdelingID(ctx context.Context, vreemdelingID string, attributes *api.QueryAttributes) ([]api.Proces, error)

	CreateAttributeByProcesID(ctx context.Context, processID string, params api.ObservationCreate) (api.Observation, error)
	DeleteAttributeByID(ctx context.Context, attributeID uuid.UUID) error
}
