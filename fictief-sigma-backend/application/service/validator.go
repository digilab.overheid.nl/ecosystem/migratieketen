package service

import (
	"fmt"
	"time"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/config"
)

// Compile time check if generated ObservationCreateValue order is not changed.
// If one or more of the lines below dont compile some order has been mixed up in the OpenAPI spec
var (
	_ map[string]interface{} = api.Value0{}
	_ string                 = api.Value1("")
	_ bool                   = api.Value2(false)
	_ []interface{}          = api.Value3{}
	_ float32                = api.Value4(0.0)
)

func validateDate(_ config.Attribute, value api.Value) error {
	v, err := value.AsValue1()
	if err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	if _, err := time.Parse("2006-01-02", v); err != nil {
		return fmt.Errorf("value could not be parsed: %s", err)
	}

	return nil
}

func validateDatetime(_ config.Attribute, value api.Value) error {
	v, err := value.AsValue1()
	if err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	if _, err := time.Parse(time.RFC3339, v); err != nil {
		return fmt.Errorf("value could not be parsed: %s", err)
	}

	return nil
}

func validateString(_ config.Attribute, value api.Value) error {
	if _, err := value.AsValue1(); err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	return nil
}

func validateNumber(attribute config.Attribute, value api.Value) error {
	v, err := value.AsValue4()
	if err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	if attribute.Min != nil {
		if v < float32((*attribute.Min).(int)) {
			return fmt.Errorf("value less then allowed: %v", *attribute.Min)
		}
	}

	if attribute.Max != nil {
		if v > float32((*attribute.Max).(int)) {
			return fmt.Errorf("value more than allowed: %v", *attribute.Max)
		}
	}

	return nil
}

func validateBoolean(_ config.Attribute, value api.Value) error {
	if _, err := value.AsValue2(); err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	return nil
}

func validateCheckboxes(attribute config.Attribute, value api.Value) error {
	values, err := value.AsValue3()
	if err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	for _, value := range values {
		found := false

		for _, option := range attribute.Options {
			if option.Name == value {
				found = true
				break
			}
		}

		if !found {
			return fmt.Errorf("value is not valid: %s", value)
		}
	}

	return nil
}

func validateRadio(attribute config.Attribute, value api.Value) error {
	val, err := value.AsValue1()
	if err != nil {
		return fmt.Errorf("value '%v' cannot be parsed: %w", value, err)
	}

	found := false

	for _, option := range attribute.Options {
		if option.Name == val {
			found = true
			break
		}
	}

	if !found {
		return fmt.Errorf("value is not valid: %s", value)
	}

	return nil
}

func validateType(attribute config.Attribute, value api.Value) error {
	switch attribute.Type {
	case "text", "textarea":
		return validateString(attribute, value)
	case "number":
		return validateNumber(attribute, value)
	case "boolean":
		return validateBoolean(attribute, value)
	case "checkboxes":
		return validateCheckboxes(attribute, value)
	case "radios":
		return validateRadio(attribute, value)
	case "select":
		if attribute.Multiple {
			return validateCheckboxes(attribute, value)
		} else {
			return validateRadio(attribute, value)
		}
	case "date":
		return validateDate(attribute, value)
	case "datetime":
		return validateDatetime(attribute, value)
	default:
		return fmt.Errorf("unknown datatype")
	}
}

func validateObservation(data api.ObservationCreate, attributes map[string]config.Attribute) error {
	attribute, ok := attributes[data.Attribute]
	if !ok {
		return fmt.Errorf("attribute does not exists")
	}

	if err := validateType(attribute, data.Value); err != nil {
		return err
	}

	return nil
}

func validateProcess(data api.ProcesCreate, processes map[string]Proces) error {
	process, ok := processes[data.Type]
	if !ok {
		return fmt.Errorf("process '%s' does not exists", data.Type)
	}

	if _, ok := process.Statuses[data.Status]; !ok {
		return fmt.Errorf("status '%s' does not exists in process '%s'", data.Status, process.Label)
	}

	for _, attribute := range data.Attributes {
		attr, ok := process.Attributes[attribute.Attribute]
		if !ok {
			return fmt.Errorf("attribute '%s' does not exists in process '%s'", attribute.Attribute, process.Label)
		}

		if err := validateType(attr, attribute.Value); err != nil {
			return fmt.Errorf("validating failed for '%s': %w", attr.Label, err)
		}
	}
	return nil
}

func validateAttribute(data api.ObservationCreate, process Proces) error {
	attribute, ok := process.Attributes[data.Attribute]
	if !ok {
		return fmt.Errorf("attribute '%s' does not exists in process '%s'", data.Attribute, process.Label)
	}

	if err := validateType(attribute, data.Value); err != nil {
		return fmt.Errorf("validation failed for '%s': %w", attribute.Label, err)
	}

	return nil
}

func validateProcesUpdate(data api.ProcesUpdate, process Proces) error {
	if _, ok := process.Statuses[data.Status]; !ok {
		return fmt.Errorf("status '%s' does not exists in process '%s'", data.Status, process.Label)
	}

	return nil
}
