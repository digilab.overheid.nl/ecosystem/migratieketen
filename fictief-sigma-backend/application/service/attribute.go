package service

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage/queries/generated"
	"go.opentelemetry.io/otel/codes"
)

func (service *Service) createAttributeByProcesID(ctx context.Context, procesID string, data api.ObservationCreate) (uuid.UUID, error) {
	process, err := service.readProcessByID(ctx, procesID, nil)
	if err != nil {
		return uuid.Nil, fmt.Errorf("process get failed: %w", err)
	}

	if err := validateAttribute(data, service.processes[process.Type]); err != nil {
		return uuid.Nil, fmt.Errorf("validation failed: %w", err)
	}

	value, err := json.Marshal(data.Value)
	if err != nil {
		return uuid.Nil, fmt.Errorf("attribute marshal failed: %w", err)
	}

	id := uuid.New()

	params := &queries.AttributeCreateParams{
		ID:        id,
		ProcessID: process.Id,
		Attribute: data.Attribute,
		Value:     value,
	}

	if err := service.storage.Queries.AttributeCreate(ctx, params); err != nil {
		return uuid.Nil, fmt.Errorf("attribute create failed: %w", err)
	}

	return id, nil
}

// CreateAttributeByProcesID implements Servicer.
func (service *Service) CreateAttributeByProcesID(ctx context.Context, processID string, params api.ObservationCreate) (api.Observation, error) {
	ctx, span := service.tracerStart(ctx, "createAttributeByProcesId")
	defer span.End()

	_, err := service.createAttributeByProcesID(ctx, processID, params)
	if err != nil {
		err := fmt.Errorf("create attribute by proces id: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return api.Observation{}, err
	}

	// observation, err := service.FindObservation(ctx, id)
	// if err != nil {
	// err := fmt.Errorf("find observation: %w", err)
	// span.SetStatus(codes.Error, err.Error())
	// return api.Observation{}, err
	// }

	span.SetStatus(codes.Ok, "")

	return api.Observation{}, nil
}

// DeleteAttributeByID implements Servicer.
func (app *Service) DeleteAttributeByID(ctx context.Context, attributeID uuid.UUID) error {
	ctx, span := app.tracerStart(ctx, "deleteAttributeById")
	defer span.End()

	if err := app.storage.Queries.AttributeSoftDelete(ctx, attributeID); err != nil {
		span.SetStatus(codes.Error, fmt.Sprintf("attribute soft delete: %s", err))
		return fmt.Errorf("process attributes soft delete failed: %w", err)
	}

	span.SetStatus(codes.Ok, "")

	return nil
}

func ToAttributes(records []*queries.SigmaProcessAttribute, source string) *map[string]api.Attribute {
	attributes := map[string]api.Attribute{}

	for i, spa := range records {
		a := attributes[spa.Attribute]

		observation := ToAttribute(records[i], source)
		a.Observations = append(a.Observations, *observation)
		a.Value = observation.Value
		attributes[spa.Attribute] = a
	}

	return &attributes
}

func ToAttribute(record *queries.SigmaProcessAttribute, source string) *api.Observation {
	var invalidatedAt *time.Time
	if record.DeletedAt.Valid {
		invalidatedAt = &record.DeletedAt.Time
	}

	return &api.Observation{
		Attribute:     record.Attribute,
		CreatedAt:     record.CreatedAt,
		InvalidatedAt: invalidatedAt,
		Id:            record.ID,
		Source:        source,
		Value:         record.Value,
	}
}
