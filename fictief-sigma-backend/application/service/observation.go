package service

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage/queries/generated"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/config"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

func (service *Service) FindObservation(ctx context.Context, id uuid.UUID) (api.Observation, error) {
	ctx, span := service.tracerStart(ctx, "findObservation")

	defer span.End()

	observation, err := service.findObservation(ctx, id)
	if err != nil {
		err := fmt.Errorf("find observation: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return api.Observation{}, err
	}

	span.SetStatus(codes.Ok, "")

	return observation, nil
}

// CreateObservationByVreemdelingenId implements Servicer.
func (service *Service) CreateObservationByVreemdelingenID(ctx context.Context, vreemdelingID string, params api.ObservationCreate) (api.Observation, error) {
	ctx, span := service.tracerStart(ctx, "createObservationByVreemdelingenId",
		trace.WithAttributes(
			attribute.String("dpl.core.user", fmt.Sprintf("vid:%s", vreemdelingID)),
		),
	)

	defer span.End()

	observationID, err := service.createObservationByVreemdelingenID(ctx, vreemdelingID, params)
	if err != nil {
		err := fmt.Errorf("create observation by vreemdelingen id: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return api.Observation{}, err
	}

	span.SetStatus(codes.Ok, "")

	observation, err := service.FindObservation(ctx, observationID)
	if err != nil {
		return api.Observation{}, err
	}

	return observation, nil
}

// DeleteObservationById implements Servicer.
func (service *Service) DeleteObservationByID(ctx context.Context, observationID uuid.UUID) error {
	if err := service.storage.Queries.ObservationInvalidate(ctx, observationID); err != nil {
		return fmt.Errorf("attribute invalidate failed: %w", err)
	}

	return nil
}

func (service *Service) findObservation(ctx context.Context, id uuid.UUID) (api.Observation, error) {
	record, err := service.storage.Queries.ObservationGet(ctx, id)
	if err != nil {
		return api.Observation{}, fmt.Errorf("observation get: %w", err)
	}

	observation, err := ToObservation(record, service.source)
	if err != nil {
		return api.Observation{}, fmt.Errorf("to observation: %w", err)
	}

	return observation, nil
}

func (service *Service) createObservationByVreemdelingenID(ctx context.Context, vreemdelingID string, data api.ObservationCreate) (uuid.UUID, error) {
	if err := validateObservation(data, service.attributes); err != nil {
		return uuid.Nil, fmt.Errorf("validation failed: %w", err)
	}

	value, err := json.Marshal(data.Value)
	if err != nil {
		return uuid.Nil, fmt.Errorf("marshal attribute value failed: %w", err)
	}
	id := uuid.New()

	params := &queries.ObservationCreateParams{
		ID:            id,
		VreemdelingID: vreemdelingID,
		Attribute:     data.Attribute,
		Value:         value,
	}

	if err := service.storage.Queries.ObservationCreate(ctx, params); err != nil {
		return uuid.Nil, fmt.Errorf("attributes create failed: %w", err)
	}

	return id, nil
}

func ToObservationByAttribute(record *queries.SigmaObservation, source string, attrConfig config.Attribute) (api.Observation, error) {
	value := new(interface{})
	if err := json.Unmarshal(record.Value, value); err != nil {
		return api.Observation{}, fmt.Errorf("unmarshal attribute failed: %w", err)
	}

	var invalidatedAt *time.Time
	if record.DeletedAt.Valid {
		invalidatedAt = &record.DeletedAt.Time
	}

	return api.Observation{
		Attribute:     attrConfig.Name,
		CreatedAt:     record.CreatedAt,
		Id:            record.ID,
		InvalidatedAt: invalidatedAt,
		Source:        source,
		Value:         value,
	}, nil
}

func ToObservation(record *queries.SigmaObservation, source string) (api.Observation, error) {
	value := new(interface{})
	if err := json.Unmarshal(record.Value, value); err != nil {
		return api.Observation{}, fmt.Errorf("unmarshal attribute failed: %w", err)
	}

	var invalidatedAt *time.Time
	if record.DeletedAt.Valid {
		invalidatedAt = &record.DeletedAt.Time
	}

	return api.Observation{
		Attribute:     record.Attribute,
		CreatedAt:     record.CreatedAt,
		Id:            record.ID,
		InvalidatedAt: invalidatedAt,
		Source:        source,
		Value:         value,
	}, nil
}
