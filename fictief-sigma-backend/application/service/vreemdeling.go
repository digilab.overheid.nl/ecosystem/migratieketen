package service

import (
	"context"
	"fmt"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage/queries/generated"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/config"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"golang.org/x/exp/maps"
)

// ReadVreemdelingById implements Servicer.
func (service *Service) ReadVreemdelingByID(ctx context.Context, vreemdelingID string, attributes ...string) (api.Vreemdeling, error) {
	ctx, span := service.tracerStart(ctx, "findVreemdeling",
		trace.WithAttributes(
			attribute.String("dpl.rva.activity.id", service.processingActivityIDs["findVreemdeling"].String()),
		),
	)
	defer span.End()

	vreemdeling, err := service.readVreemdelingByID(ctx, vreemdelingID, attributes...)
	if err != nil {
		err := fmt.Errorf("read vreemdeling by id: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	span.SetStatus(codes.Ok, "")

	return vreemdeling, nil
}

func (service *Service) readVreemdelingByID(ctx context.Context, vreemdelingID string, attributes ...string) (api.Vreemdeling, error) {
	attributesIncludingAlts := make(map[string]config.Attribute)
	for _, providedAttr := range attributes {
		if configAttr, exists := service.supportedAttributes[providedAttr]; exists {
			attributesIncludingAlts[providedAttr] = configAttr
			for _, altName := range configAttr.AlternativeNames {
				attributesIncludingAlts[altName] = configAttr
			}
		}
	}

	params := &queries.ObservationListParams{
		VreemdelingID: vreemdelingID,
		Column2:       maps.Keys(attributesIncludingAlts),
	}

	records, err := service.storage.Queries.ObservationList(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("attribute list failed: %w", err)
	}

	vreemdeling := api.Vreemdeling{}
	for _, record := range records {
		observation, err := ToObservationByAttribute(record, service.source, attributesIncludingAlts[record.Attribute])
		if err != nil {
			return nil, err
		}

		v, ok := vreemdeling[observation.Attribute]
		if !ok {
			v.Observations = []api.Observation{}
		}

		v.Observations = append(v.Observations, observation)
		v.Value = observation.Value

		vreemdeling[observation.Attribute] = v
	}

	return vreemdeling, nil
}
