package application

import (
	"log/slog"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/service"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/config"
	trcMdw "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/trace/middleware"
)

type Application struct {
	*http.Server
	logger    *slog.Logger
	cfg       *config.Config
	servicer  service.Servicer
	vwlSystem string
}

func New(logger *slog.Logger, cfg *config.Config, servicer service.Servicer) Application {
	// Register the trace exporter with a TracerProvider, using a batch
	// span processor to aggregate spans before export.
	app := Application{
		Server: &http.Server{
			Addr: cfg.BackendListenAddress,
		},
		logger:    logger,
		cfg:       cfg,
		servicer:  servicer,
		vwlSystem: "sigma v0",
	}

	return app
}

func (app *Application) Router() {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			app.logger.Debug("Request headers", "headers", r.Header)
			next.ServeHTTP(w, r)
		})
	})
	r.Use(middleware.Recoverer)
	r.Use(middleware.Heartbeat("/healthz"))
	r.Use(middleware.SetHeader("Content-Type", "application/json"))
	r.Use(trcMdw.Extractor())

	apiHandler := api.NewStrictHandler(app, nil)
	r.Mount("/v0", api.Handler(apiHandler))

	r.Group(func(r chi.Router) {
		r.Use(middleware.SetHeader("Content-Type", "application/yaml"))
		r.Get("/openapi.yaml", app.OpenAPI)
	})

	app.Handler = r
}
