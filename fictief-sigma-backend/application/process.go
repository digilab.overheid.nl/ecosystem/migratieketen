package application

import (
	"context"

	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
)

// ReadProcesById implements api.StrictServerInterface.
func (app *Application) ReadProcesById(ctx context.Context, request api.ReadProcesByIdRequestObject) (api.ReadProcesByIdResponseObject, error) {
	process, err := app.servicer.ReadProcessByID(ctx, request.ProcesId, request.Params.Attributes)
	if err != nil {
		return api.ReadProcesById400JSONResponse{}, nil
	}

	vwlID := uuid.New()
	if request.Params.FSCVWLVerwerkingsSpan != nil {
		vwlID = *request.Params.FSCVWLVerwerkingsSpan
	}

	return api.ReadProcesById200JSONResponse{
		ProcesResponseJSONResponse: api.ProcesResponseJSONResponse{
			Body: api.ProcesResponse{
				Data: process,
			},
			Headers: api.ProcesResponseResponseHeaders{
				FSCVWLSysteem:               app.vwlSystem,
				FSCVWLVerantwoordelijke:     app.cfg.Organization.Name,
				FSCVWLVerwerker:             app.cfg.Organization.Name,
				FSCVWLVerwerkingsActiviteit: "https://example.com/activity/v0/proces-read",
				FSCVWLVerwerkingsSpan:       vwlID,
			},
		},
	}, nil
}

// ReadProcessen implements api.StrictServerInterface.
func (app *Application) ReadProcessen(ctx context.Context, request api.ReadProcessenRequestObject) (api.ReadProcessenResponseObject, error) {
	processes, err := app.servicer.ReadProcesses(ctx, request.Params.Attributes)
	if err != nil {
		return api.ReadProcessen400JSONResponse{}, nil
	}

	vwlID := uuid.New()
	if request.Params.FSCVWLVerwerkingsSpan != nil {
		vwlID = *request.Params.FSCVWLVerwerkingsSpan
	}

	return api.ReadProcessen200JSONResponse{
		ProcessenResponseJSONResponse: api.ProcessenResponseJSONResponse{
			Body: api.ProcessenResponse{
				Data: processes,
			},
			Headers: api.ProcessenResponseResponseHeaders{
				FSCVWLSysteem:               app.vwlSystem,
				FSCVWLVerantwoordelijke:     app.cfg.Organization.Name,
				FSCVWLVerwerker:             app.cfg.Organization.Name,
				FSCVWLVerwerkingsActiviteit: "https://example.com/activity/v0/processen-read",
				FSCVWLVerwerkingsSpan:       vwlID,
			},
		},
	}, err
}

// ReadProcessenByVreemdelingId implements api.StrictServerInterface.
func (app *Application) ReadProcessenByVreemdelingId(ctx context.Context, request api.ReadProcessenByVreemdelingIdRequestObject) (api.ReadProcessenByVreemdelingIdResponseObject, error) {
	processes, err := app.servicer.ReadProcessesByVreemdelingID(ctx, request.VreemdelingId, request.Params.Attributes)
	if err != nil {
		return api.ReadProcessenByVreemdelingId400JSONResponse{}, nil
	}

	vwlID := uuid.New()
	if request.Params.FSCVWLVerwerkingsSpan != nil {
		vwlID = *request.Params.FSCVWLVerwerkingsSpan
	}

	return api.ReadProcessenByVreemdelingId200JSONResponse{
		ProcessenResponseJSONResponse: api.ProcessenResponseJSONResponse{
			Body: api.ProcessenResponse{
				Data: processes,
			},
			Headers: api.ProcessenResponseResponseHeaders{
				FSCVWLSysteem:               app.vwlSystem,
				FSCVWLVerantwoordelijke:     app.cfg.Organization.Name,
				FSCVWLVerwerker:             app.cfg.Organization.Name,
				FSCVWLVerwerkingsActiviteit: "https://example.com/activity/v0/processen-read",
				FSCVWLVerwerkingsSpan:       vwlID,
			},
		},
	}, nil
}

// CreateAttributeByProcesId implements api.StrictServerInterface.
func (app *Application) CreateAttributeByProcesId(ctx context.Context, request api.CreateAttributeByProcesIdRequestObject) (api.CreateAttributeByProcesIdResponseObject, error) {
	observation, err := app.servicer.CreateAttributeByProcesID(ctx, request.ProcesId, request.Body.Data)
	if err != nil {
		return api.CreateAttributeByProcesId400JSONResponse{}, nil
	}

	vwlID := uuid.New()
	if request.Params.FSCVWLVerwerkingsSpan != nil {
		vwlID = *request.Params.FSCVWLVerwerkingsSpan
	}

	return api.CreateAttributeByProcesId201JSONResponse{
		ObservationCreateResponseJSONResponse: api.ObservationCreateResponseJSONResponse{
			Body: api.ObservationCreateResponse{
				Data: observation,
			},
			Headers: api.ObservationCreateResponseResponseHeaders{
				FSCVWLSysteem:               app.vwlSystem,
				FSCVWLVerantwoordelijke:     app.cfg.Organization.Name,
				FSCVWLVerwerker:             app.cfg.Organization.Name,
				FSCVWLVerwerkingsActiviteit: "https://example.com/activity/v0/vreemdeling-create",
				FSCVWLVerwerkingsSpan:       vwlID,
			},
		},
	}, nil
}

// CreateProcesByVreemdelingenId implements api.StrictServerInterface.
func (app *Application) CreateProcesByVreemdelingenId(ctx context.Context, request api.CreateProcesByVreemdelingenIdRequestObject) (api.CreateProcesByVreemdelingenIdResponseObject, error) {
	process, err := app.servicer.CreateProcessByVreemdelingenID(ctx, request.VreemdelingId, request.Body.Data)
	if err != nil {
		str := err.Error()
		return api.CreateProcesByVreemdelingenId400JSONResponse{
			BadRequestErrorResponseJSONResponse: api.BadRequestErrorResponseJSONResponse{
				Errors: []api.Error{
					{
						Message: &str,
					},
				},
			},
		}, nil
	}

	vwlID := uuid.New()
	if request.Params.FSCVWLVerwerkingsSpan != nil {
		vwlID = *request.Params.FSCVWLVerwerkingsSpan
	}

	return api.CreateProcesByVreemdelingenId201JSONResponse{
		ProcesCreateResponseJSONResponse: api.ProcesCreateResponseJSONResponse{
			Body: api.ProcesCreateResponse{
				Data: process,
			},
			Headers: api.ProcesCreateResponseResponseHeaders{
				FSCVWLSysteem:               app.vwlSystem,
				FSCVWLVerantwoordelijke:     app.cfg.Organization.Name,
				FSCVWLVerwerker:             app.cfg.Organization.Name,
				FSCVWLVerwerkingsActiviteit: "http://example.com/activity/v0/process-create",
				FSCVWLVerwerkingsSpan:       vwlID,
			},
		},
	}, nil
}

// UpdateProcesById implements api.StrictServerInterface.
func (app *Application) UpdateProcesById(ctx context.Context, request api.UpdateProcesByIdRequestObject) (api.UpdateProcesByIdResponseObject, error) {
	_, err := app.servicer.UpdateProcessByID(ctx, request.ProcesId, request.Body.Data)
	if err != nil {
		return api.UpdateProcesById400JSONResponse{}, nil
	}

	return api.UpdateProcesById204Response{}, err
}

// DeleteAttributeById implements api.StrictServerInterface.
func (app *Application) DeleteAttributeById(ctx context.Context, request api.DeleteAttributeByIdRequestObject) (api.DeleteAttributeByIdResponseObject, error) {
	if err := app.servicer.DeleteAttributeByID(ctx, request.AttributeId); err != nil {
		return api.DeleteAttributeById400JSONResponse{}, nil
	}

	return api.DeleteAttributeById204Response{}, nil
}
