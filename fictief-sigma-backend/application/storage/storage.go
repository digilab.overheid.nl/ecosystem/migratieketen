package storage

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net"
	"net/url"
	"sync"
	"time"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres" // postgres driver
	"github.com/huandu/xstrings"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // postgres driver

	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage/migrations"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage/queries/generated"
)

const driverName = "embed"

var registerDriverOnce sync.Once //nolint:gochecknoglobals // We need a singleton pattern

type Database struct {
	DB      *sqlx.DB
	Queries *queries.Queries
}

func New(dsn string) (*Database, error) {
	db, err := NewPostgreSQLConnection(dsn)
	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	querier, err := queries.Prepare(context.Background(), db)
	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	return &Database{
		DB:      db,
		Queries: querier,
	}, nil
}

func NewPostgreSQLConnection(dsn string) (*sqlx.DB, error) {
	const maxRetries = 10
	const delay = 5 * time.Second
	var err error
	var db *sqlx.DB

	for i := 0; i < maxRetries; i++ {
		db, err = sqlx.Open("postgres", dsn)
		if err != nil {
			slog.Error("Could not open database", "err", err, "dsn", dsn)
			// ATTN: Open doesn't actually try to connect, so an error here means there's a config error, not a retryable condition.
			return nil, err
		}
		err = db.Ping()
		if err != nil {
			var netOpErr *net.OpError
			if errors.As(err, &netOpErr) {
				slog.Error("Could not connect to database. Retrying...", "err", netOpErr, "dsn", dsn, "delay", delay.Seconds())
				time.Sleep(delay)
				continue
			}
			slog.Error("Could not connect to database, no point in retrying", "err", err, "dsn", dsn)
			return nil, err
		}
		db.SetConnMaxLifetime(5 * time.Minute)
		db.SetMaxIdleConns(2)
		db.MapperFunc(xstrings.ToSnakeCase)
		return db, nil
	}
	slog.Error("Could not connect to database", "retries", maxRetries)
	return nil, fmt.Errorf("could not connect to database: %w", err)
}

func (r *Database) Shutdown() error {
	return r.DB.Close() //nolint:wrapcheck // Not necessary
}

func setupMigrator(dsn, schema string) (*migrate.Migrate, error) {
	registerDriverOnce.Do(func() {
		migrations.RegisterDriver(driverName)
	})

	uri, err := url.Parse(dsn)
	if err != nil {
		return nil, fmt.Errorf("could not parse dsn: %w", err)
	}

	values := uri.Query()
	values.Add("search_path", schema)
	uri.RawQuery = values.Encode()

	migrator, err := migrate.New(fmt.Sprintf("%s://", driverName), uri.String())
	if err != nil {
		return nil, fmt.Errorf("migrate new failed: %w", err)
	}

	return migrator, nil
}

func PostgresPerformMigrations(dsn, schema string) error {
	migrator, err := setupMigrator(dsn, schema)
	if err != nil {
		return err
	}

	if err := migrator.Up(); err != nil {
		if errors.Is(err, migrate.ErrNoChange) {
			slog.Info("migrations are up-to-date")

			return nil
		}

		return fmt.Errorf("migrator up failed: %w", err)
	}

	return nil
}

func PostgresMigrationStatus(dsn, schema string) (uint, bool, error) {
	migrator, err := setupMigrator(dsn, schema)
	if err != nil {
		return 0, false, err
	}

	version, dirty, err := migrator.Version()
	if err != nil {
		return 0, false, fmt.Errorf("migrator version failed %w", err)
	}

	return version, dirty, nil
}

func MigrateInit(psqlSchemaName string, dns string) error {
	db, err := NewPostgreSQLConnection(dns)
	if err != nil {
		return fmt.Errorf("failed to create new config: %w", err)
	}

	if _, err := db.ExecContext(context.Background(), fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS %s", psqlSchemaName)); err != nil {
		return fmt.Errorf("failed to create schema: %w", err)
	}

	return nil
}
