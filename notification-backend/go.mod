module gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-backend

go 1.22.2

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api => ../notification-api

require (
	github.com/caarlos0/env/v10 v10.0.0
	github.com/go-chi/chi/v5 v5.0.13
	github.com/nats-io/nats.go v1.35.0
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api v0.0.0
	gopkg.in/yaml.v3 v3.0.1
	nhooyr.io/websocket v1.8.11
)

require (
	github.com/getkin/kin-openapi v0.125.0 // indirect
	github.com/go-openapi/jsonpointer v0.21.0 // indirect
	github.com/go-openapi/swag v0.23.0 // indirect
	github.com/invopop/yaml v0.3.1 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/klauspost/compress v1.17.9 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/nats-io/nkeys v0.4.7 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/perimeterx/marshmallow v1.1.5 // indirect
	golang.org/x/crypto v0.24.0 // indirect
	golang.org/x/sys v0.21.0 // indirect
)
