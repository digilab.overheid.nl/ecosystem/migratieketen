module gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api

go 1.22.2

require (
	github.com/getkin/kin-openapi v0.125.0
	github.com/go-chi/chi/v5 v5.0.13
)

require (
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/go-openapi/jsonpointer v0.21.0 // indirect
	github.com/go-openapi/swag v0.23.0 // indirect
	github.com/invopop/yaml v0.3.1 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/perimeterx/marshmallow v1.1.5 // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	github.com/ugorji/go/codec v1.2.12 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
