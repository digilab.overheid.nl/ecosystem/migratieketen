openapi: "3.1.0"
info:
  title: Register van de Verwerkingsactiviteiten
  license:
    name: European Union Public License 1.2
    identifier: EUPL-1.2
  version: "0.1.0"

servers:
  - url: /v0

paths:
  /processing_activities:
    get:
      summary: Get all processing activities
      operationId: list_processing_activities
      tags:
        - processing_activity
      responses:
        "200":
          description: List of processing activities
          headers:
            Link:
              $ref: "#/components/headers/Link"
              description: "Links to the previous, next, last and first pages. Present when there are more than 1 page."
            API-Version:
              $ref: "#/components/headers/API-Version"
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ProcessingActivityItem"
  /processing_activities/{id}:
    get:
      summary: Get a processing activity
      operationId: get_processing_activity
      tags:
        - processing_activity
      parameters:
        - name: id
          in: path
          description: The ID of the processing activity
          required: true
          schema:
            type: string
      responses:
        "200":
          description: Single processing activity
          headers:
            API-Version:
              $ref: "#/components/headers/API-Version"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ProcessingActivityItem"
        "404":
          description: Processing activity does not exist
          headers:
            API-Version:
              $ref: "#/components/headers/API-Version"


components:
  schemas:
    ProcessingActivityItem:
      type: object
      properties:
        id:
          type: string
        uri:
          type: string
          format: uri
        name:
          type: string
        purpose:
          type: string
        controller:
          type: string
        envisaged_time_limit:
          type: string
        envisaged_time_limit_duration:
          type: string
        data_subject_categories:
          type: array
          items:
            type: string
        recipients_categories:
          type: array
          items:
            type: string
        personal_data_categories:
          type: array
          items:
            type: string
        legal_basis:
          $ref: "#/components/schemas/LegalBasis"
        legal_basis_comment:
          type: string
    LegalBasis:
      type: string
      description: As defined in the GDPR, article 6, section 1
      enum:
        - CONSENT
        - PERFORMANCE_OF_CONTRACT
        - LEGAL_OBLIGATION
        - VITAL_INTERESTS
        - PUBLIC_INTEREST
        - LEGITIMATE_INTERESTS

  responses:
    ResourceNotFound:
      description: Resource does not exist
      headers:
        API-Version:
          $ref: "#/components/headers/API-Version"
      content:
        application/json:
          schema:
            type: object
            properties:
              detail:
                type: string

  parameters:
    PaginationPage:
      name: page
      in: query
      description: A page number within the paginated result set.
      schema:
        type: integer

  headers:
    API-Version:
      description: Version of this API
      schema:
        type: string
        example: "1.0.0"
        externalDocs:
          description: API Design Rule API-57
          url: https://publicatie.centrumvoorstandaarden.nl/api/adr/1.0/#api-57
    Link:
      description: HTTP Link Header
      schema:
        type: string
        example: <https://example.com/?page=2>; rel="prev", <https://example.com/?page=4>; rel="next"
        externalDocs:
          description: W3C reference
          url: https://www.w3.org/wiki/LinkHeader

security: []
