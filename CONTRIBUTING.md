# Contributing

## Setup your development environment

### Requirements

We use [asdf](https://asdf-vm.com/) for installing [required tools](.tool-versions):
```sh
asdf plugin add golang
asdf plugin add golangci-lint
asdf plugin add jq
asdf plugin add k3d
asdf plugin add kubectl
asdf plugin add kustomize
asdf plugin add oapi-codegen
asdf plugin add pnpm
asdf plugin add pre-commit
asdf plugin add skaffold
asdf install
```


## Running locally

Running locally uses k3d, so we need a working docker environment.

We use pre-commmit for providing a git hook:
```sh
pre-commit install
```

The first time you run the project, getting k3d to work takes a while. Wait for all pods to be running:
```sh
make k3d
kubectl get pods -Aw
```

After that, you can just do:
```sh
make dev
```

To add test fixtures:
```sh
make seed
```

Open [index-127.0.0.1.nip.io:8080](http://index-127.0.0.1.nip.io:8080) for the landing page, linking to all services.

After changing [FSC NLX](https://docs.fsc.nlx.io) configuration, it might be needed to reload the FSC databases:
```sh
make reset_fsc_dbs
```

### Testing

```sh
make test
```

### Example URLs

These are some URLs that should work (but really you should use some openapi helper tool like SwaggerUI to generate requests for you):
- Create a new vreemdeling in the BVV:
  ```
  echo '{
    "data": {
      "naam": "John Doe",
      "geboortedatum": "1978-12-31"
    }
  }' | curl -H 'accept: application/json' \
    -X 'POST' \
    --data @- \
    'http://np-bvv-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen'
- Find a vreemdeling by name and birthdate in the BVV:
  ```
  curl -H 'accept: application/json' \
    'http://np-bvv-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen?naam=matthew&geboortedatum=1986-11-16'
  ```
- Get some attributes of a single vreemdeling:
  ```
  curl -H 'accept: application/json' \
    'http://shared-loket-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/0000000001?attributes=alias,spreektalen&sources=fictief-coa,fictief-ind'
  ```

For more URLs, look in `test/integration`.


## External documentation
Some useful links for getting familiar with the content matter:
- [Explanation of SIGMA from DT&V](https://www.dienstterugkeerenvertrek.nl/over-dtv/leidraad-terugkeer-en-vertrek/informatieuitwisseling-documenten-bagage-vervoer/sigma-vreemdelingenbeeld).
