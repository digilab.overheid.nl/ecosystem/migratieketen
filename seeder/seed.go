package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"math"
	"net/http"
	"net/http/httputil"
	"time"

	randomData "github.com/Pallinder/go-randomdata"
	"github.com/google/uuid"
	"github.com/oapi-codegen/runtime/types"

	bvv "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	sigma "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
)

type SeedCmd struct {
	BvvServer    string `env:"MK_BVV_SERVER" default:"http://np-bvv-backend-127.0.0.1.nip.io:8080/v0" name:"bvv-server" help:"bvv endpoint to send seed data to."`
	BvvSeedCount int    `type:"int" env:"MK_BVV_SEED_COUNT" default:"100" name:"bvv-seed-count" help:"amount of vreemdelingen to create."`

	ObservationSeedCount int `type:"int" env:"MK_OBSERVATION_SEED_COUNT" default:"5" name:"observation-seed-count" help:"maximum amount of attributes to create per vreemdeling."`

	ProcessSeedCount int `type:"int" env:"MK_SIGMA_PROCESS_SEED_COUNT" default:"5" name:"sigma-process-seed-count" help:"maximum amount of process to create per vreemdeling."`

	ProcessAttributeSeedCount int `type:"int" env:"MK_SIGMA_PROCESS_ATTRIBUTE_SEED_COUNT" default:"5" name:"sigma-process-attribute-seed-count" help:"maximum amount of attributes create per process."`

	SigmaServers []string `env:"MK_SIGMA_SERVERS" required:"" name:"sigma-servers" help:"sigma endpoint to send seed data to."`

	Config string `env:"MK_CONFIG" default:"./config/global.yaml" name:"config" help:"config file to use."`
}

func (opt *SeedCmd) Run(ctx *Context) error {
	bvvServer := opt.BvvServer
	bvvSeedCount := opt.BvvSeedCount
	observationSeedCount := opt.ObservationSeedCount
	processSeedCount := opt.ProcessSeedCount
	processAttributeSeedCount := opt.ProcessAttributeSeedCount
	sigmaServers := opt.SigmaServers

	logger := ctx.Logger.With("application", "http_server")
	slog.SetDefault(logger)

	logger.Info("Starting seeding", "config", opt)

	globalConfig, err := GetGlobalConfig(opt.Config)
	if err != nil {
		return fmt.Errorf("config load failed: %w", err)
	}
	cfg := Config{
		GlobalConfig: *globalConfig,
	}

	logger.Debug("Loaded Config", "config", cfg)

	bvvClient, err := bvv.NewClient(bvvServer)
	if err != nil {
		return fmt.Errorf("bvv client creation failed: %w", err)
	}

	sigmaClients := []*sigma.Client{}
	for _, server := range sigmaServers {
		sigmaClient, err := sigma.NewClient(server)
		if err != nil {
			return fmt.Errorf("sigma client creation failed: %w", err)
		}

		sigmaClients = append(sigmaClients, sigmaClient)
	}

	for i := 1; i <= bvvSeedCount; i++ {
		if err := func() error {
			id, err := createVreemdeling(bvvClient)
			if err != nil {
				return fmt.Errorf("vreemdeling creation failed: %w", err)
			}

			for j := 0; j <= randomData.Number(observationSeedCount); j++ {
				if err := createAttribute(sigmaClients, &cfg, id); err != nil {
					return fmt.Errorf("attribute creation failed: %w", err)
				}
			}

			for j := 0; j <= randomData.Number(processSeedCount); j++ {
				client := sigmaClients[randomData.Number(len(sigmaClients))]

				if err := createProcess(client, &cfg, id, processAttributeSeedCount); err != nil {
					return fmt.Errorf("process creation failed: %w", err)
				}
			}

			return nil
		}(); err != nil {
			slog.Error("Seed round failed", "err", err)
		}
	}

	return nil
}

func createVreemdeling(client *bvv.Client) (string, error) {
	date := randomData.FullDateInRange("1920-01-01", "2023-12-31")
	birthDate, err := time.Parse("Monday 2 Jan 2006", date)
	if err != nil {
		return "", err
	}

	body := bvv.CreateVreemdelingJSONBody{
		Data: bvv.VreemdelingWithoutId{
			Geboortedatum: &types.Date{
				Time: birthDate,
			},
			Naam: randomData.FullName(randomData.RandomGender),
		},
	}

	resp, err := client.CreateVreemdeling(context.Background(), nil, bvv.CreateVreemdelingJSONRequestBody(body))
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 201 {
		respBody, _ := io.ReadAll(resp.Body)

		return "", fmt.Errorf("failed to create vreemdeling %v %v %s", resp.StatusCode, resp.Status, respBody)
	}

	var model bvv.VreemdelingResponse
	if err := json.NewDecoder(resp.Body).Decode(&model); err != nil {
		return "", err
	}

	slog.Info("Created vreemdeling", "data", model)

	return model.Data.Id, nil
}

func createAttribute(clients []*sigma.Client, cfg *Config, id string) error {
	client := clients[randomData.Number(len(clients))]

	rubriek := getRandRubriek(cfg)
	attribute := getRandAttribute(rubriek.Attributes)
	data, err := getRandAttributeData(attribute)
	if err != nil {
		return err
	}

	vwlID := uuid.New()
	systeem := "sigma"
	activiteit := "https://example.com/activity/v0/observation-create"
	verantwoordelijke := "seeder"

	params := &sigma.CreateObservationByVreemdelingenIdParams{
		FSCVWLVerwerkingsActiviteit: &activiteit,
		FSCVWLVerwerkingsSpan:       &vwlID,
		FSCVWLSysteem:               &systeem,
		FSCVWLVerwerker:             &verantwoordelijke,
		FSCVWLVerantwoordelijke:     &verantwoordelijke,
	}

	body := sigma.CreateObservationByVreemdelingenIdJSONRequestBody{
		Data: sigma.ObservationCreate{
			Attribute: attribute.Name,
			Value:     *data,
		},
	}

	resp, err := client.CreateObservationByVreemdelingenId(context.Background(), id, params, body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusCreated {
		r, _ := httputil.DumpResponse(resp, true)
		return fmt.Errorf("failed to create attribute %d %s\n%s\n%+v", resp.StatusCode, resp.Status, r, body)
	}

	var model sigma.ObservationCreateResponse
	if err := json.NewDecoder(resp.Body).Decode(&model); err != nil {
		return err
	}

	slog.Info("Created observation", "data", model.Data)
	return nil
}

func createProcess(client *sigma.Client, cfg *Config, id string, attributeAmount int) error {
	proces := getRandProcess(cfg)
	procesStatus := getRandProcessStatus(&proces)

	amount := randomData.Number(attributeAmount)

	attributes := make([]sigma.ObservationCreate, 0, amount)
	for i := 0; i < amount; i++ {
		attribute := getRandAttribute(proces.Attributes)
		data, err := getRandAttributeData(attribute)
		if err != nil {
			return err
		}

		attributes = append(attributes, sigma.ObservationCreate{
			Attribute: attribute.Name,
			Value:     *data,
		})
	}

	vwlID := uuid.New()
	systeem := "sigma"
	activiteit := "https://example.com/activity/v0/proces-create"
	verantwoordelijke := "seeder"

	params := &sigma.CreateProcesByVreemdelingenIdParams{
		FSCVWLVerwerkingsActiviteit: &activiteit,
		FSCVWLVerwerkingsSpan:       &vwlID,
		FSCVWLSysteem:               &systeem,
		FSCVWLVerwerker:             &verantwoordelijke,
		FSCVWLVerantwoordelijke:     &verantwoordelijke,
	}

	body := sigma.CreateProcesByVreemdelingenIdJSONRequestBody{
		Data: sigma.ProcesCreate{
			Attributes: attributes,
			Status:     procesStatus.Name,
			Type:       proces.Name,
		},
	}

	resp, err := client.CreateProcesByVreemdelingenId(context.Background(), id, params, body)
	if err != nil {
		return fmt.Errorf("create proces by vreemdelingen id failed: %w", err)
	}

	if resp.StatusCode != http.StatusCreated {
		r, _ := httputil.DumpResponse(resp, true)
		slog.Error("failed to create attribute", "status code", resp.StatusCode, "status", resp.Status, "response", string(r), "body", body)
		return fmt.Errorf("failed to create attribute %d %s\n%s\n%+v", resp.StatusCode, resp.Status, r, body)
	}

	var model sigma.ProcesCreateResponse
	if err := json.NewDecoder(resp.Body).Decode(&model); err != nil {
		return fmt.Errorf("proces create response decode failed: %w", err)
	}

	slog.Info("Created proces", "data", model.Data)
	return nil
}

func getRandProcess(cfg *Config) Proces {
	return cfg.Processen[randomData.Number(len(cfg.Processen))]
}

func getRandProcessStatus(proces *Proces) ProcesStatus {
	return proces.Statuses[randomData.Number(len(proces.Statuses))]
}

func getRandRubriek(cfg *Config) Rubriek {
	return cfg.Rubrieken[randomData.Number(len(cfg.Rubrieken))]
}

func getRandAttribute(attributes []Attribute) Attribute {
	return attributes[randomData.Number(len(attributes))]
}

func getRandAttributeData(attribute Attribute) (*sigma.Value, error) {
	var err error
	var a sigma.Value

	switch attribute.Type {
	case "text":
		if attribute.Name == "alias" {
			err = a.FromValue1(randomData.SillyName())
		} else {
			err = a.FromValue1(randomData.Paragraph())
		}
	case "checkboxes":
		a, err = getRandAttributeDataMultiple(attribute)
	case "datetime":
		a, err = getRandAttributeDataTimedate(attribute)
	case "date":
		a, err = getRandAttributeDataDate(attribute)
	case "number":
		a, err = getRandAttributeNumber(attribute)
	case "boolean":
		err = a.FromValue2(randomData.Boolean())
	case "textarea":
		err = a.FromValue1(randomData.Paragraph())
	case "radios":
		a, err = getRandAttributeDataSingle(attribute)
	case "select":
		if attribute.Multiple {
			a, err = getRandAttributeDataMultiple(attribute)
		} else {
			a, err = getRandAttributeDataSingle(attribute)
		}
	default:
		return nil, fmt.Errorf("attribute type unknown: %sname:%s%+v", attribute.Type, attribute.Label, attribute)
	}

	return &a, err
}

func getRandAttributeDataSingle(attribute Attribute) (sigma.Value, error) {
	index := randomData.Number(len(attribute.Options))

	value := attribute.Options[index].Name

	var a sigma.Value
	if err := a.FromValue1(value); err != nil {
		return a, err
	}

	return a, nil
}

func getRandAttributeDataMultiple(attribute Attribute) (sigma.Value, error) {
	amount := randomData.Number(int(math.Min(float64(len(attribute.Options)), 5)))

	options := attribute.Options
	value := make([]interface{}, 0, amount)

	for i := 0; i <= int(amount); i++ {
		index := randomData.Number(len(options))
		option := options[index]
		value = append(value, option.Name)

		options = remove(options, index)
	}

	var a sigma.Value
	err := a.FromValue3(value)

	return a, err
}

func getRandAttributeNumber(attribute Attribute) (sigma.Value, error) {
	min := math.MinInt / 2
	max := math.MaxInt / 2

	if attribute.Min != nil {
		min = (*attribute.Min).(int)
	}

	if attribute.Max != nil {
		max = (*attribute.Max).(int)
	}

	var a sigma.Value
	if err := a.FromValue4(float32(randomData.Number(min, max))); err != nil {
		return a, err
	}

	return a, nil
}

func getRandAttributeDataTimedate(_ Attribute) (sigma.Value, error) {
	var a sigma.Value
	date := randomData.FullDate()
	d, err := time.Parse("Monday 2 Jan 2006", date)
	if err != nil {
		return a, err
	}

	if err := a.FromValue1(d.Format(time.RFC3339)); err != nil {
		return a, err
	}

	return a, nil
}

func getRandAttributeDataDate(attribute Attribute) (sigma.Value, error) {
	date := randomData.FullDate()
	if attribute.Name == "familieGeboortedatum" {
		date = randomData.FullDateInRange("1920-01-01", time.Now().Format("2006-01-02"))
	}

	var a sigma.Value
	d, err := time.Parse("Monday 2 Jan 2006", date)
	if err != nil {
		return a, err
	}

	if err := a.FromValue1(d.Format("2006-01-02")); err != nil {
		return a, err
	}

	return a, nil
}

func remove[T any](s []T, i int) []T {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}
