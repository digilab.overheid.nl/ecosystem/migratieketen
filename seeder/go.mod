module gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/seeder

go 1.22.2

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api => ../fictief-bvv-api

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api => ../fictief-sigma-api

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/alecthomas/kong v0.9.0
	github.com/google/uuid v1.6.0
	github.com/oapi-codegen/runtime v1.1.1
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api v0.0.0
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api v0.0.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/getkin/kin-openapi v0.125.0 // indirect
	github.com/go-chi/chi/v5 v5.0.13 // indirect
	github.com/go-openapi/jsonpointer v0.21.0 // indirect
	github.com/go-openapi/swag v0.23.0 // indirect
	github.com/invopop/yaml v0.3.1 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/perimeterx/marshmallow v1.1.5 // indirect
	gitlab.com/digilab.overheid.nl/ecosystem/fsc-vwl-api v0.0.0-20240313144656-6e78c7411048 // indirect
	golang.org/x/text v0.16.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
