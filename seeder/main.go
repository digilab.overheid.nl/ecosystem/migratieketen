package main

import (
	"log"
	"log/slog"
	"os"

	"github.com/alecthomas/kong"
)

type Context struct {
	Logger slog.Logger
}

var cli struct {
	Debug bool `env:"MK_DEBUG" default:"false" optional:"" name:"debug" help:"Enable debug mode."`

	Seed SeedCmd `cmd:"" help:"Start seeding."`
}

func Run() error {
	// Parse the command line.
	ctx := kong.Parse(&cli)

	// Setup the logger.
	var logLevel slog.Level
	if cli.Debug {
		logLevel = slog.LevelDebug
	} else {
		logLevel = slog.LevelWarn
	}
	logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
		Level: logLevel,
	}))

	// Call the Run() method of the selected parsed command.
	err := ctx.Run(&Context{Logger: *logger})

	return err
}

func main() {
	if err := Run(); err != nil {
		log.Fatal(err)
	}
}
