package processor

import (
	"context"

	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/bsn-pseudonimiseren-processor/config"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/consumer"
	"go.opentelemetry.io/collector/processor"
)

var t = component.MustNewType("bsn_processor")

func NewFactory() processor.Factory {
	return processor.NewFactory(t, createDefaultConfig, processor.WithTraces(createTraceProcessor, component.StabilityLevelAlpha))
}

func createTraceProcessor(_ context.Context, _ processor.CreateSettings, _ component.Config, nextConsumer consumer.Traces) (processor.Traces, error) {
	return NewBSNProcessor(nextConsumer), nil
}

func createDefaultConfig() component.Config {
	return &config.Config{}
}
