package processor

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"strings"

	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/consumer"
	"go.opentelemetry.io/collector/pdata/ptrace"
)

type BSNProcessor struct {
	next consumer.Traces
}

func NewBSNProcessor(next consumer.Traces) *BSNProcessor {
	return &BSNProcessor{next: next}
}

func (s *BSNProcessor) ConsumeTraces(ctx context.Context, batch ptrace.Traces) error {
	for i := range batch.ResourceSpans().Len() {
		// we split the incoming batch into a collection of ResourceSpans
		rs := batch.ResourceSpans().At(i)

		for j := range rs.ScopeSpans().Len() {
			ss := rs.ScopeSpans().At(j)

			for k := range ss.Spans().Len() {
				attr := ss.Spans().At(k).Attributes()
				if v, ok := attr.Get("dpl.core.user"); ok {
					if id, found := strings.CutPrefix(v.AsString(), "vid:"); found {
						hash := md5.Sum([]byte(id))
						v.SetStr(fmt.Sprintf("vid:%s", hex.EncodeToString(hash[:])))
					}
				}
			}
		}
	}

	if err := s.next.ConsumeTraces(ctx, batch); err != nil {
		return err
	}

	return nil
}

func (s *BSNProcessor) Capabilities() consumer.Capabilities {
	return consumer.Capabilities{
		MutatesData: true,
	}
}

func (s *BSNProcessor) Start(_ context.Context, host component.Host) error {
	return nil
}

func (s *BSNProcessor) Shutdown(context.Context) error {
	return nil
}
