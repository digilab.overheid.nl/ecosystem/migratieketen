module gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend

go 1.22.2

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api => ../fictief-bvv-api

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api => ../fictief-loket-api

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api => ../fictief-sigma-api

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api => ../notification-api

require (
	github.com/gofiber/contrib/websocket v1.3.1
	github.com/gofiber/fiber/v2 v2.52.4
	github.com/gofiber/template/html/v2 v2.1.1
	github.com/gorilla/websocket v1.5.3
	github.com/oapi-codegen/runtime v1.1.1
	github.com/spf13/cobra v1.8.1
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api v0.0.0
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api v0.0.0
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api v0.0.0
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api v0.0.0
	golang.org/x/exp v0.0.0-20240613232115-7f521ea00fb8
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/fasthttp/websocket v1.5.9 // indirect
	github.com/getkin/kin-openapi v0.125.0 // indirect
	github.com/go-chi/chi/v5 v5.0.13 // indirect
	github.com/go-openapi/jsonpointer v0.21.0 // indirect
	github.com/go-openapi/swag v0.23.0 // indirect
	github.com/invopop/yaml v0.3.1 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/perimeterx/marshmallow v1.1.5 // indirect
	github.com/savsgio/gotils v0.0.0-20240303185622-093b76447511 // indirect
	gitlab.com/digilab.overheid.nl/ecosystem/fsc-vwl-api v0.0.0-20240313144656-6e78c7411048 // indirect
	golang.org/x/net v0.26.0 // indirect
)

require (
	github.com/andybalholm/brotli v1.1.0 // indirect
	github.com/gofiber/template v1.8.3 // indirect
	github.com/gofiber/utils v1.1.0 // indirect
	github.com/google/uuid v1.6.0
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/klauspost/compress v1.17.9 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.55.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.21.0 // indirect
	golang.org/x/text v0.16.0
)
