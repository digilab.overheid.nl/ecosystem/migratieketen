package application

import (
	"context"
	"errors"
	"fmt"
	"log"
	"slices"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"

	loket "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	sigma "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/config"
)

type createOrUpdateProcesData struct {
	Type   string `form:"type"`
	Status string `form:"status"`

	// Decode the attributes that are passed as array
	Attributes dataAttributes `form:"attributes"`
}

func (a *Application) NewProces(c *fiber.Ctx) error {
	return c.Render("proces-new", fiber.Map{
		"vreemdelingNummer": c.Params("vreemdelingNummer"),
		"processen":         a.Cfg.Processen,
	})
}

func (a *Application) NewProcesDetails(c *fiber.Ctx) error {
	// Parse the request body
	procesType := c.Query("type")
	if procesType == "" {
		return errors.New("missing proces type")
	}

	// Find the proces in the config, based on its name/type
	var proces config.Proces
	for _, pr := range a.Cfg.Processen {
		if pr.Name == procesType {
			proces = pr
			break
		}
	}

	// In case no proces was found in the config, return an error
	if proces.Name == "" { // Note: empty / default value
		return fmt.Errorf("unrecognized proces type: '%s'", procesType)
	}

	// Get the proces attributes default values from the config
	data := make(map[string]sigma.Attribute)
	for _, attr := range proces.Attributes {
		if attr.Default != nil {
			data[attr.Name] = sigma.Attribute{
				Value: attr.Default,
			}
		}
	}

	return c.Render("proces-new-details", fiber.Map{
		"vreemdelingNummer": c.Params("vreemdelingNummer"),
		"proces":            proces,
		"data":              data,
	}, "")
}

func (a *Application) CreateProces(c *fiber.Ctx) error {
	// Parse the request body
	var data createOrUpdateProcesData
	if err := c.BodyParser(&data); err != nil {
		return fmt.Errorf("unexpected request body: %w", err)
	}

	// Insert the proces into Sigma
	sigmaClient, err := a.getSigmaClient()
	if err != nil {
		return fmt.Errorf("error creating Sigma client: %w", err)
	}

	// Get the user role
	role, ok := c.Locals("role").(config.Role)
	if !ok {
		return errors.New("error obtaining user role")
	}

	attrs, err := a.getProcesAttributes(data, role)
	if err != nil {
		return fmt.Errorf("error getting proces attributes: %w", err)
	}

	vreemdelingNummer := c.Params("vreemdelingNummer")
	ctx := context.Background()

	id := uuid.New()
	activiteit := "https://example.com/activity/v0/process-create"

	params := &sigma.CreateProcesByVreemdelingenIdParams{
		FSCVWLVerwerkingsActiviteit: &activiteit,
		FSCVWLVerwerkingsSpan:       &id,
		FSCVWLSysteem:               &a.vwlSystem,
		FSCVWLVerwerker:             &a.Cfg.Organization.Name,
		FSCVWLVerantwoordelijke:     &a.Cfg.Organization.Name,
	}

	body := sigma.CreateProcesByVreemdelingenIdJSONRequestBody{
		Data: sigma.ProcesCreate{
			Attributes: attrs,
			Status:     data.Status,
			Type:       data.Type,
		},
	}

	sigmaResp, err := sigmaClient.CreateProcesByVreemdelingenIdWithResponse(ctx, vreemdelingNummer, params, body)
	if err != nil {
		return fmt.Errorf("error adding proces in Sigma: %w", err)
	}

	// In case of an error, return
	if sigmaResp.JSON400 != nil || sigmaResp.JSON410 != nil || sigmaResp.JSON201 == nil {
		log.Printf("error creating proces in Sigma: %s", sigmaResp.Body)
		return fmt.Errorf("fout bij aanmaken van het proces: %s", sigmaResp.Body)
	}

	procesID := sigmaResp.JSON201.Data.ProcesId

	// Show a page that the person has been inserted
	return c.Render("proces-new-finished", fiber.Map{
		"vreemdelingNummer": vreemdelingNummer,
		"procesID":          procesID,
	})
}

func (a *Application) GetProces(c *fiber.Ctx) error {
	// Get the proces from Sigma loket
	loketClient, err := a.getLoketClient()
	if err != nil {
		return fmt.Errorf("error creating Sigma loket client: %w", err)
	}

	ctx := context.Background()

	// Get the user role
	role, ok := c.Locals("role").(config.Role)
	if !ok {
		return errors.New("error obtaining user role")
	}

	// Parse the proces ID
	procesID, err := uuid.Parse(c.Params("procesID"))
	if err != nil {
		return fmt.Errorf("error parsing proces ID: %w", err)
	}

	// First do a request to get only the proces type, without attributes
	sigmaResp, err := loketClient.ReadProcesByIdWithResponse(ctx, procesID, nil)
	if err != nil {
		return fmt.Errorf("error fetching proces from Sigma: %w", err)
	}

	// In case of an error, return
	if sigmaResp.JSON400 != nil || sigmaResp.JSON200 == nil {
		log.Printf("error fetching proces from Sigma: %s", sigmaResp.Body)
		return fmt.Errorf("fout bij ophalen van proces: %s", sigmaResp.Body)
	}

	attrs := role.ReadableProcesAttributes()[sigmaResp.JSON200.Data.Type]

	// Now we have the proces type, get the read permissions the role has for the proces from the config and fetch the proces including attributes from the Sigma loket
	loketResp, err := loketClient.ReadProcesByIdWithResponse(ctx, procesID, &loket.ReadProcesByIdParams{
		Attributes: &attrs,
	})
	if err != nil {
		return fmt.Errorf("error fetching proces from Sigma: %w", err)
	}

	// In case of an error, return
	if loketResp.JSON400 != nil || loketResp.JSON200 == nil {
		log.Printf("error fetching proces from Sigma loket: %s", loketResp.Body)
		return fmt.Errorf("fout bij ophalen van proces: %s", loketResp.Body)
	}

	data := loketResp.JSON200.Data

	// Find the proces in the config, based on its name/type
	var proces config.Proces
	for _, pr := range a.Cfg.Processen {
		if pr.Name == data.Type {
			proces = pr
			break
		}
	}

	// In case no proces was found in the config, return an error
	if proces.Name == "" { // Note: empty / default value
		return fmt.Errorf("unrecognized proces type: '%s'", data.Type)
	}

	// Select the corresponding status label from the config
	var statusLabel string
	for _, status := range proces.Statuses {
		if status.Name == data.Status {
			statusLabel = status.Label
			break
		}
	}

	return c.Render("proces", fiber.Map{
		"vreemdelingNummer": c.Params("vreemdelingNummer"),
		"proces":            proces,
		"statusLabel":       statusLabel,
		"data":              data,
	})
}

func (a *Application) EditProces(c *fiber.Ctx) error {
	// Get the proces from Sigma
	sigmaClient, err := a.getSigmaClient()
	if err != nil {
		return fmt.Errorf("error creating Sigma client: %w", err)
	}

	ctx := context.Background()

	// Parse the proces ID
	procesID, err := uuid.Parse(c.Params("procesID"))
	if err != nil {
		return fmt.Errorf("error parsing proces ID: %w", err)
	}

	// Get the user role
	role, ok := c.Locals("role").(config.Role)
	if !ok {
		return errors.New("error obtaining user role")
	}

	// First do a request to get only the proces type, without attributes
	sigmaResp, err := sigmaClient.ReadProcesByIdWithResponse(ctx, procesID.String(), nil)
	if err != nil {
		return fmt.Errorf("error fetching proces from Sigma: %w", err)
	}

	// In case of an error, return
	if sigmaResp.JSON400 != nil || sigmaResp.JSON410 != nil || sigmaResp.JSON200 == nil {
		log.Printf("error fetching proces from Sigma: %s", sigmaResp.Body)
		return fmt.Errorf("fout bij ophalen van proces: %s", sigmaResp.Body)
	}

	attrs := role.ReadableProcesAttributes()[sigmaResp.JSON200.Data.Type] // Note: readable instead of writable attributes, since it is possible that an attribute is writable, but not readable

	sigmaResp, err = sigmaClient.ReadProcesByIdWithResponse(ctx, procesID.String(), &sigma.ReadProcesByIdParams{
		Attributes: &attrs,
	})
	if err != nil {
		return fmt.Errorf("error fetching proces from Sigma: %w", err)
	}

	// In case of an error, return
	if sigmaResp.JSON400 != nil || sigmaResp.JSON410 != nil || sigmaResp.JSON200 == nil {
		log.Printf("error fetching proces from Sigma: %s", sigmaResp.Body)
		return fmt.Errorf("fout bij ophalen van proces: %s", sigmaResp.Body)
	}

	data := sigmaResp.JSON200.Data

	// Find the proces in the config, based on its name/type
	var proces config.Proces
	for _, pr := range a.Cfg.Processen {
		if pr.Name == data.Type {
			proces = pr
			break
		}
	}

	// In case no proces was found in the config, return an error
	if proces.Name == "" { // Note: empty / default value
		return fmt.Errorf("unrecognized proces type: '%s'", data.Type)
	}

	return c.Render("proces-edit", fiber.Map{
		"vreemdelingNummer": c.Params("vreemdelingNummer"),
		"proces":            proces,
		"data":              data,
	})
}

func (a *Application) UpdateProces(c *fiber.Ctx) error {
	// Parse the request body
	var data createOrUpdateProcesData
	if err := c.BodyParser(&data); err != nil {
		return fmt.Errorf("unexpected request body: %w", err)
	}

	// Create a Sigma client
	sigmaClient, err := a.getSigmaClient()
	if err != nil {
		return fmt.Errorf("error creating Sigma client: %w", err)
	}

	ctx := context.Background()

	procesID := c.Params("procesID")

	// Get the user role
	role, ok := c.Locals("role").(config.Role)
	if !ok {
		return errors.New("error obtaining user role")
	}

	// Get the proces type from Sigma. Note: the type is required to get the config proces in a.getProcesAttributes
	procesResp, err := sigmaClient.ReadProcesByIdWithResponse(ctx, procesID, nil)
	if err != nil {
		return fmt.Errorf("error fetching proces from Sigma: %w", err)
	}

	// In case of an error, return
	if procesResp.JSON400 != nil || procesResp.JSON410 != nil || procesResp.JSON200 == nil {
		log.Printf("error fetching proces from Sigma: %s", procesResp.Body)
		return fmt.Errorf("fout bij ophalen van proces: %s", procesResp.Body)
	}

	data.Type = procesResp.JSON200.Data.Type

	// If the user role has no permissions to set any attribute of this proces, return an error
	writableAttributes := role.WritableProcesAttributes()[data.Type]
	if len(writableAttributes) == 0 {
		return fmt.Errorf("no permissions to update proces %s", data.Type)
	}

	id := uuid.New()
	activiteit := "https://example.com/activity/v0/process-update"

	params := &sigma.UpdateProcesByIdParams{
		FSCVWLVerwerkingsActiviteit: &activiteit,
		FSCVWLVerwerkingsSpan:       &id,
		FSCVWLSysteem:               &a.vwlSystem,
		FSCVWLVerwerker:             &a.Cfg.Organization.Name,
		FSCVWLVerantwoordelijke:     &a.Cfg.Organization.Name,
	}

	body := sigma.UpdateProcesByIdJSONRequestBody{
		Data: sigma.ProcesUpdate{
			Status: data.Status,
		},
	}
	// Update the proces status
	sigmaResp, err := sigmaClient.UpdateProcesByIdWithResponse(ctx, procesID, params, body)
	if err != nil {
		return fmt.Errorf("error updating proces: %w", err)
	}

	// In case of an error, return
	if sigmaResp.JSON400 != nil || sigmaResp.JSON410 != nil {
		log.Printf("error updating proces in Sigma: %s", sigmaResp.Body)
		return fmt.Errorf("fout bij aanpassen van het proces: %s", sigmaResp.Body)
	}

	// Update the proces attributes by adding new ones that overrule the existing ones
	attrs, err := a.getProcesAttributes(data, role)
	if err != nil {
		return fmt.Errorf("error getting proces attributes: %w", err)
	}

	for _, attr := range attrs {
		activiteit := "https://example.com/activity/v0/attribute-create"

		params := &sigma.CreateAttributeByProcesIdParams{
			FSCVWLVerwerkingsActiviteit: &activiteit,
			FSCVWLVerwerkingsSpan:       &id,
			FSCVWLSysteem:               &a.vwlSystem,
			FSCVWLVerwerker:             &a.Cfg.Organization.Name,
			FSCVWLVerantwoordelijke:     &a.Cfg.Organization.Name,
		}

		body := sigma.CreateAttributeByProcesIdJSONRequestBody{
			Data: attr,
		}

		sigmaAttrResp, err := sigmaClient.CreateAttributeByProcesIdWithResponse(ctx, procesID, params, body)
		if err != nil {
			return fmt.Errorf("error storing proces attributes: %w", err)
		}

		if sigmaAttrResp.JSON400 != nil || sigmaAttrResp.JSON410 != nil {
			log.Printf("error updating proces in Sigma: %s", sigmaAttrResp.Body)
			return fmt.Errorf("fout bij aanpassen van het proces: %s", sigmaAttrResp.Body)
		}
	}

	// Show a page that the person has been inserted
	return c.Render("proces-edit-finished", fiber.Map{
		"vreemdelingNummer": c.Params("vreemdelingNummer"),
		"procesID":          procesID,
	})
}

func (a *Application) getProcesAttributes(data createOrUpdateProcesData, role config.Role) ([]sigma.ObservationCreate, error) {
	// Find the corresponding proces in the config
	found := false

	var proces config.Proces
	for _, pr := range a.Cfg.Processen {
		if pr.Name == data.Type {
			proces = pr
			found = true
			break
		}
	}

	if !found {
		return nil, fmt.Errorf("cannot find proces with name '%s'", data.Type)
	}

	writableAttributes := role.WritableProcesAttributes()[proces.Name]

	// Range over all submitted attributes
	attrs := make([]sigma.ObservationCreate, 0, len(data.Attributes))
	for _, dataAttr := range data.Attributes {
		// Get the corresponding attribute from the config
		found := false

		var attr *config.Attribute
		for i, at := range proces.Attributes {
			if at.Name == dataAttr.Name {
				attr = &proces.Attributes[i] // Note: instead of &at, which would be incorrect due to the loop
				found = true
				break
			}
		}

		// Skip if the attribute name/key is not found in the config
		if !found {
			continue
		}

		// Skip unset or empty elements. IMPROVE: implement some way in the backend and frontend to pass null values? Now e.g. there is no difference between cleared checkboxes and skipped checkboxes
		if len(dataAttr.Value) == 0 || dataAttr.Value[0] == "" {
			continue
		}

		// Skip attributes for which the role has no 'set' permissions
		if !slices.Contains[[]string](writableAttributes, dataAttr.Name) {
			log.Printf("user role %s tried to set attribute %s of proces %s for which it has no permissions, ignoring", role.Name, proces.Name, dataAttr.Name)
			continue
		}

		val, err := a.parseAttribute(dataAttr.Value, attr)
		if err != nil {
			return nil, fmt.Errorf("error parsing attribute '%s' (value: %v). error: %w", attr.Name, dataAttr.Value, err)
		}

		attrs = append(attrs, sigma.ObservationCreate{
			Attribute: attr.Name,
			Value:     val,
		})
	}

	return attrs, nil
}
