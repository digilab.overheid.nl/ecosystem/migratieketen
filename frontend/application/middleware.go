package application

import (
	"errors"
	"fmt"

	"github.com/gofiber/fiber/v2"
)

// RequireLogin checks if the user is logged in and gets the user role, storing it as local
func (a *Application) RequireLogin(c *fiber.Ctx) error {
	// If no role query parameter set, redirect to the login page
	roleName := c.Query("role")

	if roleName == "" {
		return c.Redirect("/login?next=" + c.OriginalURL()) // Note: c.Redirect uses HTTP status 302 by default, so not cached. Note: c.OriginalURL does not return the full URL, but a relative URL, which is fine
	}

	for _, role := range a.Cfg.Roles {
		if role.Name == roleName {
			// Set the role as local. Note: this can be read in all templates, since we set PassLocalsToViews: true in the Fiber config
			c.Locals("role", role)
			c.Locals("routeSuffix", fmt.Sprintf("?role=%s", role.Name))

			// IMPROVE: if the 'breakTheGlass' role is used, publish a notification via the Notification backend

			return c.Next()
		}
	}

	c.Status(403)
	return fmt.Errorf("unknown role '%s'", roleName)
}

func (a *Application) ErrorHandler(c *fiber.Ctx, err error) error {
	// Status code defaults to 500
	code := fiber.StatusInternalServerError

	// Retrieve the custom status code if it's a *fiber.Error
	var e *fiber.Error
	if errors.As(err, &e) {
		code = e.Code
	}

	data := fiber.Map{
		"title":   fmt.Sprintf("Fout %d", code),
		"code":    code,
		"details": err.Error(),
	}

	// If requested via HTMX, only return the error partial, without layout
	if len(c.Request().Header.Peek("HX-Request")) != 0 {
		if err = c.Render("partials/error-content", data, ""); err != nil { // Note: do not set the status (i.e. always return a 200 response), so HTMX will show the response. IMPROVE: listen to htmx:responseError events instead
			// In case serving the error partial fails
			return c.Status(code).SendString(err.Error())
		}

		return nil
	}

	// Otherwise, return a full custom error page
	if err = c.Status(code).Render("error", data); err != nil {
		// In case serving the error page fails
		return c.Status(code).SendString(err.Error())
	}

	return nil
}
