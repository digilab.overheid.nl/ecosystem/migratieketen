package application

import (
	"context"
	"errors"
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	openapi_types "github.com/oapi-codegen/runtime/types"
	"golang.org/x/exp/slices"

	bvv "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	loket "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	sigma "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/config"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/helpers"
)

type dataAttributes []struct {
	Name  string   `form:"name"`
	Value []string `form:"value"` // Note: can only parse this as string (slice) due to the Fiber implementation
}

type createOrUpdateVreemdelingData struct {
	Naam          string                `form:"naam"`
	Geboortedatum helpers.FormattedDate `form:"geboortedatum"`

	// Decode the attributes that are passed as array
	Attributes dataAttributes `form:"attributes"`
}

func (a *Application) MergeVreemdeling(c *fiber.Ctx) error {
	// Parse the request body
	var data struct {
		VreemdelingnummerLeidend   string `form:"vreemdelingnummer1"`
		VreemdelingnummerVervallen string `form:"vreemdelingnummer2"`
	}
	if err := c.BodyParser(&data); err != nil {
		return fmt.Errorf("unexpected request body: %w", err)
	}

	log.Printf("start merge vreeemdeling: %s with %s", data.VreemdelingnummerLeidend, data.VreemdelingnummerVervallen)

	// Search in the BVV
	client, err := a.getBVVClient()
	if err != nil {
		return fmt.Errorf("error creating BVV client: %w", err)
	}
	ctx := context.Background()

	_, err = client.CreateSamenvoeging(ctx, data.VreemdelingnummerLeidend, data.VreemdelingnummerVervallen)
	if err != nil {
		return fmt.Errorf("error merging in BVV: %w", err)
	}

	vwlID := uuid.New()
	activiteit := "https://example.com/activity/v0/samenvoeging-read"
	resp, err := client.GetSamenvoegingenWithResponse(ctx, data.VreemdelingnummerLeidend, &bvv.GetSamenvoegingenParams{
		FSCVWLVerwerkingsActiviteit: &activiteit,
		FSCVWLVerwerkingsSpan:       &vwlID,
		FSCVWLSysteem:               &a.vwlSystem,
		FSCVWLVerwerker:             &a.Cfg.Organization.Name,
		FSCVWLVerantwoordelijke:     &a.Cfg.Organization.Name,
	})
	if err != nil {
		return fmt.Errorf("error merging in BVV: %w", err)
	}

	// In case of an error, return
	if resp.JSON200 == nil {
		log.Printf("error merging people in BVV: %s", resp.Body)
		return fmt.Errorf("fout bij mergen van personen: %s", resp.Body)
	}

	results := resp.JSON200.Data

	log.Printf("end merge vreemdeling: %s with %s", data.VreemdelingnummerLeidend, data.VreemdelingnummerVervallen)

	// Show a page that the person has been inserted
	return c.Render("merge-finished", fiber.Map{
		"VreemdelingnummerLeidend":   data.VreemdelingnummerLeidend,
		"VreemdelingnummerVervallen": data.VreemdelingnummerVervallen,
		"AlleVreemdelingNummers":     results,
	})
}

func (a *Application) SearchVreemdeling(c *fiber.Ctx) error {
	// Parse the request body
	var data struct {
		Vreemdelingnummer string                `form:"vreemdelingnummer"`
		Name              string                `form:"name"`
		Birthdate         helpers.FormattedDate `form:"birthdate"`
	}
	if err := c.BodyParser(&data); err != nil {
		return fmt.Errorf("unexpected request body: %w", err)
	}

	// Search in the BVV
	client, err := a.getBVVClient()
	if err != nil {
		return fmt.Errorf("error creating BVV client: %w", err)
	}

	ctx := context.Background()

	var results []bvv.Vreemdeling
	if data.Vreemdelingnummer != "" {
		// If the vreemdelingnummer is set, get the vreemdeling by ID
		var resp *bvv.ReadVreemdelingByIdResponse

		vwlID := uuid.New()
		activiteit := "https://example.com/activity/v0/vreemdeling-read"

		if resp, err = client.ReadVreemdelingByIdWithResponse(ctx, data.Vreemdelingnummer, &bvv.ReadVreemdelingByIdParams{
			FSCVWLVerwerkingsActiviteit: &activiteit,
			FSCVWLVerwerkingsSpan:       &vwlID,
			FSCVWLSysteem:               &a.vwlSystem,
			FSCVWLVerwerker:             &a.Cfg.Organization.Name,
			FSCVWLVerantwoordelijke:     &a.Cfg.Organization.Name,
		}); err != nil {
			return fmt.Errorf("error fetching from BVV: %w", err)
		}

		if resp.JSON410 == nil { // In case no results are found, just skip
			if res := resp.JSON200; res != nil {
				results = []bvv.Vreemdeling{
					res.Data,
				}
			} else {
				return fmt.Errorf("unexpected response while fetching from BVV, http code %d, body: %s", resp.StatusCode(), resp.Body)
			}
		}
	} else {
		// If no vreemdelingnummer is set, find the vreemdeling by name and/or birthdate
		var params bvv.FindVreemdelingParams
		if data.Name != "" {
			params.Naam = &data.Name
		}
		if !data.Birthdate.IsZero() {
			bd := openapi_types.Date(data.Birthdate)
			params.Geboortedatum = &bd
		}

		resp, err := client.FindVreemdelingWithResponse(ctx, &params)
		if err != nil {
			return fmt.Errorf("error searching in BVV: %w", err)
		}

		// In case of an error, return
		if resp.JSON400 != nil || resp.JSON200 == nil {
			log.Printf("error searching people in BVV: %s", resp.Body)
			return fmt.Errorf("fout bij zoeken van personen: %s", resp.Body)
		}

		results = resp.JSON200.Data
	}

	return c.Render("search", fiber.Map{
		"results": results,
	})
}

func (a *Application) NewVreemdeling(c *fiber.Ctx) error {
	if !a.Cfg.Organization.BVVWriteAccess {
		return errors.New("this organization does not have BVV write access")
	}

	// Get the default Sigma values from the config. Note: currently not useful to allow setting BVV default values
	sigmaData := make(map[string]sigma.Observation)
	for _, rubriek := range a.Cfg.Rubrieken {
		for _, attr := range rubriek.Attributes {
			if attr.Default != nil {
				sigmaData[attr.Name] = sigma.Observation{
					Value: &attr.Default,
				}
			}
		}
	}

	return c.Render("vreemdeling-new", fiber.Map{
		"rubrieken": a.Cfg.Rubrieken,
		"sigmaData": sigmaData,
	})
}

func (a *Application) CreateVreemdeling(c *fiber.Ctx) error {
	// Parse the request body
	var data createOrUpdateVreemdelingData
	if err := c.BodyParser(&data); err != nil {
		return fmt.Errorf("unexpected request body: %w", err)
	}

	// Get the user role
	role, ok := c.Locals("role").(config.Role)
	if !ok {
		return errors.New("error obtaining user role")
	}

	// Validate that the user has BVV write access
	if !a.Cfg.Organization.BVVWriteAccess || !role.Permissions.BVVWriteAccess {
		return errors.New("no permissions to store new vreemdelingen")
	}

	// Insert the vreemdeling into the BVV
	bvvClient, err := a.getBVVClient()
	if err != nil {
		return fmt.Errorf("error creating BVV client: %w", err)
	}

	ctx := context.Background()

	vwlID := uuid.New()
	activiteit := "https://example.com/activity/v0/vreemdeling-create"

	bd := openapi_types.Date(data.Geboortedatum)

	bvvResp, err := bvvClient.CreateVreemdelingWithResponse(ctx,
		&bvv.CreateVreemdelingParams{
			FSCVWLVerwerkingsActiviteit: &activiteit,
			FSCVWLVerwerkingsSpan:       &vwlID,
			FSCVWLSysteem:               &a.vwlSystem,
			FSCVWLVerwerker:             &a.Cfg.Organization.Name,
			FSCVWLVerantwoordelijke:     &a.Cfg.Organization.Name,
		},
		bvv.CreateVreemdelingJSONRequestBody{
			Data: bvv.VreemdelingWithoutId{
				Geboortedatum: &bd,
				Naam:          data.Naam,
			}})
	if err != nil {
		return fmt.Errorf("error inserting into BVV: %w", err)
	}

	// In case of an error, return
	if bvvResp.JSON400 != nil || bvvResp.JSON201 == nil {
		log.Printf("error creating person in BVV: %s", bvvResp.Body)
		return fmt.Errorf("fout bij aanmaken van de persoon: %s", bvvResp.Body)
	}

	vreemdelingNummer := bvvResp.JSON201.Data.Id

	// Insert the remaining attributes into Sigma
	if err = a.setSigmaRubrieken(ctx, vreemdelingNummer, data.Attributes, role); err != nil {
		return fmt.Errorf("error setting attributes in Sigma: %w", err)
	}

	// Show a page that the person has been inserted
	return c.Render("vreemdeling-new-finished", fiber.Map{
		"vreemdelingNummer": vreemdelingNummer,
	})
}

func (a *Application) GetVreemdeling(c *fiber.Ctx) error {
	// Get the vreemdeling from the BVV
	bvvClient, err := a.getBVVClient()
	if err != nil {
		return fmt.Errorf("error creating BVV client: %w", err)
	}

	vreemdelingNummer := c.Params("vreemdelingNummer")

	ctx := context.Background()

	var bvvResp *bvv.ReadVreemdelingByIdResponse

	vwlID := uuid.New()
	activiteit := "https://example.com/activity/v0/vreemdeling-read"

	if bvvResp, err = bvvClient.ReadVreemdelingByIdWithResponse(ctx, vreemdelingNummer, &bvv.ReadVreemdelingByIdParams{
		FSCVWLVerwerkingsActiviteit: &activiteit,
		FSCVWLVerwerkingsSpan:       &vwlID,
		FSCVWLSysteem:               &a.vwlSystem,
		FSCVWLVerwerker:             &a.Cfg.Organization.Name,
		FSCVWLVerantwoordelijke:     &a.Cfg.Organization.Name,
	}); err != nil {
		return fmt.Errorf("error fetching from BVV: %w", err)
	}

	// In case no results are found, return an error
	if bvvResp.JSON410 != nil {
		return errors.New("deze vreemdeling bestaat niet")
	}

	var bvvData bvv.Vreemdeling
	if res := bvvResp.JSON200; res != nil {
		bvvData = res.Data
	} else {
		return fmt.Errorf("unexpected response while fetching from BVV, http code %d, body: %s", bvvResp.StatusCode(), bvvResp.Body)
	}

	// Get additional data from the Sigma loket
	loketClient, err := a.getLoketClient()
	if err != nil {
		return fmt.Errorf("error creating Sigma loket client: %w", err)
	}

	// Get the user role
	role, ok := c.Locals("role").(config.Role)
	if !ok {
		return errors.New("error obtaining user role")
	}

	// Send the request to the Sigma loket
	loketResp, err := loketClient.ReadVreemdelingByIdWithResponse(ctx, vreemdelingNummer, &loket.ReadVreemdelingByIdParams{
		Attributes: role.ReadableRubriekAttributes(), // Pass the user's Get permissions as fields we want to fetch
	})
	if err != nil {
		return fmt.Errorf("error fetching data from Sigma loket: %w", err)
	}

	// In case of an error, return
	if loketResp.JSON400 != nil || loketResp.JSON410 != nil || loketResp.JSON200 == nil {
		log.Printf("error fetching person from Sigma loket: %s", loketResp.Body)
		return fmt.Errorf("fout bij ophalen van de persoon uit Sigma loket: %s", loketResp.Body)
	}

	// Get processen from the Sigma loket
	processenResp, err := loketClient.ReadProcessenByVreemdelingIdWithResponse(ctx, vreemdelingNummer, &loket.ReadProcessenByVreemdelingIdParams{})
	if err != nil {
		return fmt.Errorf("error fetching processen from Sigma loket: %w", err)
	}

	// In case of an error, return
	if processenResp.JSON400 != nil || processenResp.JSON200 == nil {
		log.Printf("error fetching processen from Sigma loket: %s", processenResp.Body)
		return fmt.Errorf("fout bij ophalen van de processen uit Sigma loket: %s", processenResp.Body)
	}

	// For each proces in the processen data, add the label and status label
	type procesWithLabelAndStatusLabel struct {
		loket.Proces
		Label       string
		StatusLabel string
	}
	processenData := make([]procesWithLabelAndStatusLabel, len(processenResp.JSON200.Data))
	for i, loketProces := range processenResp.JSON200.Data {
		processenData[i].Proces = loketProces

		// Find the proces in the config, based on its name/type
		for _, configProces := range a.Cfg.Processen {
			if configProces.Name == loketProces.Type {
				processenData[i].Label = configProces.Label

				// Find the status label in the configProces' statuses
				for _, status := range configProces.Statuses {
					if status.Name == loketProces.Status {
						processenData[i].StatusLabel = status.Label
						break
					}
				}

				break
			}
		}

		// In case no proces was found in the config, return an error
		if processenData[i].Label == "" { // Note: empty / default value
			log.Printf("unrecognized proces type: '%s'", loketProces.Type)
			processenData[i].Label = "(Onbekend proces)"
		}

		if processenData[i].StatusLabel == "" {
			log.Printf("unrecognized proces status '%s' of proces '%s'", loketProces.Status, loketProces.Type)
			processenData[i].StatusLabel = "(Onbekende status)"
		}
	}

	return c.Render("vreemdeling", fiber.Map{
		"bvvData":       bvvData,
		"rubrieken":     a.Cfg.Rubrieken,
		"sigmaData":     loketResp.JSON200.Data.AdditionalProperties,
		"processenData": processenData,
	})
}

func (a *Application) EditVreemdeling(c *fiber.Ctx) error {
	vreemdelingNummer := c.Params("vreemdelingNummer")

	// Get the user role
	role, ok := c.Locals("role").(config.Role)
	if !ok {
		return errors.New("error obtaining user role")
	}

	ctx := context.Background()

	// If the organization and the user have BVV write access, get the current values from the BVV
	var bvvData bvv.Vreemdeling
	if a.Cfg.Organization.BVVWriteAccess && role.Permissions.BVVWriteAccess {
		bvvClient, err := a.getBVVClient()
		if err != nil {
			return fmt.Errorf("error creating BVV client: %w", err)
		}

		vreemdelingNummer := c.Params("vreemdelingNummer")

		vwlID := uuid.New()
		activiteit := "https://example.com/activity/v0/vreemdeling-read"

		var bvvResp *bvv.ReadVreemdelingByIdResponse
		if bvvResp, err = bvvClient.ReadVreemdelingByIdWithResponse(ctx, vreemdelingNummer, &bvv.ReadVreemdelingByIdParams{
			FSCVWLVerwerkingsActiviteit: &activiteit,
			FSCVWLVerwerkingsSpan:       &vwlID,
			FSCVWLSysteem:               &a.vwlSystem,
			FSCVWLVerwerker:             &a.Cfg.Organization.Name,
			FSCVWLVerantwoordelijke:     &a.Cfg.Organization.Name,
		}); err != nil {
			return fmt.Errorf("error fetching from BVV: %w", err)
		}

		// In case no results are found, return an error
		if bvvResp.JSON410 != nil {
			return errors.New("deze vreemdeling bestaat niet")
		}

		if res := bvvResp.JSON200; res != nil {
			bvvData = res.Data
		} else {
			return fmt.Errorf("unexpected response while fetching from BVV, http code %d, body: %s", bvvResp.StatusCode(), bvvResp.Body)
		}
	}

	// Get additional data from Sigma
	sigmaClient, err := a.getSigmaClient()
	if err != nil {
		return fmt.Errorf("error creating Sigma loket client: %w", err)
	}

	// Send the request to the Sigma loket
	sigmaResp, err := sigmaClient.ReadVreemdelingByIdWithResponse(ctx, vreemdelingNummer, &sigma.ReadVreemdelingByIdParams{
		Attributes: role.ReadableRubriekAttributes(), // Pass the user's Get permissions as fields we want to fetch
	})
	if err != nil {
		return fmt.Errorf("error fetching data from Sigma: %w", err)
	}

	// In case of an error, return
	if sigmaResp.JSON400 != nil || sigmaResp.JSON410 != nil || sigmaResp.JSON200 == nil {
		log.Printf("error fetching person from Sigma: %s", sigmaResp.Body)
		return fmt.Errorf("fout bij ophalen van de persoon uit Sigma: %s", sigmaResp.Body)
	}

	return c.Render("vreemdeling-edit", fiber.Map{
		"vreemdelingNummer": vreemdelingNummer,
		"rubrieken":         a.Cfg.Rubrieken,
		"bvvData":           bvvData,
		"sigmaData":         sigmaResp.JSON200.Data,
	})
}

func (a *Application) UpdateVreemdeling(c *fiber.Ctx) error {
	// Parse the request body
	var data createOrUpdateVreemdelingData
	if err := c.BodyParser(&data); err != nil {
		return fmt.Errorf("unexpected request body: %w", err)
	}

	vreemdelingNummer := c.Params("vreemdelingNummer")

	// Get the user role
	role, ok := c.Locals("role").(config.Role)
	if !ok {
		return errors.New("error obtaining user role")
	}

	ctx := context.Background()

	// If the organization and the user has BVV write access, update the vreemdeling in the BVV
	if a.Cfg.Organization.BVVWriteAccess && role.Permissions.BVVWriteAccess {
		bvvClient, err := a.getBVVClient()
		if err != nil {
			return fmt.Errorf("error creating BVV client: %w", err)
		}

		vwlID := uuid.New()
		activiteit := "https://example.com/activity/v0/vreemdeling-update"

		bd := openapi_types.Date(data.Geboortedatum)
		bvvResp, err := bvvClient.UpdateVreemdelingByIdWithResponse(ctx, vreemdelingNummer, &bvv.UpdateVreemdelingByIdParams{
			FSCVWLVerwerkingsActiviteit: &activiteit,
			FSCVWLVerwerkingsSpan:       &vwlID,
			FSCVWLSysteem:               &a.vwlSystem,
			FSCVWLVerwerker:             &a.Cfg.Organization.Name,
			FSCVWLVerantwoordelijke:     &a.Cfg.Organization.Name,
		}, bvv.UpdateVreemdelingByIdJSONRequestBody{
			Data: bvv.Vreemdeling{
				Id:            vreemdelingNummer,
				Geboortedatum: &bd,
				Naam:          data.Naam,
			},
		})
		if err != nil {
			return fmt.Errorf("error updating in BVV: %w", err)
		}

		// In case of an error, return
		if bvvResp.JSON400 != nil || bvvResp.JSON410 != nil || bvvResp.JSON200 == nil {
			log.Printf("error updating person in BVV: %s", bvvResp.Body)
			return fmt.Errorf("fout bij aanpassen van de persoon: %s", bvvResp.Body)
		}
	}

	// Update the remaining attributes into Sigma
	if err := a.setSigmaRubrieken(ctx, vreemdelingNummer, data.Attributes, role); err != nil {
		return fmt.Errorf("error setting attributes in Sigma: %w", err)
	}

	// Show a page that the person has been inserted
	return c.Render("vreemdeling-edit-finished", fiber.Map{
		"vreemdelingNummer": vreemdelingNummer,
	})
}

func (a *Application) setSigmaRubrieken(ctx context.Context, vreemdelingNummer string, attributes dataAttributes, role config.Role) error {
	// Create a Sigma client
	sigmaClient, err := a.getSigmaClient()
	if err != nil {
		return fmt.Errorf("error creating Sigma client: %w", err)
	}

	writableAttributes := role.WritableRubriekAttributes()

	// Range over all submitted attributes
	for _, dataAttr := range attributes {
		// Get the corresponding rubriek from the config
		found := false

		var attr *config.Attribute
		for i, rubriek := range a.Cfg.Rubrieken {
			for j, at := range rubriek.Attributes {
				if at.Name == dataAttr.Name {
					attr = &a.Cfg.Rubrieken[i].Attributes[j] // Note: instead of &at, which would be incorrect due to the loop
					found = true
					break
				}
			}
		}

		if !found {
			continue // Skip if the attribute name/key is not found in the config
		}

		// Skip unset or empty elements. IMPROVE: implement some way in the backend and frontend to pass null values? Now e.g. there is no difference between cleared checkboxes and skipped checkboxes
		if len(dataAttr.Value) == 0 || dataAttr.Value[0] == "" {
			continue
		}

		// Skip attributes for which the role has no 'set' permissions
		if !slices.Contains[[]string](writableAttributes, dataAttr.Name) {
			log.Printf("user role %s tried to set vreemdeling attribute %s for which it has no permissions, ignoring", role.Name, dataAttr.Name)
			continue
		}

		val, err := a.parseAttribute(dataAttr.Value, attr)
		if err != nil {
			return fmt.Errorf("error parsing attribute '%s' (value: %v). error: %w", attr.Name, dataAttr.Value, err)
		}

		id := uuid.New()
		activiteit := "https://example.com/activity/v0/observation-create"

		params := &sigma.CreateObservationByVreemdelingenIdParams{
			FSCVWLVerwerkingsActiviteit: &activiteit,
			FSCVWLVerwerkingsSpan:       &id,
			FSCVWLSysteem:               &a.vwlSystem,
			FSCVWLVerwerker:             &a.Cfg.Organization.Name,
			FSCVWLVerantwoordelijke:     &a.Cfg.Organization.Name,
		}

		body := sigma.CreateObservationByVreemdelingenIdJSONRequestBody{
			Data: sigma.ObservationCreate{
				Attribute: attr.Name,
				Value:     val,
			},
		}

		sigmaResp, err := sigmaClient.CreateObservationByVreemdelingenIdWithResponse(ctx, vreemdelingNummer, params, body)
		if err != nil {
			return fmt.Errorf("error inserting into Sigma: %w", err)
		}

		log.Printf("--> sigma response when setting rubriek: %s", sigmaResp.Body)
	}

	return nil
}
