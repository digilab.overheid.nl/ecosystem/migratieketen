FROM --platform=$BUILDPLATFORM digilabpublic.azurecr.io/golang:1.22.2-alpine3.19

RUN ["go", "install", "github.com/githubnemo/CompileDaemon@latest"]

# Install Node.js. Note: the nodejs package does not include corepack, so we use nodejs-current
RUN apk add --no-cache nodejs-current

RUN corepack enable && corepack prepare pnpm@latest --activate

# Cache dependencies
WORKDIR /build/fictief-bvv-api
COPY fictief-bvv-api/go.mod fictief-bvv-api/go.sum ./

WORKDIR /build/fictief-loket-api
COPY fictief-loket-api/go.mod fictief-loket-api/go.sum ./

WORKDIR /build/fictief-sigma-api
COPY fictief-sigma-api/go.mod fictief-sigma-api/go.sum ./

WORKDIR /build/notification-api
COPY notification-api/go.mod notification-api/go.sum ./

WORKDIR /build/frontend
COPY frontend/go.mod frontend/go.sum ./

RUN go mod download

WORKDIR /build/fictief-bvv-api
COPY fictief-bvv-api ./

WORKDIR /build/fictief-loket-api
COPY fictief-loket-api ./

WORKDIR /build/fictief-sigma-api
COPY fictief-sigma-api ./

WORKDIR /build/notification-api
COPY notification-api ./

WORKDIR /build/frontend
COPY frontend/package.json frontend/pnpm-lock.yaml frontend/tailwind.config.js frontend/.postcssrc frontend/.parcelrc ./

RUN pnpm install

COPY frontend/main.go .
COPY frontend/application application
COPY frontend/cmd cmd
COPY frontend/config config
COPY frontend/helpers helpers

COPY frontend/views views
COPY frontend/static static

COPY config /build/config

ENTRYPOINT sh -c 'pnpm run dev & CompileDaemon -log-prefix=false -pattern="(.+\.go|.+\.html|.+\.js|.+\.css|.+\.sql)$" -exclude-dir=.git -exclude-dir=node_modules -exclude-dir=.parcel-cache -build="go build -o server ." -command="./server serve"'
