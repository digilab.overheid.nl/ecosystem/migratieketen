# available variables:
# - $vreemdeling_id - id of newly created vreemdeling. Do not delete this vreemdeling, because other tests may depend on it.

echo 'Add attribute observation to SIGMA:'
resp=$(echo '{
    "data": {
        "attribute": "alias",
        "value": "foo"
    }
}' | curl $CURL_FLAGS \
    -H 'accept: application/json' \
    -X 'POST' \
    --data @- \
    "http://np-sigma-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/$vreemdeling_id/observations" \
)
printf '%s' "$resp" | jq .

echo 'Get some attributes from a specific SIGMA:'
resp=$(curl $CURL_FLAGS \
    -H 'accept: application/json' \
    "http://np-sigma-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/$vreemdeling_id?attributes=alias,spreektalen" \
)
printf '%s' "$resp" | jq .
