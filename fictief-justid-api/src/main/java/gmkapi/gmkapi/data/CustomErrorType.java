package gmkapi.gmkapi.data;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
public class CustomErrorType {
    private String errorMessage;
}
