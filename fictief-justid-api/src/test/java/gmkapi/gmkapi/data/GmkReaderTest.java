package gmkapi.gmkapi.data;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Slf4j
class GmkReaderTest {

    @Autowired
    GmkReader gmkReader;
    @Autowired
    private WebApplicationContext context;

    protected MockMvc mvc;

    @BeforeEach
    public final void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();

    }

    protected void getContentAndVerifyResult(String path) throws Exception {
        mvc.perform(get(path))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON));
    }

    protected MvcResult getContentAndReturnResult(String path) throws Exception {
        return mvc.perform(get(path))
                .andReturn();
    }


//    @Test
//    void testGmkViaAPI() throws Exception {
//        String uuid = "/api/gmk/fe8c0c28-a82d-43ea-8d05-d7c10010c91a";
//
//        MvcResult result = getContentAndReturnResult(uuid);
//        log.info(result.getResponse().getContentAsString());
//        //assertEquals("verblijfsvergunning asiel voor onbepaalde tijd", gmkResources.getNaam());
//    }


    @Test
    void testGmkReadsCorrect() {
        String uuid = "fe8c0c28-a82d-43ea-8d05-d7c10010c91a";
        GmkResources gmkResources = gmkReader.readGmkResourcesFromSource(uuid);
        log.info(gmkResources.toString());
        assertEquals("verblijfsvergunning asiel voor onbepaalde tijd", gmkResources.getNaam());
    }

    @Test
    void testGmkReadsWithContextCorrect() {
        String context = "Juridisch";
        String naam = "vreemdelingenrecht";
        GmkResources gmkResources = gmkReader.readGmkResourcesFromContextNaam(context, naam);
        log.info(gmkResources.toString());
        assertEquals("vreemdelingenrecht", gmkResources.getNaam());
    }

    @Test
    void testGmkReadBronnen() {
        String uuid = "d29bb57b-f22a-45e2-a970-304d673df36d";
        GmkBronnen gmkBronnen = gmkReader.readGmkBronnenFromSource(uuid);
        log.info(gmkBronnen.toString());
        assertEquals("artikel 33 van de Vreemdelingenwet 2000", gmkBronnen.getValue());
    }


    @Configuration
    @EnableWebMvc
    @ComponentScan(basePackages = {
            "gmkapi.gmkapi.data"
    })
    static class Config {
    }

}
