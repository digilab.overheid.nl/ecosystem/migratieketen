FROM --platform=$BUILDPLATFORM digilabpublic.azurecr.io/golang:1.22.2-alpine3.19 as builder

# Cache dependencies
RUN ["go", "install", "github.com/githubnemo/CompileDaemon@latest"]

WORKDIR /build/ldv-metadata-enrichen-processor
COPY ldv-metadata-enrichen-processor/go.mod ldv-metadata-enrichen-processor/go.sum ./

WORKDIR /build/ldv-metadata-enrichen-processor/api
COPY ldv-metadata-enrichen-processor/api/go.mod ldv-metadata-enrichen-processor/api/go.sum ./

WORKDIR /build/bsn-pseudonimiseren-processor
COPY bsn-pseudonimiseren-processor/go.mod bsn-pseudonimiseren-processor/go.sum ./

WORKDIR /build/vid-bloom-filter-exporter
COPY vid-bloom-filter-exporter/go.mod vid-bloom-filter-exporter/go.sum ./

WORKDIR /build/collector
COPY collector/go.mod collector/go.sum ./

RUN go mod download

## Build the Go Files
WORKDIR /build/ldv-metadata-enrichen-processor
COPY ldv-metadata-enrichen-processor .

WORKDIR /build/bsn-pseudonimiseren-processor
COPY bsn-pseudonimiseren-processor .

WORKDIR /build/vid-bloom-filter-exporter
COPY vid-bloom-filter-exporter .

WORKDIR /build/collector
COPY collector .

EXPOSE 4317 55680 55679

ENTRYPOINT CompileDaemon -log-prefix=false -build="go build -o server ." -command="./server --config=env:OTEL_CONFIG"
