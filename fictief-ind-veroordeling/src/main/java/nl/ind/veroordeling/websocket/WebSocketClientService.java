package nl.ind.veroordeling.websocket;

import jakarta.annotation.PostConstruct;
import java.net.URI;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
@Service

public class WebSocketClientService {
    private WebSocketClient webSocketClient;
    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${vo.ws-url}")
    private String ws_url;


    @PostConstruct
    public void init() {
        try {
            URI uri = new URI(ws_url); // Replace with the actual WebSocket server URI
            webSocketClient = new WebSocketClient(uri, restTemplate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        webSocketClient.sendMessage(message);
    }

}
