package exporter

type Config struct {
	Clickhouse struct {
		Endpoint string `mapstructure:"endpoint"`
		Database string `mapstructure:"database"`
		User     string `mapstructure:"user"`
		Password string `mapstructure:"password"`
	} `mapstructure:"clickhouse"`
}
