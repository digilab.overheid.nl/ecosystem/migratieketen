package exporter

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"github.com/ClickHouse/ch-go"
	"github.com/ClickHouse/ch-go/proto"
	"github.com/bits-and-blooms/bitset"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/consumer"
	"go.opentelemetry.io/collector/pdata/ptrace"

	"github.com/spaolacci/murmur3"
)

type vidBloomFilterExporter struct {
	size uint
	b    *bitset.BitSet
	cfg  *Config
	conn *ch.Client
}

var lookupQuery = "SELECT value FROM bf_1024_1;"

func newExporter(cfg *Config) (*vidBloomFilterExporter, error) {
	conn, err := connect(context.Background(), cfg.Clickhouse.Endpoint, cfg.Clickhouse.Database, cfg.Clickhouse.User, cfg.Clickhouse.Password)
	if err != nil {
		return nil, err
	}

	size := uint(1024)

	s := &vidBloomFilterExporter{
		size: size,
		b:    bitset.New(size),
		cfg:  cfg,
		conn: conn,
	}

	if err := s.getBloomFilter(context.Background()); err != nil {
		return nil, err
	}

	if err := s.storeBloomfilter(context.Background()); err != nil {
		return nil, err
	}

	return s, nil
}

func (s *vidBloomFilterExporter) getBloomFilter(ctx context.Context) error {
	vid := proto.ColFixedStr{}
	vid.SetSize(128)

	hasBloomfilterData := false

	query := ch.Query{
		Body: lookupQuery,
		Result: proto.Results{
			{Name: "value", Data: &vid},
		},
		OnResult: func(ctx context.Context, block proto.Block) error {
			hasBloomfilterData = (block.Rows > 0)
			return nil
		},
	}

	if err := s.conn.Do(ctx, query); err != nil {
		return err
	}

	if !hasBloomfilterData {
		if err := s.createBloomFilter(ctx); err != nil {
			return err
		}
	}

	for index, value := range vid.Buf {
		for i, j := range strconv.FormatInt(int64(value), 2) {
			b, _ := strconv.ParseBool(string(j))
			s.b.SetTo(uint((index*8)+i), b)
		}
	}

	return nil
}

func (s *vidBloomFilterExporter) createBloomFilter(ctx context.Context) error {
	query := ch.Query{
		Body: "insert into bf_1024_1 (value) VALUES (0)",
	}

	if err := s.conn.Do(ctx, query); err != nil {
		return err
	}

	return nil
}

func (s *vidBloomFilterExporter) ConsumeTraces(ctx context.Context, batch ptrace.Traces) error {
	for i := range batch.ResourceSpans().Len() {
		// we split the incoming batch into a collection of ResourceSpans
		rs := batch.ResourceSpans().At(i)

		for j := range rs.ScopeSpans().Len() {
			ss := rs.ScopeSpans().At(j)

			for k := range ss.Spans().Len() {
				attr := ss.Spans().At(k).Attributes()
				if v, ok := attr.Get("dpl.core.user"); ok {
					if id, found := strings.CutPrefix(v.AsString(), "vid:"); found {
						for i := uint(0); i < 1; i++ {
							s.b.Set(s.location(baseHash([]byte(id)), uint(1)))
						}
					}
				}
			}
		}
	}

	if err := s.storeBloomfilter(ctx); err != nil {
		return err
	}

	return nil
}

func (s *vidBloomFilterExporter) storeBloomfilter(ctx context.Context) error {
	bits := strings.Repeat("0", int(s.size))

	indices := make([]uint, s.b.Count())
	_, x := s.b.NextSetMany(0, indices)
	for _, i := range x {
		bits = replaceAtIndex(bits, '1', int(i))
	}

	if err := s.conn.Do(ctx, ch.Query{
		Body: fmt.Sprintf("ALTER TABLE %s.bf_1024_1 UPDATE value = unbin('%s') where value != 'b\\0'", s.cfg.Clickhouse.Database, bits),
	}); err != nil {
		return fmt.Errorf("updating %s.bf_1024_1 failed: %w", s.cfg.Clickhouse.Database, err)
	}

	return nil
}

func replaceAtIndex(in string, r rune, i int) string {
	out := []rune(in)
	out[i] = r
	return string(out)
}

// functions below are extracted from https://github.com/bits-and-blooms/bloom
func (s *vidBloomFilterExporter) location(h [4]uint64, i uint) uint {
	ii := uint64(i)
	return uint(h[ii%2]+ii*h[2+(((ii+(ii%2))%4)/2)]) % s.size
}

func baseHash(data []byte) [4]uint64 {
	h1, h2, h3, h4 := hash(data)
	return [4]uint64{
		h1, h2, h3, h4,
	}
}

func hash(data []byte) (h1, h2, h3, h4 uint64) {
	a1 := []byte{1}
	hasher := murmur3.New128()
	hasher.Write(data) // #nosec
	v1, v2 := hasher.Sum128()
	hasher.Write(a1) // #nosec
	v3, v4 := hasher.Sum128()
	return v1, v2, v3, v4
}

func connect(ctx context.Context, url, db, user, password string) (*ch.Client, error) {
	options := ch.Options{
		Address:  url,
		Database: db,
		User:     user,
		Password: password,
	}

	client, err := ch.Dial(ctx, options)
	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}

	if err := client.Ping(ctx); err != nil {
		return nil, err
	}

	return client, nil
}

func (s *vidBloomFilterExporter) Capabilities() consumer.Capabilities {
	return consumer.Capabilities{
		MutatesData: false,
	}
}

func (s *vidBloomFilterExporter) Start(_ context.Context, host component.Host) error {
	return nil
}

func (s *vidBloomFilterExporter) Shutdown(context.Context) error {
	return nil
}
