package httpapi.inway

import rego.v1

default allow := {
	"allowed": false,
	"status": {"reason": "permission denied"},
}

disallowed_attributes := {"verboden": true}

attributes_disallowed contains item if {
	provided_attributes := split(input.query_attributes, ",")
	some item in provided_attributes
	disallowed_attributes[item] == true
}

match_attribute if {
	count(attributes_disallowed) == 0
}

allow := {"allowed": true, "status": {"reason": reason}} if {
	match_attribute

	reason := "all clear"
}

allow := {"allowed": false, "status": {"reason": reason}} if {
	not match_attribute

	output := concat(",", attributes_disallowed)
	reason := sprintf("attributes not allowed: `%s`", [output])
}
