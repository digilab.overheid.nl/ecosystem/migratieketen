#!/usr/bin/env bash

set -euo pipefail

kubectl -n rrd exec -it -c postgres rrd-common-db-1 -- /usr/bin/pg_dump -xO nlx_manager > deploy/fixtures/rrd_nlx_manager.sql
kubectl -n rrd exec -it -c postgres rrd-common-db-1 -- /usr/bin/pg_dump -xO nlx_controller > deploy/fixtures/rrd_nlx_controller.sql
kubectl -n migratieketen exec -it -c postgres shared-common-db-1 -- /usr/bin/pg_dump -xO nlx_manager > deploy/fixtures/shared_nlx_manager.sql
kubectl -n migratieketen exec -it -c postgres shared-common-db-1 -- /usr/bin/pg_dump -xO nlx_controller > deploy/fixtures/shared_nlx_controller.sql
