--
-- PostgreSQL database dump
--

-- Dumped from database version 16.3 (Debian 16.3-1.pgdg110+1)
-- Dumped by pg_dump version 16.3 (Debian 16.3-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: contracts; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA contracts;


--
-- Name: peers; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA peers;


--
-- Name: content_hash_algorithm; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.content_hash_algorithm AS ENUM (
    'sha3_512'
);


--
-- Name: service_protocol_type; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.service_protocol_type AS ENUM (
    'PROTOCOL_TCP_HTTP_1.1',
    'PROTOCOL_TCP_HTTP_2'
);


--
-- Name: signature_type; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.signature_type AS ENUM (
    'accept',
    'reject',
    'revoke'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: content; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.content (
    hash character varying(150) NOT NULL,
    hash_algorithm contracts.content_hash_algorithm NOT NULL,
    id uuid NOT NULL,
    group_id character varying(255) NOT NULL,
    valid_not_before timestamp with time zone NOT NULL,
    valid_not_after timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL
);


--
-- Name: contract_signatures; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.contract_signatures (
    content_hash character varying(150) NOT NULL,
    signature_type contracts.signature_type NOT NULL,
    peer_id character varying(20) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL,
    signature character varying(2048) NOT NULL,
    signed_at timestamp with time zone NOT NULL
);


--
-- Name: grants_delegated_service_connection; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_delegated_service_connection (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    outway_peer_id character varying(20) NOT NULL,
    delegator_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL
);


--
-- Name: grants_delegated_service_publication; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_delegated_service_publication (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    delegator_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    service_protocol contracts.service_protocol_type NOT NULL
);


--
-- Name: grants_service_connection; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_service_connection (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    consumer_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL
);


--
-- Name: grants_service_publication; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_service_publication (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    service_protocol contracts.service_protocol_type NOT NULL
);


--
-- Name: valid_contracts; Type: VIEW; Schema: contracts; Owner: -
--

CREATE VIEW contracts.valid_contracts AS
 SELECT hash
   FROM contracts.content content
  WHERE ((valid_not_before < now()) AND (valid_not_after > now()) AND (( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'reject'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text))) = 0) AND (( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'revoke'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text))) = 0) AND (( SELECT count(DISTINCT p.id) AS count
           FROM ( SELECT grants_service_publication.directory_peer_id AS id
                   FROM contracts.grants_service_publication
                  WHERE ((grants_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_publication.service_peer_id AS id
                   FROM contracts.grants_service_publication
                  WHERE ((grants_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.consumer_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.service_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.outway_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.service_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.directory_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)) p) = ( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'accept'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text)))));


--
-- Name: certificates; Type: TABLE; Schema: peers; Owner: -
--

CREATE TABLE peers.certificates (
    certificate_thumbprint character varying(512) NOT NULL,
    peer_id character varying(20) NOT NULL,
    certificate bytea NOT NULL
);


--
-- Name: peers; Type: TABLE; Schema: peers; Owner: -
--

CREATE TABLE peers.peers (
    id character varying(20) NOT NULL,
    name character varying(60),
    manager_address character varying(255)
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    dirty boolean NOT NULL
);


--
-- Data for Name: content; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.content (hash, hash_algorithm, id, group_id, valid_not_before, valid_not_after, created_at) FROM stdin;
\.


--
-- Data for Name: contract_signatures; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.contract_signatures (content_hash, signature_type, peer_id, certificate_thumbprint, signature, signed_at) FROM stdin;
\.


--
-- Data for Name: grants_delegated_service_connection; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_delegated_service_connection (hash, content_hash, outway_peer_id, delegator_peer_id, service_peer_id, service_name, certificate_thumbprint) FROM stdin;
\.


--
-- Data for Name: grants_delegated_service_publication; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_delegated_service_publication (hash, content_hash, directory_peer_id, delegator_peer_id, service_peer_id, service_name, service_protocol) FROM stdin;
\.


--
-- Data for Name: grants_service_connection; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_service_connection (hash, content_hash, consumer_peer_id, service_peer_id, service_name, certificate_thumbprint) FROM stdin;
\.


--
-- Data for Name: grants_service_publication; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_service_publication (hash, content_hash, directory_peer_id, service_peer_id, service_name, service_protocol) FROM stdin;
\.


--
-- Data for Name: certificates; Type: TABLE DATA; Schema: peers; Owner: -
--

COPY peers.certificates (certificate_thumbprint, peer_id, certificate) FROM stdin;
U8Q1EXydxE--72ESaGOMHy-4PJpJ9LA2lYdHuOPjJqk	12345678901234567899	\\x3082066130820449a00302010202142bf0ba0903a66c72c8b86ee300e9207fae8ee227300d06092a864886f70d01010d0500305f310b3009060355040613024e4c311630140603550408130d4e6f6f72642d486f6c6c616e643112301006035504071309416d7374657264616d31163014060355040a130d436f6d6d6f6e2047726f756e64310c300a060355040b13034e4c58301e170d3233313231313230303130305a170d3236313231303230303130305a3081a8310b3009060355040613024e4c3110300e06035504081307557472656368743110300e060355040713075574726563687431273025060355040a131e72696a6b736469656e73742d7265616c69737469736368652d64656d6f73312d302b060355040313246d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e727264311d301b06035504051314313233343536373839303132333435363738393930820222300d06092a864886f70d01010105000382020f003082020a0282020100a86e510c06ecffb77972afb65a155bb55b129f5d2bac39288d1e211190427d485e1ae6e73d77773e18282087d817cf66b8c3ea9a35a1891fb9ee0dd0c9c5b19977101f85c9927506c045aca724abdf8c823916f14cdfe5672b182950996dcc9bbb796af5f623ec57c78aaa72bce244d95268c8c5c2900e841ffaf9a8a1520f44f2a38a5882e7be5eb26c44a12c05628d9dd1329470c0df4ce28d60a15b6122afc98cff0e44f3394b2f0156114f9f11a5d9f76ffa061f03a797cd3abdf65fd3859d52cd8e431308db9d2e7a7b97b8e20b10a482285f35c8a970d4c6cd3bbddd0b9121ca3c5a330d8888edadeae32fbb63a8e324d520a2562de63a02c62677e1d8adf875edffbad07084dcc1fd175c0e2702b0a7f556d781ce58f4cbc3f24d458bbcd52ad07b5ce7d89a317a14235ac0221d70a66b1fbc6fa3154aa5e481dd3417c6d1474db45b10eebd4c3fa30e4d370510d5af69326011639902b234d6b68e228bd45170b25cfdd021bbc7cd6e64f1f72be9908a52f9ae2451d439aadae564f756d7a034bfdb7d4aee53ddb7e6bb626239a2a33b8b1519fdae1ec4529cd1dabeb467ae7c1c97c3eb0f751a97830fcf3ee1f178189e04846c1f07fd746074282ea7f46015e3325c5cd78307f4ef242a10a4d427a886d97f043e430e42a3f0050b9b2e7672ff9d8f92e393513f5949727a26b0ad993c525186cdbf8e0b729c76e70203010001a381ca3081c7300e0603551d0f0101ff0404030205a0301d0603551d250416301406082b0601050507030106082b06010505070302300c0603551d130101ff04023000301d0603551d0e041604147c494af41e3480d4cfd407b0bea19c528c4e04a7301f0603551d230418301680145ae041511187acda6887c000c3065ce2792d89fd30480603551d110441303f82246d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e7272648217696e7761792d6673632d6e6c782d696e7761792e727264300d06092a864886f70d01010d050003820201007dc219c797b930ff772e52f740fd325478b854d1577700b647c02f3aeec9c4bda9d7fb2f87af100fb97e72d1e80f978300c45f0c90226b1f554dbc2e1c91d9c2a1205373dab6a18c251d81e20efd4c569a5f73caacbf4589af156157af771f1a0fcad068d6c1344c798acb9f0c188214e158c1a8a221ba11bc87b026d5baf7d058df6cf5f489780ea64fe8ea265c4ed39e5da8f21d43a2735004b6f0b3dafab158e67f71f71df96d25ec95fabc1fd8b5063001a166506c366843069b03063781c53778ef6437ae5a6f4163e4cfc5f1713242c69cf23a624dcbab87e783ef7617004d986c404436bb144ddabd56eb0b1802193470fd26748e7805709e10e275d01e839e8c7945486a03a762debe5991052abefbf0062420224bc988f72e482a08207b17737fff868f467afbd3af6ed1bf92d206b5f2ed1a9688fe1385729d86ab6bb903c4e4b742ece646c47ec106563ed5880f19e52065f7a69ed75ba2ad135f1b80c49772cf468f542ed4ac51294f9e6a9c2889b32b063ea96403bae416a741cdd71f6c461f94ae88cb458b7355e8c6b1e60f69a5788b5f9411d26dbf20ec25b475b2ed138faee5576fd96a551813512425f3d6cc5bec4ec7392200476c2fdc9c2ad0fe6da05de2c8951af260cfe0c40a655dd54cc45d47a95e8dc290fadba0a1ec0f3c4ad5294b9588fe3adbaf34385a9aef086f7d05c7d6170b3d46d8b712
\.


--
-- Data for Name: peers; Type: TABLE DATA; Schema: peers; Owner: -
--

COPY peers.peers (id, name, manager_address) FROM stdin;
12345678901234567899	rijksdienst-realistische-demos	https://manager-fsc-nlx-manager-external.rrd:8443
01702331477921460562	shared	https://manager-fsc-nlx-manager-external.migratieketen:8443
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.schema_migrations (version, dirty) FROM stdin;
1	f
\.


--
-- Name: content content_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.content
    ADD CONSTRAINT content_pk PRIMARY KEY (hash);


--
-- Name: contract_signatures contract_signatures_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_pk PRIMARY KEY (content_hash, signature_type, peer_id);


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_pk PRIMARY KEY (hash);


--
-- Name: grants_delegated_service_publication grants_delegated_service_publication_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_publication
    ADD CONSTRAINT grants_delegated_service_publication_pk PRIMARY KEY (hash);


--
-- Name: grants_service_connection grants_service_connection_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connection_pk PRIMARY KEY (hash);


--
-- Name: grants_service_publication grants_service_publication_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_pk PRIMARY KEY (hash);


--
-- Name: certificates certificates_pk; Type: CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.certificates
    ADD CONSTRAINT certificates_pk PRIMARY KEY (certificate_thumbprint);


--
-- Name: peers peers_pk; Type: CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.peers
    ADD CONSTRAINT peers_pk PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: grants_delegated_service_connection_content_hash_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_content_hash_idx ON contracts.grants_delegated_service_connection USING btree (content_hash);


--
-- Name: grants_delegated_service_connection_delegator_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_delegator_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (delegator_peer_id);


--
-- Name: grants_delegated_service_connection_outway_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_outway_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (outway_peer_id);


--
-- Name: grants_delegated_service_connection_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_service_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (service_peer_id);


--
-- Name: grants_service_connection_consumer_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_consumer_peer_id_idx ON contracts.grants_service_connection USING btree (consumer_peer_id);


--
-- Name: grants_service_connection_content_hash_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_content_hash_idx ON contracts.grants_service_connection USING btree (content_hash);


--
-- Name: grants_service_connection_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_service_peer_id_idx ON contracts.grants_service_connection USING btree (service_peer_id);


--
-- Name: grants_service_publication_directory_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_publication_directory_peer_id_idx ON contracts.grants_service_publication USING btree (directory_peer_id);


--
-- Name: grants_service_publication_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_publication_service_peer_id_idx ON contracts.grants_service_publication USING btree (service_peer_id);


--
-- Name: certificates_certificate_thumbprint_idx; Type: INDEX; Schema: peers; Owner: -
--

CREATE INDEX certificates_certificate_thumbprint_idx ON peers.certificates USING btree (certificate_thumbprint);


--
-- Name: certificates_peer_id_idx; Type: INDEX; Schema: peers; Owner: -
--

CREATE INDEX certificates_peer_id_idx ON peers.certificates USING btree (peer_id);


--
-- Name: contract_signatures contract_signatures_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: contract_signatures contract_signatures_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_delegator_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_delegator_peer_id_fk FOREIGN KEY (delegator_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_outway_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_outway_peer_id_fk FOREIGN KEY (outway_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connection_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connection_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connections_consumer_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connections_consumer_peer_id_fk FOREIGN KEY (consumer_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connections_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connections_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_publication grants_service_publication_directory_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_directory_peer_id_fk FOREIGN KEY (directory_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_publication grants_service_publication_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: certificates certificates_peer_id_fk; Type: FK CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.certificates
    ADD CONSTRAINT certificates_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--
