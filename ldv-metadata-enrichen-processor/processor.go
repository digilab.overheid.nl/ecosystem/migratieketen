package processor

import (
	"context"

	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/ldv-metadata-enrichen-processor/config"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/consumer"
	"go.opentelemetry.io/collector/processor"
)

var t = component.MustNewType("ldv_metadata_enrichen_processor")

func NewFactory() processor.Factory {
	return processor.NewFactory(t, createDefaultConfig, processor.WithTraces(createTraceProcessor, component.StabilityLevelAlpha))
}

func createTraceProcessor(_ context.Context, _ processor.CreateSettings, cfg component.Config, nextConsumer consumer.Traces) (processor.Traces, error) {
	tracer, err := NewMetadataEnrichBatch(cfg, nextConsumer)
	if err != nil {
		return nil, err
	}

	return tracer, nil
}

func createDefaultConfig() component.Config {
	return &config.Config{
		EndpointRVVA: "",
	}
}
