package processor

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"time"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/ldv-metadata-enrichen-processor/api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/ldv-metadata-enrichen-processor/config"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/consumer"
	"go.opentelemetry.io/collector/pdata/ptrace"
	"go.opentelemetry.io/collector/processor"
)

var _ processor.Traces = (*MetadataEnrichBatch)(nil)

type MetadataEnrichBatch struct {
	traces consumer.Traces
	client *api.ClientWithResponses
	log    *slog.Logger
}

func NewMetadataEnrichBatch(compcfg component.Config, traces consumer.Traces) (*MetadataEnrichBatch, error) {
	cfg, ok := compcfg.(*config.Config)
	if !ok {
		return nil, fmt.Errorf("config not valid: %v", compcfg)
	}

	client, err := api.NewClientWithResponses(cfg.EndpointRVVA)
	if err != nil {
		return nil, err
	}

	return &MetadataEnrichBatch{
		traces: traces,
		client: client,
		log:    slog.New(slog.NewJSONHandler(os.Stdout, nil)),
	}, nil
}

func (s *MetadataEnrichBatch) ConsumeTraces(ctx context.Context, batch ptrace.Traces) error {
	for i := range batch.ResourceSpans().Len() {
		// we split the incoming batch into a collection of ResourceSpans
		rs := batch.ResourceSpans().At(i)

		for j := range rs.ScopeSpans().Len() {
			ss := rs.ScopeSpans().At(j)

			for k := range ss.Spans().Len() {
				span := ss.Spans().At(k)
				if activityID, found := span.Attributes().Get("dpl.rva.activity.id"); found {
					activityID.AsString()

					resp, err := s.client.GetProcessingActivityWithResponse(ctx, activityID.AsString())
					if err != nil {
						s.log.Error("get processing activity", "err", err)
						continue
					}

					if resp.JSON200 == nil {
						s.log.Error("get processing activity response error", "body", string(resp.Body))
						continue
					}

					activity := resp.JSON200

					if activity.EnvisagedTimeLimitDuration != nil {
						duration, err := time.ParseDuration(*activity.EnvisagedTimeLimitDuration)
						if err != nil {
							s.log.Error("envisaged time limit duration parse", "err", err)
							continue
						}

						span.Attributes().PutStr("dpl.core.delete_after", time.Now().Add(duration).Format(time.RFC3339))
					}
				}
			}
		}
	}

	if err := s.traces.ConsumeTraces(ctx, batch); err != nil {
		return err
	}

	return nil
}

func (s *MetadataEnrichBatch) Capabilities() consumer.Capabilities {
	return consumer.Capabilities{
		MutatesData: false,
	}
}

func (s *MetadataEnrichBatch) Start(_ context.Context, host component.Host) error {
	return nil
}

func (s *MetadataEnrichBatch) Shutdown(context.Context) error {
	return nil
}
