package processor_test

import (
	"context"
	"fmt"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	processor "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/ldv-metadata-enrichen-processor"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/ldv-metadata-enrichen-processor/config"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/connector"
	"go.opentelemetry.io/collector/connector/connectortest"
	"go.opentelemetry.io/collector/exporter"
	"go.opentelemetry.io/collector/exporter/debugexporter"
	"go.opentelemetry.io/collector/exporter/exportertest"
	"go.opentelemetry.io/collector/extension"
	"go.opentelemetry.io/collector/extension/extensiontest"
	"go.opentelemetry.io/collector/otelcol"
	"go.opentelemetry.io/collector/otelcol/otelcoltest"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/ptrace"
	colproc "go.opentelemetry.io/collector/processor"
	"go.opentelemetry.io/collector/processor/processortest"
	"go.opentelemetry.io/collector/receiver"
	"go.opentelemetry.io/collector/receiver/receivertest"
	"go.opentelemetry.io/otel/sdk/metric"
	"go.opentelemetry.io/otel/sdk/trace"

	"go.uber.org/zap"
)

func TestLoadConfig(t *testing.T) {
	factories, err := getFactories()
	assert.NoError(t, err)

	cfgValidate, errValidate := otelcoltest.LoadConfigAndValidate(filepath.Join("testdata", "config.yaml"), factories)
	require.NoError(t, errValidate)

	cfg, errLoad := otelcoltest.LoadConfig("testdata/config.yaml", factories)
	require.NoError(t, errLoad)

	assert.Equal(t, cfg, cfgValidate)
}

func TestProcessor(t *testing.T) {
	// we have 1 ResourceSpans with 1 ILS and two traceIDs, resulting in two batches
	inBatch := ptrace.NewTraces()

	rss := inBatch.ResourceSpans()
	rss.EnsureCapacity(1)

	rs := rss.AppendEmpty()

	ss := rs.ScopeSpans()
	ss.EnsureCapacity(1)

	ils := ss.AppendEmpty()

	// the first ILS has two spans
	library := ils.Scope()
	library.SetName("first-library")

	s := ils.Spans()
	s.EnsureCapacity(2)

	firstSpan := s.AppendEmpty()
	firstSpan.SetName("first-batch-first-span")
	firstSpan.SetTraceID(pcommon.TraceID([16]byte{1, 2, 3, 4}))
	firstSpan.Attributes().PutStr("blah", "test")

	secondSpan := s.AppendEmpty()
	secondSpan.SetName("first-batch-second-span")
	secondSpan.SetTraceID(pcommon.TraceID([16]byte{2, 3, 4, 5}))

	// test
	de := debugexporter.NewFactory()
	cfg := de.CreateDefaultConfig()

	logr, err := zap.NewProduction()
	assert.NoError(t, err)

	ct, err := component.NewType("debugtracer")
	assert.NoError(t, err)

	traceProvider := trace.NewTracerProvider()
	meterProvider := metric.NewMeterProvider()

	set := exporter.CreateSettings{
		ID: component.NewID(ct),
		TelemetrySettings: component.TelemetrySettings{
			Logger:         logr,
			TracerProvider: traceProvider,
			MeterProvider:  meterProvider,
			MetricsLevel:   0,
			Resource:       pcommon.Resource{},
			ReportStatus: func(event *component.StatusEvent) {
				fmt.Printf("event: %v\n", event)
			},
		},
		BuildInfo: component.BuildInfo{
			Command:     "",
			Description: "",
			Version:     "",
		},
	}

	next, err := de.CreateTracesExporter(context.Background(), set, cfg)
	assert.NoError(t, err)

	processor, err := processor.NewMetadataEnrichBatch(&config.Config{}, next)
	assert.NoError(t, err)

	err = processor.ConsumeTraces(context.Background(), inBatch)
	assert.NoError(t, err)

	// verify
	// assert.NoError(t, err)
	// assert.Len(t, next.Traces, 2)

	// first batch
	// firstOutILS := next.Traces[0].ResourceSpans().At(0).InstrumentationLibrarySpans().At(0)
	// assert.Equal(t, library.Name(), firstOutILS.InstrumentationLibrary().Name())
	// assert.Equal(t, firstSpan.Name(), firstOutILS.Spans().At(0).Name())

	// second batch
	// secondOutILS := next.Traces[1].ResourceSpans().At(0).InstrumentationLibrarySpans().At(0)
	// assert.Equal(t, library.Name(), secondOutILS.InstrumentationLibrary().Name())
	// assert.Equal(t, secondSpan.Name(), secondOutILS.Spans().At(0).Name())
}

func getFactories() (otelcol.Factories, error) {
	var factories otelcol.Factories
	var err error

	if factories.Extensions, err = extension.MakeFactoryMap(extensiontest.NewNopFactory()); err != nil {
		return otelcol.Factories{}, err
	}

	if factories.Receivers, err = receiver.MakeFactoryMap(receivertest.NewNopFactory()); err != nil {
		return otelcol.Factories{}, err
	}

	if factories.Exporters, err = exporter.MakeFactoryMap(
		exportertest.NewNopFactory(),
	); err != nil {
		return otelcol.Factories{}, err
	}

	if factories.Processors, err = colproc.MakeFactoryMap(
		processortest.NewNopFactory(),
		processor.NewFactory(),
	); err != nil {
		return otelcol.Factories{}, err
	}

	if factories.Connectors, err = connector.MakeFactoryMap(connectortest.NewNopFactory()); err != nil {
		return otelcol.Factories{}, err
	}

	return factories, nil
}
