// Package api provides primitives to interact with the openapi HTTP API.
//
// Code generated by github.com/deepmap/oapi-codegen/v2 version v2.1.0 DO NOT EDIT.
package api

import (
	"bytes"
	"compress/gzip"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"
	"strings"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/go-chi/chi/v5"
	"github.com/oapi-codegen/runtime"
	strictnethttp "github.com/oapi-codegen/runtime/strictmiddleware/nethttp"
)

// Defines values for LegalBasis.
const (
	CONSENT               LegalBasis = "CONSENT"
	LEGALOBLIGATION       LegalBasis = "LEGAL_OBLIGATION"
	LEGITIMATEINTERESTS   LegalBasis = "LEGITIMATE_INTERESTS"
	PERFORMANCEOFCONTRACT LegalBasis = "PERFORMANCE_OF_CONTRACT"
	PUBLICINTEREST        LegalBasis = "PUBLIC_INTEREST"
	VITALINTERESTS        LegalBasis = "VITAL_INTERESTS"
)

// LegalBasis As defined in the GDPR, article 6, section 1
type LegalBasis string

// ProcessingActivityItem defines model for ProcessingActivityItem.
type ProcessingActivityItem struct {
	Controller                 *string   `json:"controller,omitempty"`
	DataSubjectCategories      *[]string `json:"data_subject_categories,omitempty"`
	EnvisagedTimeLimit         *string   `json:"envisaged_time_limit,omitempty"`
	EnvisagedTimeLimitDuration *string   `json:"envisaged_time_limit_duration,omitempty"`
	Id                         *string   `json:"id,omitempty"`

	// LegalBasis As defined in the GDPR, article 6, section 1
	LegalBasis             *LegalBasis `json:"legal_basis,omitempty"`
	LegalBasisComment      *string     `json:"legal_basis_comment,omitempty"`
	Name                   *string     `json:"name,omitempty"`
	PersonalDataCategories *[]string   `json:"personal_data_categories,omitempty"`
	Purpose                *string     `json:"purpose,omitempty"`
	RecipientsCategories   *[]string   `json:"recipients_categories,omitempty"`
	Uri                    *string     `json:"uri,omitempty"`
}

// RequestEditorFn  is the function signature for the RequestEditor callback function
type RequestEditorFn func(ctx context.Context, req *http.Request) error

// Doer performs HTTP requests.
//
// The standard http.Client implements this interface.
type HttpRequestDoer interface {
	Do(req *http.Request) (*http.Response, error)
}

// Client which conforms to the OpenAPI3 specification for this service.
type Client struct {
	// The endpoint of the server conforming to this interface, with scheme,
	// https://api.deepmap.com for example. This can contain a path relative
	// to the server, such as https://api.deepmap.com/dev-test, and all the
	// paths in the swagger spec will be appended to the server.
	Server string

	// Doer for performing requests, typically a *http.Client with any
	// customized settings, such as certificate chains.
	Client HttpRequestDoer

	// A list of callbacks for modifying requests which are generated before sending over
	// the network.
	RequestEditors []RequestEditorFn
}

// ClientOption allows setting custom parameters during construction
type ClientOption func(*Client) error

// Creates a new Client, with reasonable defaults
func NewClient(server string, opts ...ClientOption) (*Client, error) {
	// create a client with sane default values
	client := Client{
		Server: server,
	}
	// mutate client and add all optional params
	for _, o := range opts {
		if err := o(&client); err != nil {
			return nil, err
		}
	}
	// ensure the server URL always has a trailing slash
	if !strings.HasSuffix(client.Server, "/") {
		client.Server += "/"
	}
	// create httpClient, if not already present
	if client.Client == nil {
		client.Client = &http.Client{}
	}
	return &client, nil
}

// WithHTTPClient allows overriding the default Doer, which is
// automatically created using http.Client. This is useful for tests.
func WithHTTPClient(doer HttpRequestDoer) ClientOption {
	return func(c *Client) error {
		c.Client = doer
		return nil
	}
}

// WithRequestEditorFn allows setting up a callback function, which will be
// called right before sending the request. This can be used to mutate the request.
func WithRequestEditorFn(fn RequestEditorFn) ClientOption {
	return func(c *Client) error {
		c.RequestEditors = append(c.RequestEditors, fn)
		return nil
	}
}

// The interface specification for the client above.
type ClientInterface interface {
	// ListProcessingActivities request
	ListProcessingActivities(ctx context.Context, reqEditors ...RequestEditorFn) (*http.Response, error)

	// GetProcessingActivity request
	GetProcessingActivity(ctx context.Context, id string, reqEditors ...RequestEditorFn) (*http.Response, error)
}

func (c *Client) ListProcessingActivities(ctx context.Context, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewListProcessingActivitiesRequest(c.Server)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

func (c *Client) GetProcessingActivity(ctx context.Context, id string, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewGetProcessingActivityRequest(c.Server, id)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

// NewListProcessingActivitiesRequest generates requests for ListProcessingActivities
func NewListProcessingActivitiesRequest(server string) (*http.Request, error) {
	var err error

	serverURL, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	operationPath := fmt.Sprintf("/processing_activities")
	if operationPath[0] == '/' {
		operationPath = "." + operationPath
	}

	queryURL, err := serverURL.Parse(operationPath)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("GET", queryURL.String(), nil)
	if err != nil {
		return nil, err
	}

	return req, nil
}

// NewGetProcessingActivityRequest generates requests for GetProcessingActivity
func NewGetProcessingActivityRequest(server string, id string) (*http.Request, error) {
	var err error

	var pathParam0 string

	pathParam0, err = runtime.StyleParamWithLocation("simple", false, "id", runtime.ParamLocationPath, id)
	if err != nil {
		return nil, err
	}

	serverURL, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	operationPath := fmt.Sprintf("/processing_activities/%s", pathParam0)
	if operationPath[0] == '/' {
		operationPath = "." + operationPath
	}

	queryURL, err := serverURL.Parse(operationPath)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("GET", queryURL.String(), nil)
	if err != nil {
		return nil, err
	}

	return req, nil
}

func (c *Client) applyEditors(ctx context.Context, req *http.Request, additionalEditors []RequestEditorFn) error {
	for _, r := range c.RequestEditors {
		if err := r(ctx, req); err != nil {
			return err
		}
	}
	for _, r := range additionalEditors {
		if err := r(ctx, req); err != nil {
			return err
		}
	}
	return nil
}

// ClientWithResponses builds on ClientInterface to offer response payloads
type ClientWithResponses struct {
	ClientInterface
}

// NewClientWithResponses creates a new ClientWithResponses, which wraps
// Client with return type handling
func NewClientWithResponses(server string, opts ...ClientOption) (*ClientWithResponses, error) {
	client, err := NewClient(server, opts...)
	if err != nil {
		return nil, err
	}
	return &ClientWithResponses{client}, nil
}

// WithBaseURL overrides the baseURL.
func WithBaseURL(baseURL string) ClientOption {
	return func(c *Client) error {
		newBaseURL, err := url.Parse(baseURL)
		if err != nil {
			return err
		}
		c.Server = newBaseURL.String()
		return nil
	}
}

// ClientWithResponsesInterface is the interface specification for the client with responses above.
type ClientWithResponsesInterface interface {
	// ListProcessingActivitiesWithResponse request
	ListProcessingActivitiesWithResponse(ctx context.Context, reqEditors ...RequestEditorFn) (*ListProcessingActivitiesResponse, error)

	// GetProcessingActivityWithResponse request
	GetProcessingActivityWithResponse(ctx context.Context, id string, reqEditors ...RequestEditorFn) (*GetProcessingActivityResponse, error)
}

type ListProcessingActivitiesResponse struct {
	Body         []byte
	HTTPResponse *http.Response
	JSON200      *[]ProcessingActivityItem
}

// Status returns HTTPResponse.Status
func (r ListProcessingActivitiesResponse) Status() string {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.Status
	}
	return http.StatusText(0)
}

// StatusCode returns HTTPResponse.StatusCode
func (r ListProcessingActivitiesResponse) StatusCode() int {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.StatusCode
	}
	return 0
}

type GetProcessingActivityResponse struct {
	Body         []byte
	HTTPResponse *http.Response
	JSON200      *ProcessingActivityItem
}

// Status returns HTTPResponse.Status
func (r GetProcessingActivityResponse) Status() string {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.Status
	}
	return http.StatusText(0)
}

// StatusCode returns HTTPResponse.StatusCode
func (r GetProcessingActivityResponse) StatusCode() int {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.StatusCode
	}
	return 0
}

// ListProcessingActivitiesWithResponse request returning *ListProcessingActivitiesResponse
func (c *ClientWithResponses) ListProcessingActivitiesWithResponse(ctx context.Context, reqEditors ...RequestEditorFn) (*ListProcessingActivitiesResponse, error) {
	rsp, err := c.ListProcessingActivities(ctx, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseListProcessingActivitiesResponse(rsp)
}

// GetProcessingActivityWithResponse request returning *GetProcessingActivityResponse
func (c *ClientWithResponses) GetProcessingActivityWithResponse(ctx context.Context, id string, reqEditors ...RequestEditorFn) (*GetProcessingActivityResponse, error) {
	rsp, err := c.GetProcessingActivity(ctx, id, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseGetProcessingActivityResponse(rsp)
}

// ParseListProcessingActivitiesResponse parses an HTTP response from a ListProcessingActivitiesWithResponse call
func ParseListProcessingActivitiesResponse(rsp *http.Response) (*ListProcessingActivitiesResponse, error) {
	bodyBytes, err := io.ReadAll(rsp.Body)
	defer func() { _ = rsp.Body.Close() }()
	if err != nil {
		return nil, err
	}

	response := &ListProcessingActivitiesResponse{
		Body:         bodyBytes,
		HTTPResponse: rsp,
	}

	switch {
	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 200:
		var dest []ProcessingActivityItem
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON200 = &dest

	}

	return response, nil
}

// ParseGetProcessingActivityResponse parses an HTTP response from a GetProcessingActivityWithResponse call
func ParseGetProcessingActivityResponse(rsp *http.Response) (*GetProcessingActivityResponse, error) {
	bodyBytes, err := io.ReadAll(rsp.Body)
	defer func() { _ = rsp.Body.Close() }()
	if err != nil {
		return nil, err
	}

	response := &GetProcessingActivityResponse{
		Body:         bodyBytes,
		HTTPResponse: rsp,
	}

	switch {
	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 200:
		var dest ProcessingActivityItem
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON200 = &dest

	}

	return response, nil
}

// ServerInterface represents all server handlers.
type ServerInterface interface {
	// Get all processing activities
	// (GET /processing_activities)
	ListProcessingActivities(w http.ResponseWriter, r *http.Request)
	// Get a processing activity
	// (GET /processing_activities/{id})
	GetProcessingActivity(w http.ResponseWriter, r *http.Request, id string)
}

// Unimplemented server implementation that returns http.StatusNotImplemented for each endpoint.

type Unimplemented struct{}

// Get all processing activities
// (GET /processing_activities)
func (_ Unimplemented) ListProcessingActivities(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotImplemented)
}

// Get a processing activity
// (GET /processing_activities/{id})
func (_ Unimplemented) GetProcessingActivity(w http.ResponseWriter, r *http.Request, id string) {
	w.WriteHeader(http.StatusNotImplemented)
}

// ServerInterfaceWrapper converts contexts to parameters.
type ServerInterfaceWrapper struct {
	Handler            ServerInterface
	HandlerMiddlewares []MiddlewareFunc
	ErrorHandlerFunc   func(w http.ResponseWriter, r *http.Request, err error)
}

type MiddlewareFunc func(http.Handler) http.Handler

// ListProcessingActivities operation middleware
func (siw *ServerInterfaceWrapper) ListProcessingActivities(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	handler := http.Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		siw.Handler.ListProcessingActivities(w, r)
	}))

	for _, middleware := range siw.HandlerMiddlewares {
		handler = middleware(handler)
	}

	handler.ServeHTTP(w, r.WithContext(ctx))
}

// GetProcessingActivity operation middleware
func (siw *ServerInterfaceWrapper) GetProcessingActivity(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var err error

	// ------------- Path parameter "id" -------------
	var id string

	err = runtime.BindStyledParameterWithOptions("simple", "id", chi.URLParam(r, "id"), &id, runtime.BindStyledParameterOptions{ParamLocation: runtime.ParamLocationPath, Explode: false, Required: true})
	if err != nil {
		siw.ErrorHandlerFunc(w, r, &InvalidParamFormatError{ParamName: "id", Err: err})
		return
	}

	handler := http.Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		siw.Handler.GetProcessingActivity(w, r, id)
	}))

	for _, middleware := range siw.HandlerMiddlewares {
		handler = middleware(handler)
	}

	handler.ServeHTTP(w, r.WithContext(ctx))
}

type UnescapedCookieParamError struct {
	ParamName string
	Err       error
}

func (e *UnescapedCookieParamError) Error() string {
	return fmt.Sprintf("error unescaping cookie parameter '%s'", e.ParamName)
}

func (e *UnescapedCookieParamError) Unwrap() error {
	return e.Err
}

type UnmarshalingParamError struct {
	ParamName string
	Err       error
}

func (e *UnmarshalingParamError) Error() string {
	return fmt.Sprintf("Error unmarshaling parameter %s as JSON: %s", e.ParamName, e.Err.Error())
}

func (e *UnmarshalingParamError) Unwrap() error {
	return e.Err
}

type RequiredParamError struct {
	ParamName string
}

func (e *RequiredParamError) Error() string {
	return fmt.Sprintf("Query argument %s is required, but not found", e.ParamName)
}

type RequiredHeaderError struct {
	ParamName string
	Err       error
}

func (e *RequiredHeaderError) Error() string {
	return fmt.Sprintf("Header parameter %s is required, but not found", e.ParamName)
}

func (e *RequiredHeaderError) Unwrap() error {
	return e.Err
}

type InvalidParamFormatError struct {
	ParamName string
	Err       error
}

func (e *InvalidParamFormatError) Error() string {
	return fmt.Sprintf("Invalid format for parameter %s: %s", e.ParamName, e.Err.Error())
}

func (e *InvalidParamFormatError) Unwrap() error {
	return e.Err
}

type TooManyValuesForParamError struct {
	ParamName string
	Count     int
}

func (e *TooManyValuesForParamError) Error() string {
	return fmt.Sprintf("Expected one value for %s, got %d", e.ParamName, e.Count)
}

// Handler creates http.Handler with routing matching OpenAPI spec.
func Handler(si ServerInterface) http.Handler {
	return HandlerWithOptions(si, ChiServerOptions{})
}

type ChiServerOptions struct {
	BaseURL          string
	BaseRouter       chi.Router
	Middlewares      []MiddlewareFunc
	ErrorHandlerFunc func(w http.ResponseWriter, r *http.Request, err error)
}

// HandlerFromMux creates http.Handler with routing matching OpenAPI spec based on the provided mux.
func HandlerFromMux(si ServerInterface, r chi.Router) http.Handler {
	return HandlerWithOptions(si, ChiServerOptions{
		BaseRouter: r,
	})
}

func HandlerFromMuxWithBaseURL(si ServerInterface, r chi.Router, baseURL string) http.Handler {
	return HandlerWithOptions(si, ChiServerOptions{
		BaseURL:    baseURL,
		BaseRouter: r,
	})
}

// HandlerWithOptions creates http.Handler with additional options
func HandlerWithOptions(si ServerInterface, options ChiServerOptions) http.Handler {
	r := options.BaseRouter

	if r == nil {
		r = chi.NewRouter()
	}
	if options.ErrorHandlerFunc == nil {
		options.ErrorHandlerFunc = func(w http.ResponseWriter, r *http.Request, err error) {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
	}
	wrapper := ServerInterfaceWrapper{
		Handler:            si,
		HandlerMiddlewares: options.Middlewares,
		ErrorHandlerFunc:   options.ErrorHandlerFunc,
	}

	r.Group(func(r chi.Router) {
		r.Get(options.BaseURL+"/processing_activities", wrapper.ListProcessingActivities)
	})
	r.Group(func(r chi.Router) {
		r.Get(options.BaseURL+"/processing_activities/{id}", wrapper.GetProcessingActivity)
	})

	return r
}

type ListProcessingActivitiesRequestObject struct {
}

type ListProcessingActivitiesResponseObject interface {
	VisitListProcessingActivitiesResponse(w http.ResponseWriter) error
}

type ListProcessingActivities200ResponseHeaders struct {
	APIVersion string
	Link       string
}

type ListProcessingActivities200JSONResponse struct {
	Body    []ProcessingActivityItem
	Headers ListProcessingActivities200ResponseHeaders
}

func (response ListProcessingActivities200JSONResponse) VisitListProcessingActivitiesResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("API-Version", fmt.Sprint(response.Headers.APIVersion))
	w.Header().Set("Link", fmt.Sprint(response.Headers.Link))
	w.WriteHeader(200)

	return json.NewEncoder(w).Encode(response.Body)
}

type GetProcessingActivityRequestObject struct {
	Id string `json:"id"`
}

type GetProcessingActivityResponseObject interface {
	VisitGetProcessingActivityResponse(w http.ResponseWriter) error
}

type GetProcessingActivity200ResponseHeaders struct {
	APIVersion string
}

type GetProcessingActivity200JSONResponse struct {
	Body    ProcessingActivityItem
	Headers GetProcessingActivity200ResponseHeaders
}

func (response GetProcessingActivity200JSONResponse) VisitGetProcessingActivityResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("API-Version", fmt.Sprint(response.Headers.APIVersion))
	w.WriteHeader(200)

	return json.NewEncoder(w).Encode(response.Body)
}

type GetProcessingActivity404ResponseHeaders struct {
	APIVersion string
}

type GetProcessingActivity404Response struct {
	Headers GetProcessingActivity404ResponseHeaders
}

func (response GetProcessingActivity404Response) VisitGetProcessingActivityResponse(w http.ResponseWriter) error {
	w.Header().Set("API-Version", fmt.Sprint(response.Headers.APIVersion))
	w.WriteHeader(404)
	return nil
}

// StrictServerInterface represents all server handlers.
type StrictServerInterface interface {
	// Get all processing activities
	// (GET /processing_activities)
	ListProcessingActivities(ctx context.Context, request ListProcessingActivitiesRequestObject) (ListProcessingActivitiesResponseObject, error)
	// Get a processing activity
	// (GET /processing_activities/{id})
	GetProcessingActivity(ctx context.Context, request GetProcessingActivityRequestObject) (GetProcessingActivityResponseObject, error)
}

type StrictHandlerFunc = strictnethttp.StrictHTTPHandlerFunc
type StrictMiddlewareFunc = strictnethttp.StrictHTTPMiddlewareFunc

type StrictHTTPServerOptions struct {
	RequestErrorHandlerFunc  func(w http.ResponseWriter, r *http.Request, err error)
	ResponseErrorHandlerFunc func(w http.ResponseWriter, r *http.Request, err error)
}

func NewStrictHandler(ssi StrictServerInterface, middlewares []StrictMiddlewareFunc) ServerInterface {
	return &strictHandler{ssi: ssi, middlewares: middlewares, options: StrictHTTPServerOptions{
		RequestErrorHandlerFunc: func(w http.ResponseWriter, r *http.Request, err error) {
			http.Error(w, err.Error(), http.StatusBadRequest)
		},
		ResponseErrorHandlerFunc: func(w http.ResponseWriter, r *http.Request, err error) {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		},
	}}
}

func NewStrictHandlerWithOptions(ssi StrictServerInterface, middlewares []StrictMiddlewareFunc, options StrictHTTPServerOptions) ServerInterface {
	return &strictHandler{ssi: ssi, middlewares: middlewares, options: options}
}

type strictHandler struct {
	ssi         StrictServerInterface
	middlewares []StrictMiddlewareFunc
	options     StrictHTTPServerOptions
}

// ListProcessingActivities operation middleware
func (sh *strictHandler) ListProcessingActivities(w http.ResponseWriter, r *http.Request) {
	var request ListProcessingActivitiesRequestObject

	handler := func(ctx context.Context, w http.ResponseWriter, r *http.Request, request interface{}) (interface{}, error) {
		return sh.ssi.ListProcessingActivities(ctx, request.(ListProcessingActivitiesRequestObject))
	}
	for _, middleware := range sh.middlewares {
		handler = middleware(handler, "ListProcessingActivities")
	}

	response, err := handler(r.Context(), w, r, request)

	if err != nil {
		sh.options.ResponseErrorHandlerFunc(w, r, err)
	} else if validResponse, ok := response.(ListProcessingActivitiesResponseObject); ok {
		if err := validResponse.VisitListProcessingActivitiesResponse(w); err != nil {
			sh.options.ResponseErrorHandlerFunc(w, r, err)
		}
	} else if response != nil {
		sh.options.ResponseErrorHandlerFunc(w, r, fmt.Errorf("unexpected response type: %T", response))
	}
}

// GetProcessingActivity operation middleware
func (sh *strictHandler) GetProcessingActivity(w http.ResponseWriter, r *http.Request, id string) {
	var request GetProcessingActivityRequestObject

	request.Id = id

	handler := func(ctx context.Context, w http.ResponseWriter, r *http.Request, request interface{}) (interface{}, error) {
		return sh.ssi.GetProcessingActivity(ctx, request.(GetProcessingActivityRequestObject))
	}
	for _, middleware := range sh.middlewares {
		handler = middleware(handler, "GetProcessingActivity")
	}

	response, err := handler(r.Context(), w, r, request)

	if err != nil {
		sh.options.ResponseErrorHandlerFunc(w, r, err)
	} else if validResponse, ok := response.(GetProcessingActivityResponseObject); ok {
		if err := validResponse.VisitGetProcessingActivityResponse(w); err != nil {
			sh.options.ResponseErrorHandlerFunc(w, r, err)
		}
	} else if response != nil {
		sh.options.ResponseErrorHandlerFunc(w, r, fmt.Errorf("unexpected response type: %T", response))
	}
}

// Base64 encoded, gzipped, json marshaled Swagger object
var swaggerSpec = []string{

	"H4sIAAAAAAAC/6xWUW/bOAz+KwJ3j17srrs7wIfhkKZZZyBLgtTtPayFodqMw9WWdJKcNCjy3w+ynTad",
	"nRXo7S2S+VEkv49kHiGVpZIChTUQPsIKeYa6/jmcR++vURuSwh0zNKkmZesjtB+YXDK7IsOG8wg8MOkK",
	"S+6s8YGXqkAI4WQQDALwAB8sasGLc5marr/hPGLnaCgXbFEV6Py9//1P8KDSBYSwslaZ0PdVdVdQyi3h",
	"IEVhdVWupdTGcpFxrjMUA1H4XJHPM+2fDAL/HVfkHO08sFvl4jFWk8hht/NgQuK+G8qXOJ4z94l9qYtx",
	"JK+bKghO031g7f0glaX/t+I5fvpQG+BfTGPx6QaUxvUNeOw12MeXMIEP9gZeLd8/pyOmcYkaRYqdqm02",
	"m8HmdCB17m/onnyXXJtbT112+3zrdyaY8+KMG+ojzbAMlyQwYySYXSG7OJ8vPMa1pbRA9ofHDKbOmJ24",
	"FERVQvgNRrPp5Xgagwfz8eLzbPF1OB2Nk9nnZDSbxovhyH2ZjC+Gk2R2NokuhnE0m4IH11E8nCTRNB4v",
	"xpfxpYNfnU2i0dNVA4vi6OswHh8Y3nZy9GCuZYrGkMiHqaU12W1ksXQpKi0VaktYJ5xKYbUsCtTu1HGT",
	"ccsTU919x9QmKbeYS90iyWJpekHtBdeab90ZxZoMzzFLLJWYFFSS7QX2GSZZpblte7SDoKz3unCkJnd7",
	"Vn/TuIQQ3vnPs8BvJeAf8P8SmKSyLFH0Ryp4ib0fFGojBS+SunJvrZiqtJKm/wWNKSlyObzZfaXJ2S2l",
	"LrmFsD73zY/2Rtb0N51DYikdtqAURRMhZSgsLclJCMZX88n7k8EH2NcIxpUTHBfsSrhGmdcTjk0aPHOm",
	"7iWy9dBZYE7GomZrLliG7Br1BvU9idzwRsdIFgV4sN5PbggGJ4PAOZEKBVcEIZzWVx4obld1SXz11A9J",
	"66itWo41w64naplFGYQwIWM7HeQArvxGSWEa8Icg2DdRqxSuVDPApfC/m0a1z9P1iaKfKfJI63Z4dHS8",
	"nFcubLexnpNl/DD248uvL57W2j80PVgqP4PUNrtm1FZlyfUWQrhAy3hRHI3O8ty48dmlagu3zlU/if4j",
	"ZbujTF5gl8htrQzNS7R1Nb79OPfjFbLovNn92BOvc0DO0OnrWemU1fr4tyKNGYRWV3i4Wn/sr9v/Kaa3",
	"aKirmUsSeXEsyV8gGPfix+Bjd7nOuy+yTKJhQlqGD2Tsrwqgq8Ij6b6iQOcH9XqvmeY/iL8OYHe7+y8A",
	"AP//LiEiM2gKAAA=",
}

// GetSwagger returns the content of the embedded swagger specification file
// or error if failed to decode
func decodeSpec() ([]byte, error) {
	zipped, err := base64.StdEncoding.DecodeString(strings.Join(swaggerSpec, ""))
	if err != nil {
		return nil, fmt.Errorf("error base64 decoding spec: %w", err)
	}
	zr, err := gzip.NewReader(bytes.NewReader(zipped))
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %w", err)
	}
	var buf bytes.Buffer
	_, err = buf.ReadFrom(zr)
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %w", err)
	}

	return buf.Bytes(), nil
}

var rawSpec = decodeSpecCached()

// a naive cached of a decoded swagger spec
func decodeSpecCached() func() ([]byte, error) {
	data, err := decodeSpec()
	return func() ([]byte, error) {
		return data, err
	}
}

// Constructs a synthetic filesystem for resolving external references when loading openapi specifications.
func PathToRawSpec(pathToFile string) map[string]func() ([]byte, error) {
	res := make(map[string]func() ([]byte, error))
	if len(pathToFile) > 0 {
		res[pathToFile] = rawSpec
	}

	return res
}

// GetSwagger returns the Swagger specification corresponding to the generated code
// in this file. The external references of Swagger specification are resolved.
// The logic of resolving external references is tightly connected to "import-mapping" feature.
// Externally referenced files must be embedded in the corresponding golang packages.
// Urls can be supported but this task was out of the scope.
func GetSwagger() (swagger *openapi3.T, err error) {
	resolvePath := PathToRawSpec("")

	loader := openapi3.NewLoader()
	loader.IsExternalRefsAllowed = true
	loader.ReadFromURIFunc = func(loader *openapi3.Loader, url *url.URL) ([]byte, error) {
		pathToFile := url.String()
		pathToFile = path.Clean(pathToFile)
		getSpec, ok := resolvePath[pathToFile]
		if !ok {
			err1 := fmt.Errorf("path not found: %s", pathToFile)
			return nil, err1
		}
		return getSpec()
	}
	var specData []byte
	specData, err = rawSpec()
	if err != nil {
		return
	}
	swagger, err = loader.LoadFromData(specData)
	if err != nil {
		return
	}
	return
}
