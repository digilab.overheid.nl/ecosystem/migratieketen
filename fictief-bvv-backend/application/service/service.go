package service

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"time"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/pkg/storage"
	tracer "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/trace"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

var ErrNotFound = errors.New("not found")

type Service struct {
	tracer                trace.Tracer
	logger                *slog.Logger
	storage               *storage.Database
	shutdownFns           []func(context.Context) error
	processingActivityIDs map[string]uuid.UUID
}

func New(logger *slog.Logger, storage *storage.Database) (*Service, error) {
	otelShutdown, err := setupOTelSDK(context.TODO())
	if err != nil {
		return nil, fmt.Errorf("setup otel sdk: %w", err)
	}

	service := &Service{
		tracer:      otel.Tracer("bvv"),
		logger:      logger,
		storage:     storage,
		shutdownFns: []func(context.Context) error{otelShutdown},
		processingActivityIDs: map[string]uuid.UUID{
			"createSamenvoeging":    uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"getSamenvoeging":       uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"createVreemdeling":     uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"deleteVreemdelingByID": uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"readVreemdelingByID":   uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"updateVreemdelingByID": uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"findVreemdeling":       uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
		},
	}

	return service, nil
}

func (service *Service) tracerStart(ctx context.Context, name string, opts ...trace.SpanStartOption) (context.Context, trace.Span) {
	var parent tracer.ProcessingContext
	if p := tracer.ProcessingOperationFromContext(ctx); p.IsValid() {
		parent = *p
	}

	if opts == nil {
		opts = make([]trace.SpanStartOption, 0, 2)
	}

	opts = append(opts,
		trace.WithTimestamp(time.Now()),
		trace.WithAttributes(
			attribute.String("dpl.rva.activity.id", service.processingActivityIDs[name].String()),
		))

	if parent.IsValid() && parent.Foreign() {
		opts = append(opts, trace.WithAttributes(
			attribute.String("dpl.rva.foreign.trace_id", parent.TraceID().String()),
			attribute.String("dpl.rva.foreign.operation_id", parent.OperationID().String()),
		))
	}

	ctx, span := service.tracer.Start(ctx, name, opts...)

	op := tracer.New(tracer.TraceID(span.SpanContext().TraceID()), tracer.SpanID(span.SpanContext().SpanID()))
	ctx = tracer.ContextWithProcessingOperation(ctx, op)

	return ctx, span
}
