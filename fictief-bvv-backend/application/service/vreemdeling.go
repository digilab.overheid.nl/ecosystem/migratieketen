package service

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/google/uuid"
	openapi_types "github.com/oapi-codegen/runtime/types"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/pkg/storage/queries/generated"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"golang.org/x/exp/maps"
)

func setSubjectID(vreemdelingID string) attribute.KeyValue {
	return attribute.String("dpl.core.user", fmt.Sprintf("vid:%s", vreemdelingID))
}

func (service *Service) CreateSamenvoeging(ctx context.Context, leadingID, expiredID string) error {
	ctx, span := service.tracerStart(ctx, "createSamenvoeging",
		trace.WithAttributes(
			setSubjectID(leadingID),
		),
	)

	defer span.End()

	if err := service.createSamenvoeging(ctx, leadingID, expiredID); err != nil {
		span.SetStatus(codes.Error, err.Error())
		return err
	}

	span.SetStatus(codes.Ok, "")

	return nil
}

func (service *Service) GetSamenvoegingen(ctx context.Context, vreemdelingID api.VreemdelingId) ([]api.VreemdelingId, error) {
	ctx, span := service.tracerStart(ctx, "getSamenvoeging",
		trace.WithAttributes(
			setSubjectID(vreemdelingID),
		),
	)

	defer span.End()

	vreemdelingenIDs, err := service.getSamenvoegingen(ctx, vreemdelingID)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	span.SetStatus(codes.Ok, "")

	return vreemdelingenIDs, nil
}

func (service *Service) CreateVreemdeling(ctx context.Context, vreemdeling api.VreemdelingWithoutId) (*api.Vreemdeling, error) {
	ctx, span := service.tracerStart(ctx, "createVreemdeling")
	defer span.End()

	vreemdelingID, err := service.createVreemdeling(ctx, vreemdeling)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	span.SetAttributes(setSubjectID(vreemdelingID))

	v, err := service.readVreemdelingByID(ctx, vreemdelingID)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	span.SetStatus(codes.Ok, "")

	return v, nil
}

func (service *Service) DeleteVreemdelingByID(ctx context.Context, vreemdelingID api.VreemdelingId) error {
	ctx, span := service.tracerStart(ctx, "deleteVreemdelingByID",
		trace.WithAttributes(
			setSubjectID(vreemdelingID),
		),
	)

	defer span.End()

	if err := service.deleteVreemdelingByID(ctx, vreemdelingID); err != nil {
		span.SetStatus(codes.Error, err.Error())
		return err
	}

	span.SetStatus(codes.Ok, "")

	return nil
}

func (service *Service) ReadVreemdelingByID(ctx context.Context, vreemdelingID api.VreemdelingId) (*api.Vreemdeling, error) {
	ctx, span := service.tracerStart(ctx, "readVreemdelingByID",
		trace.WithAttributes(
			setSubjectID(vreemdelingID),
		),
	)

	defer span.End()

	vreemdeling, err := service.readVreemdelingByID(ctx, vreemdelingID)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	span.SetStatus(codes.Ok, "")

	return vreemdeling, nil
}

func (service *Service) UpdateVreemdelingByID(ctx context.Context, vreemdelingID api.VreemdelingId, vreemdeling api.Vreemdeling) (*api.Vreemdeling, error) {
	ctx, span := service.tracerStart(ctx, "updateVreemdelingByID",
		trace.WithAttributes(
			setSubjectID(vreemdelingID),
		),
	)
	defer span.End()

	if err := service.updateVreemdelingByID(ctx, vreemdelingID, vreemdeling); err != nil {
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	v, err := service.ReadVreemdelingByID(ctx, vreemdelingID)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	span.SetStatus(codes.Ok, "")

	return v, nil
}

func (service *Service) FindVreemdeling(ctx context.Context, params api.FindVreemdelingParams) ([]api.Vreemdeling, error) {
	ctx, span := service.tracerStart(ctx, "findVreemdeling")
	defer span.End()

	vreemdelingen, err := service.findVreemdeling(ctx, params)
	if err != nil {
		return nil, err
	}

	span.SetStatus(codes.Ok, "")

	return vreemdelingen, nil
}

func (service *Service) findVreemdeling(ctx context.Context, params api.FindVreemdelingParams) ([]api.Vreemdeling, error) {
	argName := sql.NullString{
		Valid: false,
	}

	if params.Naam != nil {
		name := regexp.MustCompile("[_%]").ReplaceAllStringFunc(*params.Naam, func(match string) string {
			return `\` + match
		})

		argName = sql.NullString{
			String: name,
			Valid:  *params.Naam != "",
		}
	}

	argBirthdate := sql.NullTime{
		Valid: false,
	}

	if params.Geboortedatum != nil {
		argBirthdate = sql.NullTime{
			Time:  params.Geboortedatum.Time,
			Valid: true,
		}
	}

	args := queries.VreemdelingSearchByNameAndBirthdateParams{
		Name:      argName,
		Birthdate: argBirthdate,
	}

	records, err := service.storage.Queries.VreemdelingSearchByNameAndBirthdate(ctx, &args)
	if err != nil {
		return nil, fmt.Errorf("vreemdeling search by name and birthdate: %w", err)
	}

	vreemdelingen := make([]api.Vreemdeling, 0, len(records))
	for _, record := range records {
		vreemdeling := ToVreemdeling(record)
		vreemdelingen = append(vreemdelingen, *vreemdeling)
	}

	return vreemdelingen, nil
}

func (service *Service) createVreemdeling(ctx context.Context, vreemdeling api.VreemdelingWithoutId) (string, error) {
	searchname := ToSearchName(vreemdeling.Naam)

	count, err := service.storage.Queries.VreemdelingCount(ctx)
	if err != nil {
		return "", fmt.Errorf("vreemdeling count: %w", err)
	}

	vreemdelingNummer := fmt.Sprintf("%010d", count+1)

	params := &queries.VreemdelingCreateParams{
		ID:                uuid.New(),
		Vreemdelingnummer: vreemdelingNummer,
		Name:              sql.NullString{String: vreemdeling.Naam, Valid: true},
		Searchname:        sql.NullString{String: searchname, Valid: true},
		Birthdate:         sql.NullTime{Time: vreemdeling.Geboortedatum.Time, Valid: true},
	}

	if err := service.storage.Queries.VreemdelingCreate(ctx, params); err != nil {
		return "", err
	}

	return vreemdelingNummer, nil
}

func (service *Service) createSamenvoeging(ctx context.Context, leadingID, expiredID string) error {
	params := &queries.SamenvoegingInsertParams{
		ID:                     uuid.New(),
		VreemdelingLeidendID:   leadingID,
		VreemdelingVervallenID: expiredID,
		CreatedAt:              time.Now(),
	}

	if err := service.storage.Queries.SamenvoegingInsert(ctx, params); err != nil {
		return fmt.Errorf("samenvoegen failed: %w", err)
	}

	return nil
}

func (service *Service) getSamenvoegingen(ctx context.Context, vreemdelingID api.VreemdelingId) ([]api.VreemdelingId, error) {
	records, err := service.storage.Queries.SamenvoegingGet(ctx, vreemdelingID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNotFound
		}

		return nil, fmt.Errorf("samenvoeging get: %w", err)
	}

	vreemdelingIDs := make(map[api.VreemdelingId]struct{})
	for _, record := range records {
		vreemdelingIDs[record.VreemdelingLeidendID] = struct{}{}
		vreemdelingIDs[record.VreemdelingVervallenID] = struct{}{}
	}

	return maps.Keys(vreemdelingIDs), nil
}

func (service *Service) deleteVreemdelingByID(ctx context.Context, vreemdelingNummer api.VreemdelingId) error {
	if err := service.storage.Queries.VreemdelingDeleteByNummer(ctx, vreemdelingNummer); err != nil {
		return fmt.Errorf("vreemdeling delete by nummer: %w", err)
	}

	return nil
}

func (service *Service) readVreemdelingByID(ctx context.Context, vreemdelingNummer api.VreemdelingId) (*api.Vreemdeling, error) {
	vreemdelingResult, err := service.storage.Queries.VreemdelingGetByNummer(ctx, vreemdelingNummer)
	if err != nil {
		return nil, err
	}

	return ToVreemdeling(vreemdelingResult), nil
}

func (service *Service) updateVreemdelingByID(ctx context.Context, vreemdelingID api.VreemdelingId, vreemdeling api.Vreemdeling) error {
	var newVreemdelingID string
	if vreemdeling.Id != "" {
		newVreemdelingID = vreemdeling.Id
	} else {
		newVreemdelingID = vreemdelingID
	}

	params := queries.VreemdelingUpdateByNummerParams{
		Vreemdelingnummer:   vreemdelingID,
		Vreemdelingnummer_2: newVreemdelingID,
	}

	if vreemdeling.Naam != "" {
		params.Name = sql.NullString{String: vreemdeling.Naam, Valid: true}
		params.Searchname = sql.NullString{String: ToSearchName(vreemdeling.Naam), Valid: true}
	}

	if vreemdeling.Geboortedatum != nil {
		params.Birthdate = sql.NullTime{Time: vreemdeling.Geboortedatum.Time, Valid: true}
	}

	if err := service.storage.Queries.VreemdelingUpdateByNummer(ctx, &params); err != nil {
		return fmt.Errorf("vreemdeling update by nummer: %w", err)
	}

	return nil
}

func ToSearchName(input string) string {
	return strings.ToLower(input)
}

func ToVreemdeling(record *queries.BvvBackendVreemdeling) *api.Vreemdeling {
	birthDate := &openapi_types.Date{
		Time: record.Birthdate.Time,
	}

	return &api.Vreemdeling{
		Geboortedatum: birthDate,
		Id:            record.Vreemdelingnummer,
		Naam:          record.Name.String,
	}
}
