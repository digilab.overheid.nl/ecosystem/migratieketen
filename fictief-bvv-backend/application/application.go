package application

import (
	"log/slog"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/application/service"
	traceMdl "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/trace/middleware"
)

type Application struct {
	*http.Server
	logger  *slog.Logger
	service *service.Service
}

func New(logger *slog.Logger, backendListenAddress string, service *service.Service) Application {
	return Application{
		Server: &http.Server{
			Addr: backendListenAddress,
		},
		logger:  logger,
		service: service,
	}
}

func (app *Application) Router() {
	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			app.logger.Debug("Request headers", "headers", r.Header)
			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	})
	r.Use(middleware.Recoverer)
	r.Use(middleware.Heartbeat("/healthz"))
	r.Use(middleware.SetHeader("Content-Type", "application/json"))
	r.Use(traceMdl.Extractor())

	apiHandler := api.NewStrictHandler(app, nil)
	r.Mount("/v0", api.Handler(apiHandler))

	r.Group(func(r chi.Router) {
		r.Use(middleware.SetHeader("Content-Type", "application/yaml"))
		r.Get("/openapi.yaml", app.OpenAPI)
	})

	app.Handler = r
}
