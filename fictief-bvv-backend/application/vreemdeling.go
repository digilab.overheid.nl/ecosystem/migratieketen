package application

import (
	"context"
	"errors"
	"fmt"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/application/service"
)

// Implements api.StrictServerInterface.
func (app *Application) CreateSamenvoeging(ctx context.Context, request api.CreateSamenvoegingRequestObject) (api.CreateSamenvoegingResponseObject, error) {
	if err := app.service.CreateSamenvoeging(ctx, request.VreemdelingLeidendId, request.VreemdelingVervallenId); err != nil {
		return nil, err
	}

	return api.CreateSamenvoeging204JSONResponse{}, nil
}

// Implements api.StrictServerInterface.
func (app *Application) GetSamenvoegingen(ctx context.Context, request api.GetSamenvoegingenRequestObject) (api.GetSamenvoegingenResponseObject, error) {
	vreemdelingen, err := app.service.GetSamenvoegingen(ctx, request.VreemdelingId)
	if err != nil {
		return nil, err
	}

	return api.GetSamenvoegingen200JSONResponse{
		VreemdelingIdsResponseJSONResponse: api.VreemdelingIdsResponseJSONResponse{
			Body: api.VreemdelingIdsResponse{
				Data: vreemdelingen,
			},
		},
	}, nil
}

// Implements api.StrictServerInterface.
func (app *Application) CreateVreemdeling(ctx context.Context, request api.CreateVreemdelingRequestObject) (api.CreateVreemdelingResponseObject, error) {
	vreemdeling, err := app.service.CreateVreemdeling(ctx, request.Body.Data)
	if err != nil {
		return nil, err
	}

	return api.CreateVreemdeling201JSONResponse{
		VreemdelingResponseJSONResponse: api.VreemdelingResponseJSONResponse{
			Body: api.VreemdelingResponse{
				Data: *vreemdeling,
			},
		},
	}, nil
}

// Implements api.StrictServerInterface.
func (app *Application) DeleteVreemdelingById(ctx context.Context, request api.DeleteVreemdelingByIdRequestObject) (api.DeleteVreemdelingByIdResponseObject, error) {
	if err := app.service.DeleteVreemdelingByID(ctx, request.VreemdelingId); err != nil {
		return nil, fmt.Errorf("delete vreemdeling by id: %w", err)
	}

	return api.DeleteVreemdelingById200JSONResponse{
		EmptyResponseJSONResponse: map[string]interface{}{},
	}, nil
}

// Implements api.StrictServerInterface.
func (app *Application) ReadVreemdelingById(ctx context.Context, request api.ReadVreemdelingByIdRequestObject) (api.ReadVreemdelingByIdResponseObject, error) {
	vreemdeling, err := app.service.ReadVreemdelingByID(ctx, request.VreemdelingId)
	if err != nil {
		if errors.Is(err, service.ErrNotFound) {
			msg := err.Error()
			return api.ReadVreemdelingById410JSONResponse{
				GoneErrorResponseJSONResponse: api.GoneErrorResponseJSONResponse{
					Errors: []api.Error{
						{Message: &msg},
					},
				},
			}, nil
		}

		return nil, fmt.Errorf("read vreemdeling by id: %w", err)
	}

	return api.ReadVreemdelingById200JSONResponse{
		VreemdelingResponseJSONResponse: api.VreemdelingResponseJSONResponse{
			Body: api.VreemdelingResponse{
				Data: *vreemdeling,
			},
		},
	}, nil
}

// Implements api.StrictServerInterface.
func (app *Application) UpdateVreemdelingById(ctx context.Context, request api.UpdateVreemdelingByIdRequestObject) (api.UpdateVreemdelingByIdResponseObject, error) {
	vreemdeling, err := app.service.UpdateVreemdelingByID(ctx, request.VreemdelingId, request.Body.Data)
	if err != nil {
		return nil, fmt.Errorf("update vreemdeling by id: %w", err)
	}

	return api.UpdateVreemdelingById200JSONResponse{
		VreemdelingResponseJSONResponse: api.VreemdelingResponseJSONResponse{
			Body: api.VreemdelingResponse{
				Data: *vreemdeling,
			},
		},
	}, nil
}

// FindVreemdeling implements api.StrictServerInterface.
func (app *Application) FindVreemdeling(ctx context.Context, request api.FindVreemdelingRequestObject) (api.FindVreemdelingResponseObject, error) {
	vreemdelingen, err := app.service.FindVreemdeling(ctx, request.Params)
	if err != nil {
		return nil, err
	}

	response := api.FindVreemdeling200JSONResponse{
		VreemdelingenResponseJSONResponse: api.VreemdelingenResponseJSONResponse{
			Body: api.VreemdelingenResponse{
				Data: vreemdelingen,
			},
		},
	}

	return response, nil
}
