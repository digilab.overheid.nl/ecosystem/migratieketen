package cmd

import (
	"fmt"

	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/application/service"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/pkg/storage"
)

type ServeCmd struct {
	BackendListenAddress string `env:"MK_BACKEND_LISTEN_ADDRESS" default:":8080" name:"backend-listen-address" help:"Address to listen  on."`
	PostgresDSN          string `env:"MK_POSTGRES_DSN" required:"" name:"postgres-dsn" help:"Postgres Connection URL."`
}

func (opt *ServeCmd) Run(ctx *Context) error {
	logger := ctx.Logger.With("application", "http_server")

	logger.Info("Starting fictief bvv backend", "config", opt)

	if err := storage.MigrateInit(ctx.PSQLSchemaName, opt.PostgresDSN); err != nil {
		logger.Error("migrate init failed", "err", fmt.Errorf("migrate init: %w", err))
		return err
	}
	logger.Info("Migrate init successful")

	if err := storage.PostgresPerformMigrations(opt.PostgresDSN, ctx.PSQLSchemaName); err != nil {
		logger.Error("failed to migrate db", "err", err)
		return err
	}

	db, err := storage.New(opt.PostgresDSN)
	if err != nil {
		logger.Error("failed to connect to the database", "err", err)
		return err
	}

	service, err := service.New(logger, db)
	if err != nil {
		return err
	}

	app := application.New(logger, opt.BackendListenAddress, service)

	app.Router()

	if err := app.ListenAndServe(); err != nil {
		logger.Error("listen and serve failed", "err", err)
		return err
	}

	return nil
}
