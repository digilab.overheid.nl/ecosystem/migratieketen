package main

import (
	"log"

	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/cmd"
)

func main() {
	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}
}
