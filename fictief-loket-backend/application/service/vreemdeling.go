package service

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"sort"
	"time"

	"github.com/google/uuid"
	bvv_api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	sigma_api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"golang.org/x/exp/maps"
	"golang.org/x/sync/errgroup"
)

var ErrNotFound = errors.New("resource not found")

func (service *Service) ReadVreemdelingByID(
	ctx context.Context,
	vreemdelingID api.VreemdelingId,
	attributes map[string]struct{},
	sources []string,
) (map[string]api.Attribute, error) {
	ctx, span := service.tracerStart(ctx, "readVreemdelingByID",
		trace.WithAttributes(
			setSubjectID(vreemdelingID),
		),
	)

	defer span.End()

	additionalProperties, err := service.readVreemdelingByID(ctx, vreemdelingID, attributes, sources)
	if err != nil {
		err := fmt.Errorf("create proces by vreemdelingen id: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	span.SetStatus(codes.Ok, "")

	return additionalProperties, nil
}

func (service *Service) readVreemdelingByID(
	ctx context.Context,
	vreemdelingID api.VreemdelingId,
	attributes map[string]struct{},
	sources []string,
) (map[string]api.Attribute, error) {
	if err := service.verifyVreemdelingExistsByID(ctx, vreemdelingID); err != nil {
		return nil, err
	}

	set, err := service.getSamenvoegen(ctx, vreemdelingID)
	if err != nil {
		return nil, err
	}

	observations := service.readObservationsFromAllSigmaSources(ctx, maps.Keys(set), attributes, sources)
	additionalProperties := service.mergeObservations(attributes, observations)

	return additionalProperties, nil
}

func (service *Service) verifyVreemdelingExistsByID(ctx context.Context, vreemdelingID api.VreemdelingId) error {
	params := &bvv_api.ReadVreemdelingByIdParams{}

	// RH: TODO: maybe these 2 BVV calls should be one?
	// ie. a call to just get all alternative vnrs, regardless if any merges have been performed?
	resp, err := service.clients.bvv.ReadVreemdelingByIdWithResponse(ctx, vreemdelingID, params)
	if err != nil {
		return fmt.Errorf("read vreemdeling by id: %w", err)
	}

	if resp.StatusCode() == http.StatusGone {
		return ErrNotFound
	}

	if resp.JSON200 == nil {
		return fmt.Errorf("invalid response code: %s", string(resp.Body))
	}

	return nil
}

func (service *Service) getSamenvoegen(ctx context.Context, vreemdelingID api.VreemdelingId) (map[string]struct{}, error) {
	params := &bvv_api.GetSamenvoegingenParams{}

	resp, err := service.clients.bvv.GetSamenvoegingenWithResponse(ctx, vreemdelingID, params)
	if err != nil {
		return nil, fmt.Errorf("get samenvoegingen: %w", err)
	}

	if resp.JSON200 == nil {
		return nil, fmt.Errorf("invalid statuscode: %s", string(resp.Body))
	}

	set := map[string]struct{}{vreemdelingID: {}}
	for _, v := range resp.JSON200.Data {
		set[v] = struct{}{}
	}

	return set, nil
}

// contains just the value and metadata of an observation, to be used in a situation where attribute and source are known.
type bareObservationValue struct {
	ID            uuid.UUID
	Value         any
	CreatedAt     time.Time
	InvalidatedAt *time.Time
}

// Merge attribute observations from different sources.
// RH: ATTN: this can't be completely generic, because merge logic might be different for different attributes.
func (service *Service) mergeObservations(attributes map[string]struct{}, obssByAttrAndSrc map[string]map[string][]bareObservationValue) map[string]api.Attribute {
	result := make(map[string]api.Attribute, len(attributes))
	for attr := range attributes {
		var lastObsCreatedAt time.Time
		var lastObsOrValue bool
		var avgCount float64
		var avgTotal float64
		var avgBool bool
		var minIsSet bool
		var maxBool bool
		var maxValue float64 = 0
		var minValue float64 = 0
		var concatString string
		if obssBySrc, exists := obssByAttrAndSrc[attr]; exists {
			attrWithObs := api.Attribute{}
			for src, obss := range obssBySrc {
				for _, obs := range obss {
					attrWithObs.Observations = append(attrWithObs.Observations, api.Observation{
						Id:            obs.ID,
						Attribute:     attr,
						Value:         obs.Value,
						CreatedAt:     obs.CreatedAt,
						InvalidatedAt: obs.InvalidatedAt,
						Source:        src,
					})

					attrib := service.supportedAttributes[attr]
					if attrib.Aggregator == "or" && attrib.Type == "boolean" {
						service.logger.Error("Attr", "attr", attrib.Name)
						if !lastObsOrValue && obs.Value == true {
							lastObsOrValue = true
							attrWithObs.Value = obs.Value
						} else if !lastObsOrValue && obs.Value == false {
							attrWithObs.Value = obs.Value
						}

					} else if attrib.Aggregator == "avg" && attrib.Type == "number" {
						avgBool = true
						avgCount++
						if v, ok := obs.Value.(float64); ok {
							avgTotal += v
						}

					} else if attrib.Aggregator == "max" && attrib.Type == "number" {
						maxBool = true
						if v, ok := obs.Value.(float64); ok {
							if v > maxValue {
								maxValue = v
							}
						}
					} else if attrib.Aggregator == "min" && attrib.Type == "number" {
						if v, ok := obs.Value.(float64); ok {
							if !minIsSet {
								minIsSet = true
								minValue = v
							} else if v < minValue {
								minValue = v
							}
						}
					} else if attrib.Aggregator == "concat" && attrib.Type == "text" {
						if v, ok := obs.Value.(string); ok {
							if concatString == "" {
								concatString = v
							} else {
								concatString += ", " + v
							}
						}
					} else {
						if obs.CreatedAt.After(lastObsCreatedAt) {
							lastObsCreatedAt = obs.CreatedAt
							attrWithObs.Value = obs.Value
						}
					}
				}
			}

			if avgBool {
				attrWithObs.Value = avgTotal / avgCount
			} else if maxBool {
				attrWithObs.Value = maxValue
			} else if minIsSet {
				attrWithObs.Value = minValue
			} else if concatString != "" {
				attrWithObs.Value = concatString
			}

			// RH: TODO: this might be slow when there are many sources with many observations, sorting being O(n log n).
			// If that's a problem, we might change the internal datastructure to use something like a sorted btree for the observations, they're automatically sorted on insert.
			sort.SliceStable(attrWithObs.Observations, func(i, j int) bool {
				return attrWithObs.Observations[i].CreatedAt.After(attrWithObs.Observations[j].CreatedAt)
			})
			result[attr] = attrWithObs
		}
	}
	return result
}

// Returns a map of bare observations (ie. without source and attribute name) keyed by attribute name and source.
func (service *Service) readObservationsFromAllSigmaSources(ctx context.Context, vnrs []string, attributes map[string]struct{}, sources []string) map[string]map[string][]bareObservationValue {
	obssByAttrAndSrc := map[string]map[string][]bareObservationValue{}
	obsChan := make(chan sigma_api.Observation) // observations sent to this channel are added to the map of observations
	g := errgroup.Group{}

	for _, vnr := range vnrs {
		for _, src := range sources {
			g.Go(func() error {
				logger := service.logger.With("src", src)
				sigma, ok := service.clients.sigmas[service.cfg.SigmaFscEndpointNamesBySource[src]]
				if !ok {
					return fmt.Errorf("sigma client not found: src: '%s'", src)
				}

				params := &sigma_api.ReadVreemdelingByIdParams{
					Attributes: maps.Keys(attributes),
				}

				resp, err := sigma.ReadVreemdelingByIdWithResponse(ctx, vnr, params)
				if err != nil {
					logger.Error("Request to SIGMA failed", "err", err)
					return fmt.Errorf("read vreemdeling by id: %w", err)
				}

				if resp.JSON200 == nil {
					logger.Error("Request to SIGMA returned error", "err", err, "status_code", resp.StatusCode(), "body", string(resp.Body))
					return fmt.Errorf("failed to call sigma source '%s': %d body: %s", src, resp.StatusCode(), string(resp.Body))
				}

				for attr := range attributes {
					if vrAttr, exists := resp.JSON200.Data[attr]; exists {
						for _, obs := range vrAttr.Observations {
							obsChan <- obs
						}
					}
				}

				return nil
			})
		}
	}

	go func() {
		if err := g.Wait(); err != nil {
			service.logger.Error("Error waiting for all SIGMA queries", "err", err)
		}
		close(obsChan)
	}()

	for obs := range obsChan {
		attr := obs.Attribute
		src := obs.Source
		bareObs := bareObservationValue{
			ID:            obs.Id,
			Value:         obs.Value,
			CreatedAt:     obs.CreatedAt,
			InvalidatedAt: obs.InvalidatedAt,
		}
		if _, exists := obssByAttrAndSrc[attr]; !exists {
			obssByAttrAndSrc[attr] = map[string][]bareObservationValue{}
		}
		obssByAttrAndSrc[attr][src] = append(obssByAttrAndSrc[attr][src], bareObs)
	}
	return obssByAttrAndSrc
}
