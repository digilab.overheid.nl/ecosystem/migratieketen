package service

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/helpers"
	sigma_api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"golang.org/x/sync/errgroup"
)

func (service *Service) ReadProcessen(ctx context.Context, sources []string, attributes *api.QueryAttributesOptional) ([]api.Proces, error) {
	ctx, span := service.tracerStart(ctx, "readProcessen")

	defer span.End()

	processen, err := service.readProcessen(ctx, sources, attributes)
	if err != nil {
		err := fmt.Errorf("read processen: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	span.SetStatus(codes.Ok, "")

	return processen, nil
}

func (service *Service) ReadProcesByID(ctx context.Context, procesID api.ProcesId, sources []string, attributes *api.QueryAttributesOptional) (*api.Proces, error) {
	ctx, span := service.tracerStart(ctx, "readProcesById")

	defer span.End()

	process, err := service.readProcesByID(ctx, procesID, sources, attributes)
	if err != nil {
		err := fmt.Errorf("read proces by vreemdelingen id: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	span.SetStatus(codes.Ok, "")

	return process, nil
}

func (service *Service) ReadProcessenByVreemdelingID(ctx context.Context, vreemdelingID api.VreemdelingId, sources []string, attributes *api.QueryAttributesOptional) ([]api.Proces, error) {
	ctx, span := service.tracerStart(ctx, "readProcessenByVreemdelingId",
		trace.WithAttributes(
			setSubjectID(vreemdelingID),
		),
	)

	defer span.End()

	processen, err := service.readProcessenByVreemdelingID(ctx, vreemdelingID, sources, attributes)
	if err != nil {
		err := fmt.Errorf("read proces by vreemdelingen id: %w", err)
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}

	span.SetStatus(codes.Ok, "")

	return processen, nil
}

func (service *Service) readProcessen(ctx context.Context, sources []string, attributes *api.QueryAttributesOptional) ([]api.Proces, error) {
	processes := helpers.ConcurrentSlice[api.Proces]{}

	g, gctx := errgroup.WithContext(ctx)
	for _, source := range sources {
		g.Go(func() error {
			client, ok := service.clients.sigmas[service.cfg.SigmaFscEndpointNamesBySource[source]]
			if !ok {
				return fmt.Errorf("sigma not found")
			}

			resp, err := client.ReadProcessenWithResponse(gctx, &sigma_api.ReadProcessenParams{
				Attributes: attributes,
			})
			if err != nil {
				return err
			}

			if resp.JSON200 == nil {
				return fmt.Errorf("failed to call sigma source '%s': %d body: %s", source, resp.StatusCode(), string(resp.Body))
			}

			for _, record := range resp.JSON200.Data {
				processes.Append(ToProces(&record))
			}

			return nil
		})
	}

	if err := g.Wait(); err != nil {
		return nil, err
	}

	processen := make([]api.Proces, 0, processes.Length())
	for a := range processes.Iter() {
		processen = append(processen, a)
	}

	return processen, nil
}

func (service *Service) readProcesByID(ctx context.Context, procesID api.ProcesId, sources []string, attributes *api.QueryAttributesOptional) (*api.Proces, error) {
	var proces api.Proces

	g := new(errgroup.Group)

	// Note: because we are expecting one proces we are not taking into account race conditions or anything else since this should not happen.
	for _, source := range sources {
		g.Go(func() error {
			client, ok := service.clients.sigmas[service.cfg.SigmaFscEndpointNamesBySource[source]]
			if !ok {
				return fmt.Errorf("sigma not found")
			}

			resp, err := client.ReadProcesByIdWithResponse(ctx, procesID.String(), &sigma_api.ReadProcesByIdParams{
				Attributes: attributes,
			})
			if err != nil {
				return err
			}

			if resp.JSON200 == nil {
				return fmt.Errorf("failed to call sigma source '%s': %d body: %s", source, resp.StatusCode(), string(resp.Body))
			}

			proces = ToProces(&resp.JSON200.Data)

			return nil
		})
	}

	if err := g.Wait(); err != nil && proces.Id == uuid.Nil {
		return nil, err
	}

	return &proces, nil
}

func (service *Service) readProcessenByVreemdelingID(ctx context.Context, vreemdelingID api.VreemdelingId, sources []string, attributes *api.QueryAttributesOptional) ([]api.Proces, error) {
	processes := helpers.ConcurrentSlice[api.Proces]{}
	g, gctx := errgroup.WithContext(ctx)
	for _, source := range sources {

		source := source

		g.Go(func() error {
			client, ok := service.clients.sigmas[service.cfg.SigmaFscEndpointNamesBySource[source]]
			if !ok {
				return fmt.Errorf("sigma not found")
			}

			resp, err := client.ReadProcessenByVreemdelingIdWithResponse(gctx, vreemdelingID, &sigma_api.ReadProcessenByVreemdelingIdParams{
				Attributes: attributes,
			})
			if err != nil {
				return err
			}

			if resp.JSON200 == nil {
				return fmt.Errorf("failed to call sigma source '%s': %d body: %s", source, resp.StatusCode(), string(resp.Body))
			}

			for _, record := range resp.JSON200.Data {
				processes.Append(ToProces(&record))
			}

			return nil
		})
	}

	if err := g.Wait(); err != nil {
		return nil, err
	}

	proc := make([]api.Proces, 0, processes.Length())
	for a := range processes.Iter() {
		proc = append(proc, a)
	}

	return proc, nil
}

func ToProces(record *sigma_api.Proces) api.Proces {
	proces := api.Proces{
		CreatedAt:     record.CreatedAt,
		DeletedAt:     record.DeletedAt,
		Id:            record.Id,
		ProcesId:      record.ProcesId,
		VreemdelingId: record.VreemdelingId,
		Status:        record.Status,
		Type:          record.Type,
		Source:        record.Source,
	}

	if record.Attributes != nil {
		attributes := map[string]api.Attribute{}

		for k, attr := range *record.Attributes {
			procAttr := attributes[k]
			procAttr.Value = attr.Value

			for _, a := range attr.Observations {
				procAttr.Observations = append(procAttr.Observations, (api.Observation)(a))
			}

			attributes[k] = procAttr
		}

		proces.Attributes = &attributes
	}

	return proces
}
