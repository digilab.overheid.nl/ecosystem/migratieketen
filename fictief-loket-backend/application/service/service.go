package service

import (
	"context"
	"fmt"
	"log/slog"
	"time"

	"github.com/google/uuid"
	bvv "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/config"
	tracer "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/trace"
	sigma "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

type clients struct {
	bvv    *bvv.ClientWithResponses
	sigmas map[string]*sigma.ClientWithResponses
}

type Service struct {
	tracer                trace.Tracer
	logger                *slog.Logger
	cfg                   *config.Config
	shutdownFns           []func(context.Context) error
	clients               clients
	processingActivityIDs map[string]uuid.UUID
	supportedAttributes   map[string]config.Attribute // precomputed map of attribute names with their config
}

func New(cfg *config.Config, logger *slog.Logger) (*Service, error) {
	otelShutdown, err := setupOTelSDK(context.TODO())
	if err != nil {
		return nil, fmt.Errorf("setup otel sdk: %w", err)
	}

	service := &Service{
		tracer:              otel.Tracer("bvv"),
		logger:              logger,
		cfg:                 cfg,
		shutdownFns:         []func(context.Context) error{otelShutdown},
		supportedAttributes: initSupportedAttributes(cfg, logger),
		processingActivityIDs: map[string]uuid.UUID{
			"readProcessen":                uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"readProcesById":               uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"readProcessenByVreemdelingId": uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
			"readVreemdelingByID":          uuid.MustParse("2c32fb24-e631-54b3-9d6f-81271b55c389"),
		},
	}

	bvv, err := service.getBvvClient()
	if err != nil {
		return nil, err
	}

	sigmas, err := service.getSigmaClients()
	if err != nil {
		return nil, err
	}

	service.clients = clients{
		bvv:    bvv,
		sigmas: sigmas,
	}

	return service, nil
}

func (service *Service) tracerStart(ctx context.Context, name string, opts ...trace.SpanStartOption) (context.Context, trace.Span) {
	var parent tracer.ProcessingContext
	if p := tracer.ProcessingOperationFromContext(ctx); p.IsValid() {
		parent = *p
	}

	if opts == nil {
		opts = make([]trace.SpanStartOption, 0, 2)
	}

	opts = append(opts,
		trace.WithTimestamp(time.Now()),
		trace.WithAttributes(
			attribute.String("dpl.rva.activity.id", service.processingActivityIDs[name].String()),
		))

	if parent.IsValid() && parent.Foreign() {
		opts = append(opts, trace.WithAttributes(
			attribute.String("dpl.rva.foreign.trace_id", parent.TraceID().String()),
			attribute.String("dpl.rva.foreign.operation_id", parent.OperationID().String()),
		))
	}

	ctx, span := service.tracer.Start(ctx, name, opts...)

	op := tracer.New(tracer.TraceID(span.SpanContext().TraceID()), tracer.SpanID(span.SpanContext().SpanID()))
	ctx = tracer.ContextWithProcessingOperation(ctx, op)

	return ctx, span
}

func initSupportedAttributes(cfg *config.Config, logger *slog.Logger) map[string]config.Attribute {
	supportedAttributes := make(map[string]config.Attribute)

	for _, rubriek := range cfg.GlobalConfig.Rubrieken {
		for _, attr := range rubriek.Attributes {
			logger.Debug("Added supported attribute", "attr_name", attr.Name)
			supportedAttributes[attr.Name] = attr
		}
	}

	return supportedAttributes
}

func setSubjectID(vreemdelingID bvv.VreemdelingId) attribute.KeyValue {
	return attribute.String("dpl.core.user", fmt.Sprintf("vid:%s", vreemdelingID))
}
