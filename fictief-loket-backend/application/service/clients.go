package service

import (
	"context"
	"fmt"
	"net/http"

	bvv "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/config"
	tracer "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/trace"
	sigma "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
)

func injector(cfg config.FscConfigValue) func(ctx context.Context, req *http.Request) error {
	return func(ctx context.Context, req *http.Request) error {
		req.Header.Set("Fsc-Grant-Hash", cfg.FscGrantHash)
		tracer.Inject(ctx, req.Header)
		return nil
	}
}

func (a *Service) getBvvClient() (*bvv.ClientWithResponses, error) {
	cfg := a.cfg.FscConfig["fictief-np-bvv-backend"] // Note: the BVV is deployed centrally, within the NP organization

	url := fmt.Sprintf("%s/v0", cfg.Endpoint)
	reqHeaders := bvv.WithRequestEditorFn(injector(cfg))

	client, err := bvv.NewClientWithResponses(url, reqHeaders)
	if err != nil {
		return nil, fmt.Errorf("bvv: new client: %w", err)
	}

	return client, nil
}

func (a *Service) getSigmaClients() (map[string]*sigma.ClientWithResponses, error) {
	clients := map[string]*sigma.ClientWithResponses{}
	for key, value := range a.cfg.FscConfig {
		client, err := a.getSigmaClient(value)
		if err != nil {
			return nil, err
		}
		clients[key] = client
	}

	return clients, nil
}

func (a *Service) getSigmaClient(cfg config.FscConfigValue) (*sigma.ClientWithResponses, error) {
	url := fmt.Sprintf("%s/v0", cfg.Endpoint)
	reqHeaders := sigma.WithRequestEditorFn(injector(cfg))

	client, err := sigma.NewClientWithResponses(url, reqHeaders)
	if err != nil {
		return nil, fmt.Errorf("sigma: new client: %w", err)
	}

	return client, nil
}
