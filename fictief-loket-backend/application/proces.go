package application

import (
	"context"
	"fmt"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
)

// ReadProcessen implements api.StrictServerInterface.
func (app *Application) ReadProcessen(ctx context.Context, request api.ReadProcessenRequestObject) (api.ReadProcessenResponseObject, error) {
	sources := app.getSources(request.Params.Sources)

	processen, err := app.service.ReadProcessen(ctx, sources, request.Params.Attributes)
	if err != nil {
		return nil, fmt.Errorf("read processen: %w", err)
	}

	return api.ReadProcessen200JSONResponse{
		ProcessenResponseJSONResponse: api.ProcessenResponseJSONResponse{
			Body: api.ProcessenResponse{
				Data: processen,
			},
		},
	}, nil
}

// ReadProcesById implements api.StrictServerInterface.
func (app *Application) ReadProcesById(ctx context.Context, request api.ReadProcesByIdRequestObject) (api.ReadProcesByIdResponseObject, error) {
	sources := app.getSources(request.Params.Sources)
	process, err := app.service.ReadProcesByID(ctx, request.ProcesId, sources, request.Params.Attributes)
	if err != nil {
		return nil, fmt.Errorf("read proces by id: %w", err)
	}

	return api.ReadProcesById200JSONResponse{
		ProcesResponseJSONResponse: api.ProcesResponseJSONResponse{
			Body: api.ProcesResponse{
				Data: *process,
			},
		},
	}, nil
}

// ReadProcessenByVreemdelingId implements api.StrictServerInterface.
func (app *Application) ReadProcessenByVreemdelingId(ctx context.Context, request api.ReadProcessenByVreemdelingIdRequestObject) (api.ReadProcessenByVreemdelingIdResponseObject, error) {
	sources := app.getSources(request.Params.Sources)
	processes, err := app.service.ReadProcessenByVreemdelingID(ctx, request.VreemdelingId, sources, request.Params.Attributes)
	if err != nil {
		return nil, fmt.Errorf("read processen by vreemdelingID: %w", err)
	}

	return api.ReadProcessenByVreemdelingId200JSONResponse{
		ProcessenResponseJSONResponse: api.ProcessenResponseJSONResponse{
			Body: api.ProcessenResponse{
				Data: processes,
			},
		},
	}, nil
}
