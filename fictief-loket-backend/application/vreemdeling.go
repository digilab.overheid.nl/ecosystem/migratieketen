package application

import (
	"context"
	"errors"
	"fmt"
	"slices"

	"golang.org/x/exp/maps"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/application/service"
)

var (
	ErrNotFound              = errors.New("resource not found")
	ErrAttributeNotSupported = errors.New("attribute not supported")
)

func (app *Application) getAttributes(request []string) (map[string]struct{}, error) {
	// Verify and parse request:
	attributes := map[string]struct{}{} // Set of requested attributes
	for _, f := range request {
		if _, exists := app.supportedAttributes[f]; !exists {
			return nil, fmt.Errorf("%w: '%s'", ErrAttributeNotSupported, f)
		}
		attributes[f] = struct{}{}
	}

	return attributes, nil
}

func (app *Application) getSources(request *[]string) []string {
	var sources []string
	if request == nil || slices.Equal(*request, api.QuerySources{""}) { // RH: ATTN: note that `?sources=` creates a slice with a single empty string, NOT an empty slice.
		sources = maps.Keys(app.cfg.SigmaFscEndpointNamesBySource)
	} else {
		for _, v := range *request {
			if _, exists := app.cfg.SigmaFscEndpointNamesBySource[v]; exists {
				sources = append(sources, v)
			} else {
				app.logger.Error("Unknown source SIGMA requested", "src", v, "known_sigmas", maps.Keys(app.cfg.SigmaFscEndpointNamesBySource))
			}
		}
	}

	return sources
}

func (app *Application) ReadVreemdelingById(ctx context.Context, request api.ReadVreemdelingByIdRequestObject) (api.ReadVreemdelingByIdResponseObject, error) {
	attributes, err := app.getAttributes(request.Params.Attributes)
	if err != nil {
		if errors.Is(err, ErrAttributeNotSupported) {
			return api.ReadVreemdelingById400JSONResponse{
				BadRequestErrorResponseJSONResponse: api.BadRequestErrorResponseJSONResponse{
					Errors: []api.Error{{Message: err.Error()}},
				},
			}, nil
		}
	}

	sources := app.getSources(request.Params.Sources)

	properties, err := app.service.ReadVreemdelingByID(ctx, request.VreemdelingId, attributes, sources)
	if err != nil {
		if errors.Is(err, service.ErrNotFound) {
			return api.ReadVreemdelingById410JSONResponse{
				GoneErrorResponseJSONResponse: api.GoneErrorResponseJSONResponse{
					Errors: []api.Error{{Message: "VreemdelingID not found"}},
				},
			}, nil
		}
	}

	return api.ReadVreemdelingById200JSONResponse{
		VreemdelingWithObservationsResponseJSONResponse: api.VreemdelingWithObservationsResponseJSONResponse{
			Body: api.VreemdelingWithObservationsResponse{
				Data: api.VreemdelingWithObservations{
					Id:                   request.VreemdelingId,
					AdditionalProperties: properties,
				},
			},
		},
	}, nil
}
