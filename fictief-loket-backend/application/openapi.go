package application

import (
	"io"
	"net/http"
	"os"
)

func (app *Application) OpenAPI(w http.ResponseWriter, _ *http.Request) {
	f, err := os.Open("./../fictief-loket-api/spec/openapi.yaml")
	if err != nil {
		app.logger.Error("opening openapi failed", "err", err)
		return
	}

	defer f.Close()

	if _, err := io.Copy(w, f); err != nil {
		app.logger.Error("openapi copy failed", "err", err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
