module gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend

go 1.22.2

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api => ../fictief-bvv-api

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api => ../fictief-loket-api

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api => ../fictief-sigma-api

require (
	github.com/alecthomas/kong v0.9.0
	github.com/go-chi/chi/v5 v5.0.13
	github.com/google/uuid v1.6.0
	github.com/stretchr/testify v1.9.0
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api v0.0.0
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api v0.0.0
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api v0.0.0
	go.opentelemetry.io/otel v1.27.0
	go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc v1.27.0
	go.opentelemetry.io/otel/sdk v1.27.0
	go.opentelemetry.io/otel/trace v1.27.0
	golang.org/x/exp v0.0.0-20240613232115-7f521ea00fb8
	golang.org/x/sync v0.7.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/cenkalti/backoff/v4 v4.3.0 // indirect
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/getkin/kin-openapi v0.125.0 // indirect
	github.com/go-logr/logr v1.4.2 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/go-openapi/jsonpointer v0.21.0 // indirect
	github.com/go-openapi/swag v0.23.0 // indirect
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.20.0 // indirect
	github.com/invopop/yaml v0.3.1 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/oapi-codegen/runtime v1.1.1 // indirect
	github.com/perimeterx/marshmallow v1.1.5 // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	gitlab.com/digilab.overheid.nl/ecosystem/fsc-vwl-api v0.0.0-20240313144656-6e78c7411048 // indirect
	go.opentelemetry.io/otel/exporters/otlp/otlptrace v1.27.0 // indirect
	go.opentelemetry.io/otel/metric v1.27.0 // indirect
	go.opentelemetry.io/proto/otlp v1.3.1 // indirect
	golang.org/x/net v0.26.0 // indirect
	golang.org/x/sys v0.21.0 // indirect
	golang.org/x/text v0.16.0 // indirect
	google.golang.org/genproto/googleapis/api v0.0.0-20240617180043-68d350f18fd4 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240617180043-68d350f18fd4 // indirect
	google.golang.org/grpc v1.64.0 // indirect
	google.golang.org/protobuf v1.34.2 // indirect
)
