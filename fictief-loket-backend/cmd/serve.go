package cmd

import (
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/application/service"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/config"
)

type ServeCmd struct {
	Name                 string `env:"MK_NAME" required:"" name:"name" help:"Name of the service."`
	BackendListenAddress string `env:"MK_BACKEND_LISTEN_ADDRESS" default:":8080" name:"backend-listen-address" help:"Address to listen  on."`

	ConfigPath    string `env:"MK_CONFIG_PATH"  default:"../config/global.yaml" name:"config-path" help:"Location of the config"`
	FscConfigPath string `env:"MK_FSC_CONFIG_PATH" name:"fsc-config-path" help:"Location of the config"`
}

func (opt *ServeCmd) Run(ctx *Context) error {
	fscConfig, sigmaFscEndpointNamesBySource, err := config.GetFscConfig(opt.FscConfigPath)
	if err != nil {
		return err
	}

	globalConfig, err := config.GetGlobalConfig(opt.ConfigPath)
	if err != nil {
		return err
	}

	config := &config.Config{
		Name:                          opt.Name,
		BackendListenAddress:          opt.BackendListenAddress,
		GlobalConfig:                  globalConfig,
		FscConfig:                     fscConfig,
		SigmaFscEndpointNamesBySource: sigmaFscEndpointNamesBySource,
	}

	logger := ctx.Logger.With("application", "http_server")

	logger.Info("Starting fictief loket backend", "config", config)

	svc, err := service.New(config, logger)
	if err != nil {
		logger.Error("service new", "err", err)
		return nil
	}

	app := application.New(config, logger, svc)

	app.Router()

	if err := app.ListenAndServe(); err != nil {
		logger.Error("listen and serve failed", "err", err)
		return nil

	}

	return nil
}
