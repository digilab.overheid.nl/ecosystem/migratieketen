package main

import (
	"log"

	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/cmd"
)

func main() {

	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}
}
