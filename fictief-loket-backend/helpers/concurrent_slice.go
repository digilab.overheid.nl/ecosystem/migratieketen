package helpers

import "sync"

// ConcurrentSlice type that can be safely shared between goroutines
type ConcurrentSlice[T any] struct {
	sync.RWMutex
	items []T
}

func (s *ConcurrentSlice[T]) Append(v T) {
	s.Lock()
	s.items = append(s.items, v)
	s.Unlock()
}

func (s *ConcurrentSlice[T]) Length() int {
	s.RLock()
	l := len(s.items)
	s.RUnlock()
	return l
}

func (s *ConcurrentSlice[T]) Iter() <-chan T { // Not super happy with the current implementation since the iteration could block the whole slice
	c := make(chan T)

	go func() {
		s.RLock()
		defer s.RUnlock()

		for _, v := range s.items {
			c <- v
		}

		close(c)
	}()

	return c
}
