package trace

import (
	"context"
	"encoding/hex"
	"fmt"
	"net/http"
	"strings"
)

const (
	traceparentHeader = "traceparent"
)

var (
	emptyProcessingOperation = ProcessingContext{}
	currentProcessingKey     ctxKey
)

type ctxKey int

func Extract(ctx context.Context, h http.Header) context.Context {
	tp := h.Get(traceparentHeader)
	if tp == "" {
		return ctx
	}

	parts := strings.SplitN(tp, "-", 4)
	version := parts[0]
	if len(version) != 2 {
		return ctx
	}

	traceID, err := hex.DecodeString(parts[1])
	if err != nil || len(traceID) != 16 {
		return ctx
	}

	parentID, err := hex.DecodeString(parts[2])
	if err != nil || len(parentID) != 8 {
		return ctx
	}

	op := ProcessingContext{
		traceID: TraceID(traceID),
		spanID:  SpanID(parentID),
		foreign: true,
	}

	return ContextWithProcessingOperation(ctx, &op)
}

func Inject(ctx context.Context, h http.Header) {
	op := ProcessingOperationFromContext(ctx)
	if !op.IsValid() {
		return
	}

	v := fmt.Sprintf("00-%x-%x-01", op.traceID[:], op.spanID[:])

	h.Set(traceparentHeader, v)
}

func ContextWithProcessingOperation(ctx context.Context, op *ProcessingContext) context.Context {
	return context.WithValue(ctx, currentProcessingKey, op)
}

func ProcessingOperationFromContext(ctx context.Context) *ProcessingContext {
	if ctx == nil {
		return &emptyProcessingOperation
	}

	op, ok := ctx.Value(currentProcessingKey).(*ProcessingContext)
	if !ok {
		return &emptyProcessingOperation
	}

	return op
}
