package middleware

import (
	"net/http"

	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/trace"
)

func Extractor() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := trace.Extract(r.Context(), r.Header)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
