package config

import (
	"fmt"
	"log/slog"
	"os"
	"regexp"

	"gopkg.in/yaml.v3"
)

type Attribute struct {
	Name       string      `yaml:"name"`
	Label      string      `yaml:"label"`
	Type       string      `yaml:"type"`
	Aggregator string      `yaml:"aggregator"`
	Multiple   bool        `yaml:"multiple"` // For types 'select' and 'file'
	Default    interface{} `yaml:"default"`

	// For number types
	Min  *interface{} `yaml:"min"`
	Max  *interface{} `yaml:"max"`
	Step *interface{} `yaml:"step"`

	// For select, radios, and checkboxes types
	Options []struct {
		Name  string `yaml:"name"`
		Label string `yaml:"label"`
	} `yaml:"options"`
}

type Rubriek struct {
	Name  string `yaml:"name"`
	Label string `yaml:"label"`

	Attributes []Attribute `yaml:"attributes"`
}

type GlobalConfig struct {
	Rubrieken []Rubriek `yaml:"rubrieken"`
}

type FscConfigValue struct {
	Endpoint     string `yaml:"endpoint"`
	FscGrantHash string `yaml:"FscGrantHash"`
}

type FscConfig map[string]FscConfigValue

type Config struct {
	Name                 string
	BackendListenAddress string

	GlobalConfig `mapstructure:",squash"`
	FscConfig
	SigmaFscEndpointNamesBySource FscConfigMap
}

func readConfig(path string, cfg any) error {
	file, err := os.ReadFile(path)
	if err != nil {
		return fmt.Errorf("error reading config file: %w", err)
	}

	if err := yaml.Unmarshal(file, cfg); err != nil {
		return fmt.Errorf("unmarshalling config failed: %w", err)
	}

	return nil
}

// RH: TODO: ATTN: poor man's service discovery, this is extremely brittle.
var (
	sigmaBackendNameRe = regexp.MustCompile(`^(.*)+-sigma-backend$`)
	fscGrantHashRe     = regexp.MustCompile(`^\$[a-z0-1,=]+\$`) // https://en.wikipedia.org/wiki/Crypt_(C)#Key_derivation_functions_supported_by_crypt
)

type FscConfigMap = map[string]string

func getSigmaFscEndpointNamesBySourceFromFscConfig(cfg FscConfig) FscConfigMap {
	result := map[string]string{}
	for k, v := range cfg {
		if m := sigmaBackendNameRe.FindStringSubmatch(k); m != nil {
			if fscGrantHashRe.MatchString(v.FscGrantHash) {
				slog.Info("Added SIGMA endpoint as source", "name", k)
				result[m[1]] = k
			} else {
				slog.Error("Invalid FSC grant hash for SIGMA endpoint", "name", k, "hash", v.FscGrantHash)
			}
		}
	}

	return result
}

func GetFscConfig(path string) (FscConfig, FscConfigMap, error) {
	cfg := FscConfig{}

	if err := readConfig(path, &cfg); err != nil {
		return nil, nil, err
	}

	sigmaFscEndpointNamesBySource := getSigmaFscEndpointNamesBySourceFromFscConfig(cfg)

	return cfg, sigmaFscEndpointNamesBySource, nil
}

func GetGlobalConfig(path string) (GlobalConfig, error) {
	cfg := GlobalConfig{}

	if err := readConfig(path, &cfg); err != nil {
		return cfg, err
	}

	return cfg, nil
}
